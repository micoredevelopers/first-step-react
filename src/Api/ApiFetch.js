import { get } from 'lodash'
import { session } from 'store2'
import { toast } from 'react-toastify'
import { LogOut } from '../Helpers'

export const ApiFetch = async (url, configs = {}) => {
  try {
    const token = session.get('token')
    const initRequestOptions = {
      ...configs,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: configs.isFile ? configs.body : JSON.stringify(configs.body)
    }

    configs.isFile && delete initRequestOptions.headers['Content-Type']
    const ApiCall = await fetch(url, initRequestOptions)
    const ApiData = await ApiCall.json()
  
    if (!ApiCall.ok) {
      const errors = get(ApiData, 'errors', [])
      
      if (ApiCall.status === 401) {
        toast.error('Пользователь не авторизирован!')
  
        setTimeout(() => {
          LogOut()
        }, 5000)
        
        return { errors: [] }
      } else {
        errors.forEach((err) => {
          const errorMsg = get(err, 'msg')
          const errorFrontMsg = get(err, 'frontError')
    
          toast.error(errorFrontMsg || errorMsg)
        })
  
        return { errors }
      }
    }

    return {
      errors: ApiData.error,
      data: ApiData.data,
      abort: () => ApiData.controller && ApiData.controller.abort()
    }
  } catch (e) {
    const err = e
    const errors = err.name !== 'AbortError' ? err : null

    return { errors }
  }
}
