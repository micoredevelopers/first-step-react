import { ApiFetch } from './ApiFetch'
import { filesUrl, fileUrl } from './Urls'

export const uploadFiles = async (files) => {
  const formData = new FormData()
  files.map(file => formData.append(`${files.length === 1 ? 'uploadFile' : 'uploadFiles[]'}`, file))
  
  const apiCall = await ApiFetch(`${files.length === 1 ? fileUrl : filesUrl}`, {
    isFile: true,
    method: 'POST',
    body: formData
  })
  
  return apiCall && apiCall
}
