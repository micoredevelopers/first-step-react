import { toast } from 'react-toastify'
import { groupsUrl } from './Urls'
import { ApiFetch } from './ApiFetch'
import { stringify } from 'query-string'

export const getGroups = async (filters) => {
  const apiCall = await ApiFetch(`${groupsUrl}?${stringify(filters)}`)
  
  if (!apiCall.errors) {
    return apiCall.data.rows
  } else {
    toast.error('Ошибка при загрузке списка групп!')
  }
}

export const updateGroup = async (id, data) => {
  const apiCall = await ApiFetch(`${groupsUrl}/${id}`, {
    body: data,
    method: 'POST'
  })
  
  if (!apiCall.errors) {
    toast.success('Группа успешно перемещена!')
    return apiCall.data
  } else {
    toast.error('Ошибка перемещении группы!')
  }
}
