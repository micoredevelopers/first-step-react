import { toast } from 'react-toastify'
import { stringify } from 'query-string'
import { ApiFetch } from './ApiFetch'
import { lessonsUrl, lessonCreateUrl } from './Urls'

export const getLessons = async (filters) => {
  const {
    old,
    page,
    today,
    order,
    limit,
    month,
    types,
    group,
    student,
    tomorrow,
    teachers,
    byStudent,
    orderByDay,
    newLessons,
    replacement,
    attrGroups = 'lesson_assoc,student_lesson_work_assoc',
    ...other
  } = filters
  const newFilter = {
    old,
    student,
    page: page,
    replacement,
    today: today,
    limit: limit,
    month: month,
    types: types,
    groups: group,
    new: newLessons,
    orderByDate: order,
    tomorrow: tomorrow,
    teachers: teachers,
    by_day: orderByDay,
    mixByStudent: byStudent,
    attribute_groups: attrGroups,
    ...other
  }
  const apiCall = await ApiFetch(`${lessonsUrl}?${stringify(newFilter)}`)

  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при загрузке списка уроков!')
  }
}

export const updateLesson = async (lessonId, params) => {
  const apiCall = await ApiFetch(`${lessonsUrl}/${lessonId}`, { method: 'POST', body: params })
  
  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при редактировании урока!')
  }
}

export const duplicateLesson = async (lessonId, date) => {
  const apiCall = await ApiFetch(`${lessonsUrl}/${lessonId}/duplicate`, { method: 'POST', body: { date } })
  
  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при дублировании урока!')
  }
}

export const createLesson = async (data) => {
  const apiCall = await ApiFetch(lessonCreateUrl, { method: 'POST', body: data })
  
  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при создании урока!')
  }
}

export const deleteLesson = async (id) => {
  const apiCall = await ApiFetch(`${lessonsUrl}/${id}`, { method: 'DELETE' })
  
  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при удалении урока!')
  }
}
