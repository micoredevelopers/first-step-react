import { meetingUrl, createMeetingUrl } from './Urls'
import { ApiFetch } from './ApiFetch'
import { toast } from 'react-toastify'

export const getMeeting = async () => {
  const apiCall = await ApiFetch(meetingUrl)

  if (!apiCall.errors) {
    return apiCall.data
  }
}

export const createMeeting = async (id) => {
  const apiCall = await ApiFetch(createMeetingUrl, { method: 'POST', body: { group: id } })

  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при загрузке ссылки на урок!')
  }
}

export const deleteMeeting = async () => {
  const apiCall = await ApiFetch(meetingUrl, { method: 'DELETE' })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}
