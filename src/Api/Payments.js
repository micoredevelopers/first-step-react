import { stringify } from 'query-string'
import { paymentUrl } from './Urls'
import { ApiFetch } from './ApiFetch'

export const getPayments = async (filters) => {
  const dataFilters = {
    ...filters,
    orderByDate: 'desc',
    attribute_groups: 'payment_assoc,teacher_assoc,student_assoc'
  }
  const apiCall = await ApiFetch(`${paymentUrl}?${stringify(dataFilters)}`)
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}

export const updatePayment = async (params) => {
  const sendData = {
    status: params.status,
    receiptFile: params.file
  }
  const apiCall = await ApiFetch(`${paymentUrl}/${params.paymentId}`, {
    method: 'POST',
    body: sendData
  })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}
