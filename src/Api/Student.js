import { toast } from 'react-toastify'
import { stringify } from 'query-string'
import { studentsUrl } from './Urls'
import { ApiFetch } from './ApiFetch'

export const getStudents = async (filters) => {
  const dataFilters = {
    ...filters,
    attribute_groups: 'student_assoc,lesson_assoc,student_lesson_work_assoc'
  }
  const apiCall = await ApiFetch(`${studentsUrl}?${stringify(dataFilters)}`)
  
  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при загрузке списка учеников!')
  }
}

export const createStudent = async (data) => {
  const apiCall = await ApiFetch(studentsUrl, {
    body: data,
    method: 'POST'
  })
  
  if (!apiCall.errors) {
    toast.success('Ученик успешно создан!')
    return apiCall.data
  } else {
    toast.error('При создании ученика произошла ошибка!')
  }
}

export const updateStudent = async (id, data) => {
  const apiCall = await ApiFetch(`${studentsUrl}/${id}`, {
    body: data,
    method: 'POST'
  })
  
  if (!apiCall.errors) {
    toast.success('Ученик успешно обновлен!')
    return apiCall.data
  } else {
    toast.error('При обновлении ученика произошла ошибка!')
  }
}

export const enableStudent = async (id, status) => {
  const apiCall = await ApiFetch(`${studentsUrl}/${id}/enable`, {
    body: status,
    method: 'POST'
  })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}

export const deleteStudent = async (id) => {
  const apiCall = await ApiFetch(`${studentsUrl}/${id}`, { method: 'DELETE' })
  
  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при удалении ученика!')
  }
}
