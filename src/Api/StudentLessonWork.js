import { ApiFetch } from './ApiFetch'
import { stringify } from 'query-string'
import { studentLessonWorkUrl, lessonsUrl, updateCurrentLessonWorkUrl } from './Urls'

export const getStudentLessonWork = async (filters) => {
  const sendData = {
    attribute_groups: 'student_lesson_work_assoc',
    ...filters
  }
  const apiCall = await ApiFetch(`${studentLessonWorkUrl}?${stringify(sendData)}`)
  
  if (!apiCall.errors) {
    return apiCall.data.rows
  }
}

export const updateStudentLessonWork = async (lessonId, params) => {
  const sendData = {
    absent: params.absent,
    homeWorkFiles: params.files,
    lessonMark: params.lessonMark,
    homeworkMark: params.homeworkMark,
    teacherComment: params.teacherComment
  }
  const apiCall = await ApiFetch(`${studentLessonWorkUrl}/${lessonId}`, { method: 'POST', body: sendData })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}

export const updateCurrentStudentLessonWork = async (lessonId, files) => {
  const sendData = { homeworkFiles: files }
  const apiCall = await ApiFetch(updateCurrentLessonWorkUrl(lessonId), {
    method: 'POST',
    body: sendData
  })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}

export const createHomework = async ({ lessonId, data }) => {
  const sendData = {
    homeworkFiles: data.files,
    homeworkDescription: data.description
  }
  const apiCall = await ApiFetch(
    `${lessonsUrl}/${lessonId}/homework`,
    { method: 'POST', body: sendData })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}
