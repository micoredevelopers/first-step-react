import { ApiFetch } from './ApiFetch'
import { studentStudyUrl, teacherStudyUrl } from './Urls'

export const studyTeacher = async () => {
  const apiCall = await ApiFetch(teacherStudyUrl, {
    method: 'POST',
    body: {
      wasTrained: 1
    }
  })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}

export const studyStudent = async () => {
  const apiCall = await ApiFetch(studentStudyUrl, {
    method: 'POST',
    body: {
      wasTrained: 1
    }
  })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}
