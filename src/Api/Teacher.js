import { toast } from 'react-toastify'
import { stringify } from 'query-string'
import { ApiFetch } from './ApiFetch'
import { confirmContractUrl, teachersUrl } from './Urls'

export const getTeachers = async (filters) => {
  const newFilters = {
    ...filters,
    attribute_groups: 'teacher_assoc,safe'
  }
  const apiCall = await ApiFetch(`${teachersUrl}?${stringify(newFilters)}`)
  
  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при загрузке списка учителей!')
  }
}

export const createTeacher = async (data) => {
  const apiCall = await ApiFetch(teachersUrl, {
    body: data,
    method: 'POST'
  })
  
  if (!apiCall.errors) {
    toast.success('Учитель успешно создан!')
    return apiCall.data
  } else {
    toast.error('Произошла ошибка при создании учителя!')
  }
}

export const updateTeacher = async (id, data) => {
  const apiCall = await ApiFetch(`${teachersUrl}/${id}?attribute_groups=teacher_assoc,safe`, {
    body: data,
    method: 'POST'
  })
  
  if (!apiCall.errors) {
    toast.success('Учитель успешно обновлен!')
    return apiCall.data
  } else {
    toast.error('Произошла ошибка при обновлении учителя!')
  }
}

export const enableTeacher = async (id, status) => {
  const apiCall = await ApiFetch(`${teachersUrl}/${id}/enable`, {
    body: status,
    method: 'POST'
  })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}

export const deleteTeacher = async (id) => {
  const apiCall = await ApiFetch(`${teachersUrl}/${id}`, { method: 'DELETE' })
  
  if (!apiCall.errors) {
    return apiCall.data
  } else {
    toast.error('Ошибка при удалении учителя!')
  }
}

export const confirmContract = async () => {
  const apiCall = await ApiFetch(confirmContractUrl, {
    method: 'POST',
    body: { confirmedContract: 1 }
  })
  
  if (!apiCall.errors) {
    return apiCall.data
  }
}
