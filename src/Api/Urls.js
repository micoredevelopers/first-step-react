export const apiUrl = process.env.REACT_APP_API_URL

export const loginUrl = `${apiUrl}/login-api`
export const groupsUrl = `${apiUrl}/api/groups`
export const teachersUrl = `${apiUrl}/api/teachers`
export const studentsUrl = `${apiUrl}/api/students`
export const noteUrl = `${apiUrl}/api/notes/current`
export const fileUrl = `${apiUrl}/api/files`
export const filesUrl = `${fileUrl}/upload-multiple`
export const filesDownloadUrl = `${fileUrl}/download-multiple`

export const lessonsUrl = `${apiUrl}/api/lessons`
export const lessonPeriods = `${apiUrl}/api/lessons/date-periods`
export const lessonCreateUrl = `${apiUrl}/api/lessons?attribute_groups=safe,lesson_assoc`

export const studentLessonWorkUrl = `${apiUrl}/api/student-lesson-works`
export const updateCurrentLessonWorkUrl = (id) => `${studentLessonWorkUrl}/current/${id}?attribute_groups=student_lesson_work_assoc`

export const createMeetingUrl = `${apiUrl}/api/meetings`
export const meetingUrl = `${apiUrl}/api/meetings/current`

export const paymentUrl = `${apiUrl}/api/payments`

export const teacherStudyUrl = `${apiUrl}/api/teachers/current/trained`
export const studentStudyUrl = `${apiUrl}/api/students/current/trained`

export const confirmContractUrl = `${apiUrl}/api/teachers/current/confirmed-contract`
