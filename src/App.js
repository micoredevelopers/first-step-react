import React from 'react'
import { loadCldr } from '@syncfusion/ej2-base'
import { ThemeProvider } from 'styled-components'
import { ToastContainer, Flip } from 'react-toastify'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core'
import { RootRouter } from './Router'
import { getAccountType, getToken } from './Helpers'
import { GlobalStyled, MaterialTheme, StyledTheme } from './Styles'
import { GroupsProvider, LessonsProvider, LessonWorkProvider, StaticListsProvider } from './Store'

loadCldr(
  require('cldr-data/main/ru/numbers.json'),
  require('cldr-data/main/ru/ca-gregorian.json'),
  require('cldr-data/main/ru/timeZoneNames.json')
)

export const App = () => {
  const token = getToken()
  const account = getAccountType()
  
  return (
    <StaticListsProvider>
      <GroupsProvider>
        <LessonsProvider>
          <LessonWorkProvider>
            <MuiThemeProvider theme={MaterialTheme}>
              <ThemeProvider theme={StyledTheme}>
                <RootRouter isAuth={token} account={account}/>
              </ThemeProvider>
              <GlobalStyled/>
            </MuiThemeProvider>
            <ToastContainer
              newestOnTop
              theme="colored"
              hideProgressBar
              autoClose={2500}
              transition={Flip}
            />
          </LessonWorkProvider>
        </LessonsProvider>
      </GroupsProvider>
    </StaticListsProvider>
  )
}
