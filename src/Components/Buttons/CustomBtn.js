import React from 'react'
import { ButtonS } from './Styles'

export const CustomBtn = ({ title, onClick, color = "primary", ...props }) => {
  return (
    <ButtonS
      disableRipple
      color={color}
      children={title}
      onClick={onClick}
      variant="contained"
      style={props.style}
      {...props}
    />
  )
}
