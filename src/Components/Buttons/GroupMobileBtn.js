import React, { useEffect } from 'react'
import { MenuButton } from './MenuButton'
import { useGroupsDispatch, useGroupsStore, useLessonsDispatch, useLessonWorkDispatch } from '../../Store'
import { LinesWrap, Line, GroupName, SelectedGroupWrap } from './Styles'
import { getLessons } from '../../Api'

export const GroupMobileBtn = ({ filters }) => {
  const groupsDispatch = useGroupsDispatch()
  const lessonsDispatch = useLessonsDispatch()
  const lessonWorksDispatch = useLessonWorkDispatch()
  const { groups, selectedGroup } = useGroupsStore()
  
  const changeGroup = (group) => {
    groupsDispatch({
      selectedGroup: group,
      type: 'SET_SELECTED_GROUP_SUCCESS'
    })
    lessonWorksDispatch && lessonWorksDispatch({ type: 'CLEAR_SELECTED_WORKS' })
    lessonsDispatch && lessonsDispatch({ type: 'CLEAR_SELECTED_LESSON_SUCCESS' })
  }
  
  const getLessonsList = async () => {
    const apiData = await getLessons({ group: selectedGroup?.id, ...filters })
    
    apiData && lessonsDispatch({
      lessons: apiData.rows,
      type: 'SET_LESSONS_LIST_SUCCESS',
      pagination: {
        page: apiData.page,
        limit: apiData.limit,
        maxPages: Math.ceil(apiData.count / apiData.limit)
      }
    })
  }
  
  const handleClear = () => {
    groupsDispatch({ type: 'CLEAR_SELECTED_GROUP_SUCCESS' })
  }
  
  /* EFFECTS */
  useEffect(() => {
    selectedGroup !== undefined && getLessonsList()
    // eslint-disable-next-line
  }, [selectedGroup, filters])
  
  return (
    <MenuButton
      linksList={groups}
      clear={handleClear}
      linkClick={changeGroup}
      style={{ minWidth: 'auto' }}
      placeholder={{ title: 'Группы' }}
    >
      <LinesWrap>
        <Line/>
        <Line/>
        <Line/>
      </LinesWrap>
      {selectedGroup && (
        <SelectedGroupWrap>
          <GroupName children={selectedGroup.name}/>
        </SelectedGroupWrap>
      )}
    </MenuButton>
  )
}
