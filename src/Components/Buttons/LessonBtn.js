import React from 'react'
import { BtnIcon, BtnIconWrap, BtnText, LessonBtnMobile } from './Styles'

export const LessonBtn = ({ title, onClick, ...props }) => {
  return (
    <LessonBtnMobile disableRipple onClick={onClick} {...props}>
      <BtnIconWrap>
        <BtnIcon src="/assets/images/StartLessonIcon.svg" alt="Lesson start"/>
      </BtnIconWrap>
      <BtnText children={title}/>
    </LessonBtnMobile>
  )
}
