import React, { useRef, useState } from 'react'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import Button from '@material-ui/core/Button'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import { useStyles, ArrowDown, GrowS, LogoutText, MenuLink, MenuSelect, ProfileImg, UserName } from './Styles'

export const MenuButton = (props) => {
  const {
    id,
    title,
    clear,
    withIcon,
    children,
    withArrow,
    linksList,
    linkClick,
    eventData,
    placeholder,
    customStyles,
    placement = 'bottom',
    ...other
  } = props
  const classes = useStyles()
  const anchorRef = useRef(null)
  const [open, setOpen] = useState(false)

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen)
  }
  const handleClose = (e) => {
    if (anchorRef.current && anchorRef.current.contains(e.target)) {
      return
    }

    setOpen(false)
  }
  
  const handleItemClick = ({ event, link }) => (e) => {
    event && event(eventData)
    linkClick && linkClick(link)
    handleClose(e)
  }
  const handleListKeyDown = (e) => {
    e.preventDefault()
    setOpen(false)
  }
  const handleClear = (e) => {
    clear()
    handleClose(e)
  }
  
  return (
    <>
      <Button
        id={id}
        disableRipple
        ref={anchorRef}
        onClick={handleToggle}
        className={classes.dropdownBtnRoot}
        aria-controls={open ? id : undefined}
        startIcon={withIcon && <ProfileImg src="/assets/images/ProfileDefault.svg" alt="Profile"/>}
        endIcon={withArrow && <ArrowDown isMenuOpen={open} src="/assets/images/ArrowDown.svg" alt="Arrow"/>}
        {...other}
      >
        {children && children}
        {title && <UserName style={customStyles?.title} children={title}/>}
      </Button>
      <Popper
        transition
        open={open}
        disablePortal
        placement={placement}
        anchorEl={anchorRef.current}
        className={classes.popperRoot}
      >
        {({ TransitionProps, placement}) => (
          <GrowS {...TransitionProps} placement={placement} className={classes.dropdownListRoot}>
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList
                  id={id}
                  autoFocusItem={open}
                  onKeyDown={handleListKeyDown}
                  style={customStyles?.menuList}
                  className={classes.menuListRoot}
                >
                  {placeholder && (
                    <MenuItem disableRipple className={classes.menuListItemRoot}>
                      <MenuSelect isSelected selectedVal={placeholder.val}>
                        <span>{placeholder.title}</span>
                        <span>{placeholder.val}</span>
                      </MenuSelect>
                    </MenuItem>
                  )}
                  {clear && (
                    <MenuItem disableRipple className={classes.menuListItemRoot} onClick={handleClear}>
                      <MenuSelect selectedVal>
                        <span>Сбросить</span>
                      </MenuSelect>
                    </MenuItem>
                  )}
                  {placeholder?.val && (
                    <MenuItem
                      disableRipple
                      className={classes.menuListItemRoot}
                      onClick={handleItemClick({ link: { id: '' } })}
                    >
                      <MenuSelect>Убрать замену</MenuSelect>
                    </MenuItem>
                  )}
                  {linksList.map((link, index) => (
                    <MenuItem
                      key={index}
                      disableRipple
                      className={classes.menuListItemRoot}
                      onClick={handleItemClick({ event: link.event, link })}
                    >
                      {link.isLink ? (
                        <MenuLink to={`${link.path}${eventData ? eventData.id : ''}`} style={customStyles?.menuItemText}>
                          {link.title}
                        </MenuLink>
                      ) : link.isLogout ? (
                        <LogoutText style={customStyles?.menuItemText}>{link.title}</LogoutText>
                      ) : (
                        <MenuSelect style={{ ...customStyles?.menuItemText, ...link.style }}>
                          {link.format ? link.format(eventData || link) : (link.title || link.fullName || link.name)}
                        </MenuSelect>
                      )}
                    </MenuItem>
                  ))}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </GrowS>
        )}
      </Popper>
    </>
  )
}
