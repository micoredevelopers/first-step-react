import React, { useState } from 'react'
import { FiltersModal } from '../Modals'
import { Line, LinesWrap } from './Styles'

export const GlobalMobileFilters = ({ filters, update, filtersList, clear }) => {
  const [openFiltersModal, setOpenFiltersModal] = useState(false)
  
  const handleOpenFiltersModal = () => {
    setOpenFiltersModal(true)
  }
  const handleCloseFiltersModal = () => {
    setOpenFiltersModal(false)
  }
  
  return (
    <>
      <LinesWrap onClick={handleOpenFiltersModal}>
        <Line/>
        <Line/>
        <Line/>
      </LinesWrap>
  
      <FiltersModal
        clear={clear}
        submit={update}
        filters={filters}
        open={openFiltersModal}
        filtersList={filtersList}
        closeModal={handleCloseFiltersModal}
      />
    </>
  )
}

