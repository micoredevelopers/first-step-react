import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Button from '@material-ui/core/Button'

export const ButtonS = styled(Button)`
  width: 100%;
  height: 50px;
  display: flex;
  min-height: 50px;
  max-width: 220px;
  min-width: 220px;
  border-radius: 6px;
  align-items: center;
  justify-content: center;
  box-shadow: none !important;
  transition: all 0.5s ease-in-out;
  background-color: ${({ color }) => color === 'secondary' ? '#d51006' : '#003d7c'};
  
  &:hover {
    background-color: ${({ color }) => color === 'secondary' ? '#860600' : '#00244a'} !important;

    .MuiButton-label {
      color: #fff !important;
    }
  }
  
  &:disabled, &.Mui-disabled {
    opacity: 0.4;
    background-color: ${({ color }) => color === 'secondary' ? '#d51006' : '#003d7c'};
  }
  
  .MuiButton-label {
    color: #ffffff;
    font-size: 16px;
    text-transform: none;
    transition: all 0.5s ease-in-out;
    font-family: "Avenir Next Cyr", sans-serif;
  }
  
  ${down('md')} {
    height: 46px;
    min-height: 46px;
    
    .MuiButton-label {
      font-size: 15px;
    }
  }
`
