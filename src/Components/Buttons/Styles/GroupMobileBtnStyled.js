import styled from 'styled-components'

export const LinesWrap = styled.div`
  display: flex;
  padding: 10px;
  align-items: center;
  flex-direction: column;
`

export const Line = styled.span`
  height: 2px;
  border-radius: 1px;
  margin-bottom: 5px;
  background-color: ${({ theme }) => theme.colors.main};
  
  &:nth-child(1) {
    width: 20px;
  }
  
  &:nth-child(2) {
    width: 14px;
  }
  
  &:nth-child(3) {
    width: 6px;
  }
  
  &:last-child {
    margin-bottom: 0;
  }
`

export const SelectedGroupWrap = styled.div`
  left: 0;
  bottom: 0;
  width: 16px;
  height: 16px;
  display: flex;
  position: absolute;
  align-items: center;
  border-radius: 100%;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.main};
`

export const GroupName = styled.div`
  color: #fff;
  font-size: 11px;
  font-weight: 400;
  text-transform: uppercase;
  font-family: "Avenir Next Cyr";
`
