import styled from 'styled-components'
import Button from '@material-ui/core/Button'

export const LessonBtnMobile = styled(Button)`
  padding: 0;
  display: flex;
  align-items: center;
`

export const BtnIconWrap = styled.div`
  width: 32px;
  height: 32px;
  display: flex;
  border-radius: 100%;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.main};
`

export const BtnText = styled.span`
  font-size: 16px;
  font-weight: 400;
  margin-left: 10px;
  text-transform: none;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.main};
`

export const BtnIcon = styled.img`
  width: 13px;
  height: 12px;
`
