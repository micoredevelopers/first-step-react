import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Grow from '@material-ui/core/Grow'
import makeStyles from '@material-ui/core/styles/makeStyles'

const getMenuPlacement = (placement, position) => {
  if (placement.includes('start') && position === 'left') {
    return '15px'
  } else if (placement.includes('start') && position === 'right') {
    return 'auto'
  } else if (placement.includes('end') && position === 'left') {
    return 'auto'
  } else if (placement.includes('end') && position === 'right') {
    return '15px'
  } else if (position === 'left') {
    return '50%'
  }
}

export const useStyles = makeStyles(theme => ({
  dropdownBtnRoot: {
    padding: 0,
    width: 'auto',
    display: 'flex',
    minWidth: 'auto',
    alignItems: 'center',
    textTransform: 'none',
    justifyContent: 'space-between',
    '& .MuiButton-startIcon, & .MuiButton-endIcon': {
      margin: 0
    },
    '&:hover': {
      backgroundColor: 'transparent',
      '& .MuiButton-label': {
        '& span': {
          color: theme.palette.primary.main
        }
      }
    }
  },
  popperRoot: {
    zIndex: 10
  },
  dropdownListRoot: {
    width: 220,
    marginTop: 20,
    borderRadius: 6,
    position: 'relative',
    backgroundColor: '#ffffff',
    boxShadow: '0 0 12px 5px rgba(0, 12, 24, 0.1) !important',
    [theme.breakpoints.down('md')]: {
      width: 'auto',
      marginTop: 10
    }
  },
  menuListRoot: {
    height: 'auto',
    maxHeight: 150,
    overflow: 'auto',
    position: 'relative',
    padding: '0px !important',
    [theme.breakpoints.down('md')]: {
      maxHeight: 235
    }
  },
  menuListItemRoot: {
    minHeight: 'auto !important',
    borderBottom: '1px solid #e6e6e6',
    padding: '13px 20px 12px !important',
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  }
}))

export const ProfileImg = styled.img`
  width: 32px;
  height: 32px;
`

export const UserName = styled.span`
  color: #000c18;
  font-size: 16px;
  line-height: normal;
  text-transform: none;
  margin: 0 10px 0 15px;
  transition: all .35s ease-in-out;
  font-family: "Avenir Next Cyr", sans-serif;
`

export const ArrowDown = styled.img`
  width: 14px;
  height: 7px;
  transition: all 0.4s ease-in-out;
  transform: rotateX(${({ isMenuOpen }) => isMenuOpen ? 180 : 0}deg);
`

export const GrowS = styled(Grow)`
  position: relative;

  &:before {
    width: 0;
    height: 0;
    top: -9px;
    content: '';
    position: absolute;
    border-style: solid;
    border-width: 0 6px 9px 6px;
    border-color: transparent transparent #fff transparent;
     left: ${({ placement }) => getMenuPlacement(placement, 'left')};
    right: ${({ placement }) => getMenuPlacement(placement, 'right')};
    transform: ${({ placement }) => placement !== 'bottom' ? 'translateX(0)' : 'translateX(-50%)'};
  }
`

export const MenuLink = styled(Link)`
  width: 100%;
  color: #000c18;
  font-size: 16px;
  text-decoration: none;
  font-family: "Avenir Next Cyr";
`

export const LogoutText = styled.span`
  width: 100%;
  color: #d51006;
  font-size: 16px;
  text-align: center;
  border-bottom: none;
  text-decoration: none;
  font-family: "Avenir Next Cyr";
`

export const MenuSelect = styled.div`
  width: 100%;
  font-size: 16px;
  font-weight: 400;
  text-align: center;
  font-family: "Avenir Next Cyr";
  color: ${({ isSelected }) => isSelected ? '#bcbfc2' : '#000c18'};
  pointer-events: ${({ isSelected }) => isSelected ? 'none' : 'all'};
  
  span {
    font: inherit;
    
    &:nth-child(1) {
      margin-right: 5px;
      color: ${({ selectedVal }) => selectedVal ? '#000c18' : '#bcbfc2'}
    }
  }
`
