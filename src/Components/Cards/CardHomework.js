import React, {useEffect, useState} from 'react'
import { toast } from 'react-toastify'
import {CustomBtn} from "../Buttons/index";
import { InfoChip } from '../Custom'
import { ChipSelect } from '../Selects'
import { Marks } from '../../Constants'
import { FileModal, PhotoModal } from '../Modals'
import { formatDate, getAccountType } from '../../Helpers'
import { apiUrl, updateStudentLessonWork } from '../../Api'
import { useLessonWorkDispatch, useLessonWorkStore } from '../../Store'
import {
  Name,
  ChipIcon,
  CardWrap,
  PostDate,
  ChipsWrap,
  CardTopInfo,
  Description,
  CardMarkInfo,
  CardWrapInfo,
  HomeworkImage,
  CardBottomInfo,
  TopStudentInfo,
  NotSelectedText,
  NotSelectedWrap,
  HomeworkImageWrap, CommentForm, CommentInput
} from './Styles/CardHomeworkStyled'

export const CardHomework = ({ open, openModal, closeModal }) => {
  const account = getAccountType()
  const dispatch = useLessonWorkDispatch()
  const { lessonWork } = useLessonWorkStore()
  const today = lessonWork && new Date() === new Date(lessonWork.dateHomeWork)
  const [modalPhoto, setModalPhoto] = useState([])
  const [teacherComment, setTeacherComment] = useState('');

  const handleOpenModal = (src) => () => {
    setModalPhoto(src)
  }

  const handleCloseModal = () => {
    setModalPhoto([])
  }

  const handleChangeComment = (e) => {
    setTeacherComment(e.target.value)
  }

  const sendComment = (workId) => async (e) => {
    e.preventDefault()
    const filters = { teacherComment, homeworkMark: lessonWork?.homeworkMark}
    const apiData = await updateStudentLessonWork(workId, filters)

    if (apiData) {
      dispatch({
        workId: workId,
        updatedWork: apiData,
        type: 'UPDATE_LESSON_WORK_SUCCESS'
      })
      toast.success('Коментарий успешно отправлен!')
    } else {
      toast.error('Произошла ошибка при отправке коментария!')
    }
  }

  const changeMark = (workId) => async (e) => {
    e.stopPropagation()
    const { name, value } = e.target
    const filters = { [name]: value, teacherComment }
    const apiData = await updateStudentLessonWork(workId, filters)
    
    if (apiData) {
      dispatch({
        workId: workId,
        updatedWork: apiData,
        type: 'UPDATE_LESSON_WORK_SUCCESS'
      })
      toast.success('Оценка успешно выставлена!')
    } else {
      toast.error('Произошла ошибка при выставлении оценки!')
    }
  }

  useEffect(() => {
    setTeacherComment(lessonWork?.teacherComment || '')
  },[lessonWork])
  return (
    <CardWrap>
      {lessonWork ? (
        <>
          <CardWrapInfo>
            <CardTopInfo>
              <Name variant="h6">
                {lessonWork.lesson.teacher.fullName}
              </Name>
              <Description>{lessonWork.lesson.homeworkDescription}</Description>
              <ChipsWrap>
                {lessonWork.lesson.homeworkFilesImages.length > 0 && (
                  <InfoChip type="photo" onClick={handleOpenModal(lessonWork.lesson.homeworkFilesImages)}>
                    <ChipIcon src="/assets/images/PhotoIcon.svg" alt="Photo icon"/>
                    <span>Фото</span>
                  </InfoChip>
                )}
                {lessonWork.lesson.homeworkFilesNotImages.length > 0 && (
                  <InfoChip type="file" onClick={openModal('file')}>
                    <ChipIcon src="/assets/images/FileIcon.svg" alt="File icon"/>
                    <span>Файл</span>
                  </InfoChip>
                )}
              </ChipsWrap>
            </CardTopInfo>
            <CardBottomInfo>
              <TopStudentInfo>
                <Name variant="h6" style={{ marginBottom: 0 }}>{lessonWork.student.fullName}</Name>
                {lessonWork.dateHomeWork && (
                  <PostDate variant="h6">
                    {today ? 'Сегодня' : formatDate(lessonWork.dateHomeWork, 'dd MMM')},
                    в {formatDate(lessonWork.dateHomeWork, 'HH:mm')}
                  </PostDate>
                )}
              </TopStudentInfo>
              <HomeworkImageWrap>
                {lessonWork.homeworkFiles.length === 0 ?
                  <p>Ученик еще не прикрепил фото домашнего задания</p> :
                  lessonWork.homeworkFilesImages.map(file => (
                    <HomeworkImage onClick={handleOpenModal(lessonWork?.homeworkFilesImages)} key={file.id} src={`${apiUrl}/${file.path}`} alt="Homework"/>
                  ))
                }
              </HomeworkImageWrap>
            </CardBottomInfo>
            <CardBottomInfo>
              <CardMarkInfo>
                <Name variant="h6" style={{ marginBottom: 0 }}>Оценка</Name>
                <ChipSelect
                    options={Marks}
                    name="homeworkMark"
                    onChange={changeMark(lessonWork.id)}
                    // disabled={lessonWork.homeworkFiles.length === 0}
                    value={lessonWork.homeworkMark ? lessonWork.homeworkMark : ''}
                    placeholder={(lessonWork.homeworkFiles.length > 0 && !lessonWork.homeworkMark) ? '?' : '-'}
                />
              </CardMarkInfo>
            </CardBottomInfo>
            <CommentForm>
              <Name variant="h6" style={{ marginBottom: 0 }}>Коментарий к дз</Name>
              <CommentInput
                  name="teacherComment"
                  placeholder="Коментарий к дз"
                  onChange={handleChangeComment}
                  value={teacherComment}
                  max={300}
                  rows={6}
                  multiline
              />
              <CustomBtn type="submit" onClick={sendComment(lessonWork.id)} title='Отправить' style={{ alignSelf: 'end' }}/>
            </CommentForm>
          </CardWrapInfo>
    
          {modalPhoto.length > 0 && (
            <PhotoModal
              isPhotoFiles
              title="Фото"
              open={!!modalPhoto.length}
              close={handleCloseModal}
              slides={modalPhoto}
              subtitle="Фотографии, которые учитель прикрепил к домашнему заданию"
            />
          )}
          {lessonWork.lesson.homeworkFilesNotImages.length > 0 && (
            <FileModal
              open={open.file}
              close={closeModal}
              files={lessonWork.lesson.homeworkFilesNotImages}
            />
          )}
        </>
      ) : (
        <NotSelectedWrap>
          <NotSelectedText>
            {account === 'admin' ? 'Выберите ученика!' : 'Выберите домашнее задание и ученика'}
          </NotSelectedText>
        </NotSelectedWrap>
      )}
    </CardWrap>
  )
}
