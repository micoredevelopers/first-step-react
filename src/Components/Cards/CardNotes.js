import React from 'react'
import { NoteInput } from '../Inputs'
import { NotesWrap, NoteTitle, NoteInputsWrap } from './Styles/CardNotesStyled'

export const CardNotes = () => {
  return (
    <NotesWrap>
      <NoteTitle variant="h6">Заметки</NoteTitle>
      <NoteInputsWrap>
        <NoteInput/>
      </NoteInputsWrap>
    </NotesWrap>
  )
}
