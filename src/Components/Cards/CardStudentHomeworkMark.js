import MuiStyled from "@material-ui/core/styles/styled";
import React, {useEffect, useState} from 'react'
import _ from 'lodash'
import { toast } from 'react-toastify'
import {down} from "styled-breakpoints";
import styled from "styled-components";
import { InfoChip } from '../Custom'
import {FormInput} from "../Inputs/index";
import { FileModal, PhotoModal } from '../Modals'
import { formatDate, MobileView } from '../../Helpers'
import { useLessonWorkDispatch, useLessonWorkStore } from '../../Store/Context'
import { apiUrl, updateCurrentStudentLessonWork, uploadFiles } from '../../Api'
import { ArrowMain, LessonInfoTopRow, LessonName } from './Styles/LessonInfoStyled'
import {
  ChipIcon,
  ChipsWrap,
  CommentForm,
  Name,
  PostDate,
  TopStudentInfo
} from './Styles/CardHomeworkStyled'
import {
  CSHMWrap,
  TeacherName,
  HomeworkDesc,
  CSHMInfoWrap,
  HomeworkImage,
  CSHMUploadWrap,
  NotSelectedText,
  HomeworkImageWrap,
  UploadHomeworkBtn
} from './Styles/CardStudentHomeworkMarkStyled'


const EditHomework = styled(FormInput)`
  position: absolute;
  right: 0;
  bottom: 3px;
  max-width: unset;
  width: auto;
`

export const CardStudentHomeworkMark = ({ update }) => {
  const dispatch = useLessonWorkDispatch()
  const { lessonWork: homework } = useLessonWorkStore()
  const today = homework && new Date() === new Date(homework.dateHomeWork)
  const [openModal, setOpenModal] = useState({ photo: false, file: false })
  
  const handleUploadHomework = async (e) => {
    const input = e.target
    const files = Object.values(input.files).map(file => file)

    if (_.filter(files, (file) => file.type.includes('image')).length === 0) {
      toast.error('Загружен не верный формат файла! Доступные форматы файлов: .jpg, .jpeg, .png')
    } else {
      await uploadFiles(files)
        .then(async (res) => {
          if (res.data) {
            const tokens = res.data.token ? res.data.token : res.data.map(file => file.token)
            const apiData = await updateCurrentStudentLessonWork(homework.lesson.id, tokens)
            if (apiData) {
              toast.success('Домашнее задание успешно отправлено!')
              dispatch({
                updatedWork: apiData,
                type: 'UPDATE_LESSON_WORK_SUCCESS'
              })
              update()
            } else {
              toast.error('При отправке домашнего задания произошла ошибка!')
            }
          } else {
            toast.error('Ошибка при загрузке файлов!')
          }
        })
    }
  }
  
  const goToLessons = () => {
    dispatch({ type: 'BACK_TO_LESSONS' })
  }
  
  /* MODALS */
  const handleOpenModal = (modal) => (e) => {
    e.stopPropagation()
    setOpenModal(prev => ({ ...prev, [modal]: true }))
  }
  const handleCLoseModal = () => {
    setOpenModal({ photo: false, file: false })
  }

  return (
    <>
      <MobileView>
        <LessonInfoTopRow onClick={goToLessons} style={{ padding: '0 15px' }}>
          <ArrowMain src="/assets/images/ArrowLeftMain.svg" alt="Arrow"/>
          <LessonName variant="h2" children="Оценки и дз"/>
        </LessonInfoTopRow>
      </MobileView>
      
      <CSHMWrap>
        {homework ? (
          <>
            <CSHMInfoWrap>
              <TeacherName variant="h3">
                {homework.lesson.teacher.lastName} {homework.lesson.teacher.name} {homework.lesson.teacher.middleName}
              </TeacherName>
              {homework.lesson.homeworkDescription && (
                <HomeworkDesc children={homework.lesson.homeworkDescription}/>
              )}
              <ChipsWrap>
                {homework.lesson.homeworkFilesImages.length > 0 && (
                  <InfoChip type="photo" onClick={handleOpenModal('photo')}>
                    <ChipIcon src="/assets/images/PhotoIcon.svg" alt="Photo icon"/>
                    <span>Фото</span>
                  </InfoChip>
                )}
                {homework.lesson.homeworkFilesNotImages.length > 0 && (
                  <InfoChip type="file" onClick={handleOpenModal('file')}>
                    <ChipIcon src="/assets/images/FileIcon.svg" alt="File icon"/>
                    <span>Файл</span>
                  </InfoChip>
                )}
              </ChipsWrap>
            </CSHMInfoWrap>
            <CSHMUploadWrap>
              {homework.homeworkFiles.length > 0 ? (
                <>
                  <TopStudentInfo>
                    <Name variant="h6" style={{ marginBottom: 0 }}>
                      {homework.student.lastName} {homework.student.name}
                    </Name>
                    <PostDate variant="h6">
                      {today ? 'Сегодня' : formatDate(homework.dateHomeWork, 'dd MMM')},
                      в {formatDate(homework.dateHomeWork, 'HH:mm')}
                    </PostDate>
                    <EditHomework
                      type="file"
                      name="file"
                      accept="image/*"
                      onChange={handleUploadHomework}
                      icon="/assets/images/EditIcon.svg"
                    />
                  </TopStudentInfo>
                  {homework.homeworkFiles.map(file => (
                    <HomeworkImageWrap key={file.id}>
                      <HomeworkImage src={`${apiUrl}/${file.path}`} alt={file.path}/>
                    </HomeworkImageWrap>
                  ))}
                  {homework?.teacherComment &&
                    <CommentForm>
                      <Name variant="h6" style={{ marginBottom: 0 }}>Коментарий к дз</Name>
                      <HomeworkDesc>{homework.teacherComment}</HomeworkDesc>
                    </CommentForm>
                  }
                </>
              ) : (
                <UploadHomeworkBtn
                  multiple
                  type="file"
                  name="files"
                  accept="image/*"
                  label="Прикрепить ответ"
                  style={{ marginBottom: 0 }}
                  onChange={handleUploadHomework}
                />
              )}
            </CSHMUploadWrap>
          </>
        ) : (
          <NotSelectedText children="Выберите оценку для просмотра комментария"/>
        )}
      </CSHMWrap>
      
      {homework && (
        <>
          <PhotoModal
            title="Фото"
            isPhotoFiles
            open={openModal.photo}
            close={handleCLoseModal}
            slides={homework.lesson.homeworkFilesImages}
            subtitle="Фотографии, которые учитель прикрепил к домашнему заданию"
          />
          <FileModal
            open={openModal.file}
            close={handleCLoseModal}
            files={homework.lesson.homeworkFilesNotImages}
          />
        </>
      )}
    </>
  )
}
