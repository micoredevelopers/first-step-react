import React from 'react'
import {
  CSIWrap,
  CSITopInfo,
  StudentName,
  StudentLetter,
  CSILessonsWrap,
  CSILessonRow,
  LessonName,
  LessonStatusIcon
} from './Styles/CardStudentInfoStyled'

export const CardStudentInfo = ({ student }) => {
  return (
    <CSIWrap>
      <CSITopInfo>
        <StudentLetter children={student.name.split('')[0]}/>
        <StudentName variant="h3">{student.fullName}</StudentName>
      </CSITopInfo>
      <CSILessonsWrap>
        {student.lessonWorks.length > 0 && student.lessonWorks.map(lesson => (
          <CSILessonRow key={lesson.id}>
            <LessonName variant="h4" children={lesson.lesson.subject}/>
            <LessonStatusIcon
              alt={lesson.absent ? 'Absent' : 'Present'}
              src={`/assets/images/${lesson.absent ? 'AbsentIcon' : 'PresentIcon'}.svg`}
            />
          </CSILessonRow>
        ))}
      </CSILessonsWrap>
    </CSIWrap>
  )
}
