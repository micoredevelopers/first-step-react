import React from 'react'
import { InfoChip } from '../Custom'
import {
  CSMBtn,
  CSMWrap,
  CSMMark,
  CSMDate,
  CSMTitle,
  DateArrow,
  CSMTopInfo,
  CSMContent,
  CSMDateRow,
  CSMSubjectName
} from './Styles/CardStudentMarkStyled'
import { DesktopView, formatDate, MobileView } from '../../Helpers'
import { useLessonWorkDispatch, useLessonWorkStore } from '../../Store/Context'

/* CSM - Card Student Mark */
export const CardStudentMark = ({ lesson }) => {
  const workDispatch = useLessonWorkDispatch()
  const { lessonWork } = useLessonWorkStore()
  const subject = lesson.subject || lesson.lesson.subject
  const mark = !lesson.type ? lesson.homeworkMark : lesson.currentStudentWork?.lessonMark
  
  const handleSelectHomework = () => {
    workDispatch({
      lessonWork: lesson,
      type: 'GET_LESSON_WORK_SUCCESS'
    })
  }
  
  return (
    <CSMWrap>
      <MobileView>
        <CSMTopInfo>
          <CSMTitle variant="h2" children={!lesson.type ? 'Дом. задание' : lesson.type === 'TYPE_DEFAULT' ? 'Урок' : lesson.typeName}/>
        </CSMTopInfo>
      </MobileView>
      
      <DesktopView>
        <CSMTopInfo>
          <CSMTitle variant="h2" children={subject}/>
          {!lesson.type ? (
            <InfoChip
              type="st_mark"
              children="ДЗ"
              tooltip="Домашнее задание"
              style={{ marginRight: 0, minWidth: 'auto' }}
            />
          ) : (
            <InfoChip
              type="st_mark"
              style={{ marginRight: 0 }}
              tooltip={lesson.type === 'TYPE_DEFAULT' ? 'Урок' : lesson.typeName}
              children={lesson.type === 'TYPE_DEFAULT' ? 'У' : lesson.typeName?.split('')[0]}
            />
          )}
        </CSMTopInfo>
      </DesktopView>
      <CSMContent>
        {!lesson.type ? (
          <>
            <CSMMark variant="h3" value={mark ?? '-'} children={mark ?? '-'}/>
            <CSMDateRow>
              {lesson.createdAt && (
                <CSMDate>
                  <DateArrow src="/assets/images/DateArrowUp.svg" alt="Arrow"/>
                  <span>{formatDate(lesson.createdAt, 'dd MMMM')}</span>
                </CSMDate>
              )}
              {lesson.dateHomeWork && (
                <CSMDate>
                  <DateArrow down src="/assets/images/DateArrowUp.svg" alt="Arrow"/>
                  <span>{formatDate(lesson.dateHomeWork, 'dd MMMM')}</span>
                </CSMDate>
              )}
            </CSMDateRow>
            <MobileView>
              <CSMSubjectName children={subject}/>
            </MobileView>
            <CSMBtn
              title="Подробнее"
              onClick={handleSelectHomework}
              className={lessonWork?.id === lesson.id && 'active'}
            />
          </>
        ) : (
          <>
            <CSMMark variant="h3" value={mark ?? '-'} children={mark ?? '-'}/>
            <CSMDate>
              <span>{formatDate(lesson.date, 'dd MMMM')}</span>
            </CSMDate>
            <MobileView>
              <CSMSubjectName children={subject}/>
            </MobileView>
          </>
        )}
      </CSMContent>
    </CSMWrap>
  )
}
