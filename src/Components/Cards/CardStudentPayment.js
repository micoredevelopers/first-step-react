import React, { useState } from 'react'
import { toast } from 'react-toastify'
import { PhotoModal } from '../Modals'
import { updatePayment, uploadFiles } from '../../Api'
import { checkDevice, formatDate } from '../../Helpers'
import {
  BtnS,
  CSPWrap,
  PaymentBtn,
  PaymentIcon,
  PaymentData,
  PaymentMonth,
  UploadCheckBtn,
  PaymentBtnsRow,
  PaymentStatusWrap
} from './Styles/CardStudentPaymentStyled'

export const CardStudentPayment = ({ payment, update }) => {
  const [openModal, setOpenModal] = useState(false)
  const status = payment.status === 'new' && payment.receiptFile ? 'wait' : payment.status

  const handleClick = () => {
    if (payment.status === 'paid') {
      setOpenModal(true)
    }
  }
  const handleCloseModal = () => {
    setOpenModal(false)
  }
  
  const uploadPayment = async (e) => {
    const input = e.target
    const id = input.name.split('_')[1]

    if (input.files && input.files[0]) {
      if (input.files[0].type.includes('image')) {
        await uploadFiles([input.files[0]])
          .then(async (res) => {
            if (!res.errors) {
              const apiData = await updatePayment({ paymentId: id, file: res.data.token })

              if (apiData) {
                toast.success('Чек успешно отправлен!')
                update()
              }
            } else {
              toast.error('Ошибка при отправке чека!')
            }
          })
      } else {
        toast.error('Загружен не верный формат файла! Доступные форматы файлов: .jpg, .jpeg, .png')
      }
    }
  }
  
  const getStatusIcon = (status) => {
    switch (status) {
      case 'wait':
        return 'PaymentWait'
      case 'paid':
        return 'PaymentAccepted'
      case 'new':
      case 'canceled':
        return 'PaymentCancel'
      default:
        return 'PaymentCancel'
    }
  }
  
  const getPaymentBtnTitle = () => {
    switch (status) {
      case 'paid':
        if (payment.receiptFile) {
          return 'Посмотреть чек'
        } else {
          return 'Оплачено наличкой'
        }
      case 'wait':
        return 'Чек на проверке'
      default: return ''
    }
  }
  
  return (
    <CSPWrap>
      <PaymentStatusWrap status={status}>
        <PaymentIcon src={`/assets/images/${getStatusIcon(status)}.svg`} alt={getStatusIcon(status)}/>
      </PaymentStatusWrap>
      <PaymentMonth variant="h2">
        {formatDate(payment.payDate, 'MMMM yyyy')}
      </PaymentMonth>
      <PaymentData variant="h4" style={{ marginBottom: 5 }}>
        {`Нужно оплатить до ${formatDate(payment.payDate, 'dd.MM.yy')}`}
      </PaymentData>
      <PaymentData variant="h4" style={{ marginBottom: 15 }}>
        {status === 'canceled' ? 'Оплата отклонена' :
          (status === 'new' ? 'Не оплачено' :
            `${checkDevice('min-lg') ? 'Опл.' : 'Оплачено'}
              ${formatDate(payment.receiptDate, 'dd.MM.yy')}`)}
      </PaymentData>
      {status !== 'paid' ? (
        <PaymentBtnsRow>
          <UploadCheckBtn
            type="file"
            style={BtnS}
            accept="image/*"
            onChange={uploadPayment}
            name={`payment_${payment.id}`}
            label={checkDevice('min-lg') ? <img src="/assets/images/CheckIcon.svg" alt="Check"/> : 'Чек'}
          />
        </PaymentBtnsRow>
      ) : (
        <PaymentBtn
          status={status}
          onClick={handleClick}
          title={getPaymentBtnTitle()}
          disabled={!payment.receiptFile}
        />
      )}
      {openModal && (
        <PhotoModal
          title="Чек"
          isPhotoFiles
          open={openModal}
          close={handleCloseModal}
          slides={payment.receiptFile ? [payment.receiptFile] : []}
        />
      )}
    </CSPWrap>
  )
}
