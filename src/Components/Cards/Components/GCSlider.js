import React from 'react'
import Slider from 'react-slick'
import styled from 'styled-components'

export const CustomSlider = styled(Slider)`
  .slick-slide {
    &>div {
      width: 100%;
      padding: 0 15px;
    }
  }
  
  .slick-dots {
    bottom: 0;
    width: 100%;
    margin-top: 20px;
    position: relative;
    align-items: center;
    justify-content: center;
    display: flex !important;
    
    li {
      width: 8px;
      height: 8px;
      margin: 0 5px;
      min-width: 8px;
      
      &.slick-active {
        button {
          &:before {
            opacity: 1;
            color: ${({ theme }) => theme.colors.main}
          }
        }
      }
      
      button {
        padding: 0;
        width: 100%;
        height: 100%;
        
        &:before {
          width: 8px;
          height: 8px;
          font-size: 8px;
          line-height: normal;
          opacity: 1;
          color: ${({ theme }) => theme.colors.lightGrey}
        }
      }
    }
  }
`

const SliderSettings = {
  dots: true,
  speed: 700,
  arrows: false,
  infinite: false,
  slidesToShow: 1,
  slidesToScroll: 1
}

export const GCSlider = ({ children }) => {
  return (
    <CustomSlider {...SliderSettings}>
      {children}
    </CustomSlider>
  )
}

