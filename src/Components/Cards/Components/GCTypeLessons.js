import React, { useState } from 'react'
import { InfoChip } from '../../Custom'
import { ChipSelect } from '../../Selects'
import { Marks } from '../../../Constants'
import { formatDate } from '../../../Helpers'
import { FileModal, PhotoModal } from '../../Modals'
import { ChipIcon } from '../Styles/CardHomeworkStyled'
import { ArrowLink, GCChipsWrap, GCDescription, GCLessonName, GCTopWrap, GCActionsWrap, ActionIcon } from '../Styles/GeneralCardStyled'

export const GCTypeLessons = (props) => {
  const {
    data,
    isLink,
    onEdit,
    showType,
    showData,
    onDelete,
    withEdit,
    showGroup,
    showFiles,
    isHomework,
    withDelete,
    showComment,
    showChipMark,
    withoutDescription
  } = props
  const [openModal, setOpenModal] = useState({ photo: null, file: null })
  /* MODALS EVENTS */
  const handleOpenModal = (modal, id) => (e) => {
    e.stopPropagation()
    setOpenModal(prev => ({ ...prev, [modal]: { [id]: true } }))
  }
  const handleCLoseModal = () => {
    setOpenModal({ photo: null, file: null })
  }
  const todayDate = new Date()
  const tomorrowDate = new Date(todayDate)
  tomorrowDate.setDate(tomorrowDate.getDate() + 1)
  const today = formatDate(todayDate, 'ddMMyyyy') === formatDate(data.date || data.createdAt, 'ddMMyyyy')
  const tomorrow = formatDate(tomorrowDate, 'ddMMyyyy') === formatDate(data.date || data.createdAt, 'ddMMyyyy')
  return (
    <>
      <GCTopWrap>
        <GCLessonName variant="h3">
          {isHomework && <span>Урок:</span>}
          {data.subject ?? data.lesson?.subject}
        </GCLessonName>
        <GCActionsWrap>
          {withEdit && <ActionIcon onClick={onEdit(data)} className="action-icon" src="/assets/images/EditIcon.svg" alt="Edit"/>}
          {withDelete && <ActionIcon onClick={onDelete(data)} className="action-icon" src="/assets/images/Delete.svg" alt="Delete"/>}
          {isLink && <ArrowLink src="/assets/images/ArrowRightBlack.svg" alt="Link"/>}
        </GCActionsWrap>
      </GCTopWrap>
      
      {!withoutDescription && (
        <GCDescription className={isLink ? 'mb-0' : ''}>
          {isHomework ? data.homeworkDescription : !data.type ? data.lesson.homeworkDescription : data.description}
        </GCDescription>
      )}
      
      <GCChipsWrap>
        {showComment && (!data.type && data.homeworkFiles.length > 0) && (
          <InfoChip type="group" children="Ком." style={{ minWidth: 75 }}/>
        )}
        {showGroup && (
          <InfoChip type="group" children={data.group?.name ?? data.lesson?.group.name}/>
        )}
        {showFiles && (
          <>
            {data.homeworkFilesImages.length > 0 && (
              <InfoChip type="photo" onClick={handleOpenModal('photo', data.id)}>
                <ChipIcon src="/assets/images/PhotoIcon.svg" alt="Photo icon"/>
                <span>Фото</span>
              </InfoChip>
            )}
            {data.homeworkFilesNotImages.length > 0 && (
              <InfoChip type="file" onClick={handleOpenModal('file', data.id)}>
                <ChipIcon src="/assets/images/FileIcon.svg" alt="File icon"/>
                <span>Файл</span>
              </InfoChip>
            )}
          </>
        )}
        {showData && (
          <InfoChip type="date" style={{ minWidth: today || tomorrow ? 'auto' : 160 }}>
            {today ? 'Сегодня в' : tomorrow ? 'Завтра в' : ''} {today || tomorrow ?
              formatDate(data.date || data.createdAt, 'HH:mm') :
                formatDate(data.date || data.createdAt, 'dd MMM, HH:mm')}
          </InfoChip>
        )}
        {showType && (
          <InfoChip
            type="type"
            tooltip={!data.type ? 'Дом. задание' : (data.typeName || data.lesson.typeName)}
          >
            {!data.type ? 'ДЗ' : (data.typeName || data.lesson.typeName).split('')[0]}
          </InfoChip>
        )}
        {showChipMark && (
          <ChipSelect
            noevent
            options={Marks}
            name={data.dateHomeWork ? 'homeworkMark' : 'lessonMark'}
            value={data.dateHomeWork ? data.homeworkMark : data.lessonMark}
            placeholder={(data.dateHomeWork && data.homeworkFilesImages.length > 0) ? '?' : '-'}
          />
        )}
      </GCChipsWrap>
      
      {showFiles && data.homeworkFilesImages.length > 0 && (
        <PhotoModal
          isPhotoFiles
          title="Фото"
          close={handleCLoseModal}
          slides={data.homeworkFilesImages}
          subtitle="Фотографии, которые учитель прикрепил к домашнему заданию"
          open={(openModal.photo && openModal.photo[data.id]) ? openModal.photo[data.id] : false}
        />
      )}
      {showFiles && data.homeworkFilesNotImages.length > 0 && (
        <FileModal
          close={handleCLoseModal}
          files={data.homeworkFilesNotImages}
          open={(openModal.file && openModal.file[data.id]) ? openModal.file[data.id] : false}
        />
      )}
    </>
  )
}
