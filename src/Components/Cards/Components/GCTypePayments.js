import React, { useState } from 'react'
import styled from 'styled-components'
import { toast } from 'react-toastify'
import { updatePayment } from '../../../Api'
import { formatDate } from '../../../Helpers'
import { ask, PhotoModal } from '../../Modals'
import { PaymentBtn } from '../Styles/CardStudentPaymentStyled'
import { GCTopWrap, GCLessonName, GCDescription } from '../Styles/GeneralCardStyled'

export const GCLeftWrap = styled.div`

`

export const GCRightWrap = styled.div`

`

export const PaymentModalButtons = ({ data, update, closeModal }) => {
  return [
    {
      title: 'Отклонить',
      color: 'secondary',
      event: async () => {
        const apiData = await updatePayment({ paymentId: data.id, status: 'canceled' })
      
        if (apiData) {
          update()
          closeModal()
          toast.success('Платеж отклонен!')
        }
      }
    },
    {
      title: 'Подтвердить',
      event: async () => {
        const apiData = await updatePayment({ paymentId: data.id, status: 'paid', file: data.receiptFile?.token  })
      
        if (apiData) {
          update()
          closeModal()
          toast.success('Платеж подтвержден!')
        }
      }
    }
  ]
}

export const GCTypePayments = ({ data, update, ...props }) => {
  const [openModal, setOpenModal] = useState(false)
  const getPaidStatus = () => {
    switch (data.status) {
      case 'paid':
        if (data.receiptFile) {
          return 'paidCheck'
        } else {
          return 'paidCash'
        }
      case 'new':
        if (data.receiptFile) {
          return 'needCheck'
        } else {
          return 'new'
        }
      case 'canceled':
        return 'canceled'
      default: return null
    }
  }
  const getPaidBtnTitle = () => {
    switch (getPaidStatus()) {
      case 'new':
      case 'paidCash':
        return 'Оплачено наличкой'
      case 'needCheck':
        return 'Проверить чек'
      case 'paidCheck':
        return 'Посмотреть чек'
      case 'canceled':
        return 'Отклонен'
      default: return ''
    }
  }
  
  const handleClick = () => {
    if (getPaidStatus() === 'paidCheck' || getPaidStatus() === 'needCheck') {
      setOpenModal(true)
    } else if (getPaidStatus() === 'new') {
      ask({
        title: 'Оплата',
        subTitle: 'Вы уверены что хотите изменить статус платежа?',
        btnTitle: 'Подтверить',
        callback: async () => {
          const apiData = await updatePayment({ paymentId: data.id, status: 'paid' })
  
          if (apiData) {
            update()
            toast.success('Платеж подтвержден!')
          }
        }
      })
    }
  }
  const handleCloseModal = () => {
    setOpenModal(false)
  }

  return (
    <>
      <GCTopWrap isStudents={props.isStudents}>
        <GCLeftWrap>
          <GCLessonName style={{ textTransform: 'capitalize' }}>
            {formatDate(data.payDate, 'MMMM yyyy')}
          </GCLessonName>
          {!!data.statusDate && (
            <GCDescription style={{ marginBottom: 0 }}>
              {`Оплачено ${formatDate(data.statusDate, 'dd.MM.yy')}`}
            </GCDescription>
          )}
        </GCLeftWrap>
        <GCRightWrap>
          <PaymentBtn
            onClick={handleClick}
            status={getPaidStatus()}
            title={getPaidBtnTitle()}
            disabled={getPaidStatus() === 'paidCash' || getPaidStatus() === 'canceled'}
          />
        </GCRightWrap>
      </GCTopWrap>
      <PhotoModal
        title="Чек"
        isPhotoFiles
        open={openModal}
        close={handleCloseModal}
        slides={data.receiptFile ? [data.receiptFile] : []}
        btnOptions={getPaidStatus() === 'needCheck' ? PaymentModalButtons({ data, update, closeModal: handleCloseModal }) : []}
      />
    </>
  )
}

