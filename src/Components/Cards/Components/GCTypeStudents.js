import React from 'react'
import { toast } from 'react-toastify'
import { ChipSelect } from '../../Selects'
import { Marks } from '../../../Constants'
import { useLessonWorkDispatch } from '../../../Store'
import { updateStudentLessonWork } from '../../../Api'
import { ArrowLink, GCChipsMarkWrap, GCLessonName, GCTopWrap } from '../Styles/GeneralCardStyled'
import { InfoChip } from '../../Custom'

export const GCTypeStudents = ({ data, showMarks, showType, showAbsent, isLink, isStudents, isHomework, viewOnly }) => {
  const lessonWorkDispatch = useLessonWorkDispatch()
  
  /* CHANGE EVENTS */
  const handleChangeMark = (work) => async (e) => {
    e.stopPropagation()
    const { name, value } = e.target
    const filters = {
      [name]: value,
      [isHomework ? 'lessonMark' : 'homeworkMark']: isHomework ? work.lessonMark : work.homeworkMark
    }
    const apiData = await updateStudentLessonWork(work.id, filters)
    
    if (apiData) {
      lessonWorkDispatch({
        workId: work.id,
        updatedWork: apiData,
        type: 'UPDATE_LESSON_WORK_SUCCESS'
      })
      toast.success('Оценка успешно выставлена!')
    } else {
      toast.error('Произошла ошибка при выставлении оценки!')
    }
  }
  
  return (
    <>
      <GCTopWrap isStudents={isStudents}>
        <GCLessonName variant="h3">
          {showType ? data.lesson.subject : (data.fullName || data.student.fullName)}
        </GCLessonName>
        
        {showMarks && (
          <GCChipsMarkWrap>
            {showType && (
              <InfoChip
                type="type"
                tooltip={data.lesson.typeName}
                children={data.lesson.typeName.split('')[0]}
              />
            )}
            {showAbsent && (
              <ChipSelect
                name="absent"
                placeholder="-"
                noevent={viewOnly}
                value={data.absent ? '1' : '0'}
                onChange={handleChangeMark(data)}
                options={[ { name: 'Б', value: '0' }, { name: 'Н/Б', value: '1' } ]}
              />
            )}
            <ChipSelect
              options={Marks}
              noevent={viewOnly}
              onChange={handleChangeMark(data)}
              isAbsent={!isHomework && data.absent}
              disabled={!isHomework && data.absent}
              name={isHomework ? 'homeworkMark' : 'lessonMark'}
              value={isHomework ? data.homeworkMark : data.lessonMark}
              placeholder={(isHomework && data.homeworkFiles.length > 0) ? '?' : '-'}
            />
          </GCChipsMarkWrap>
        )}
        
        {isLink && <ArrowLink src="/assets/images/ArrowRightBlack.svg" alt="Link"/>}
      </GCTopWrap>
    </>
  )
}
