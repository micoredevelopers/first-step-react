import React, { useState, useEffect } from 'react'
import _ from 'lodash'
import { checkDevice } from '../../Helpers'
import { getLessons, getStudentLessonWork } from '../../Api'
import { useGroupsStore, useLessonsStore, useLessonWorkDispatch } from '../../Store'
import { GCTypeLessons, GCTypePayments, GCTypeStudents, GCSlider } from './Components'
import { GCWrap, GCTitle, GCContentWrap, GCInnerContentWrap, EmptyText } from './Styles/GeneralCardStyled'

export const GeneralCard = (props) => {
  const {
    filters,
    cardData,
    cardTitle,
    linkEvent,
    isStudents,
    lessonType,
    isSelected,
    withMobileSlider,
    withoutGroupFilters,
    ...other
  } = props
  const { lessons } = useLessonsStore()
  const { selectedGroup } = useGroupsStore()
  const { lessonWork } = useLessonWorkDispatch()
  const [maxPages, setMaxPages] = useState(1)
  const [filteredMarks, setFilteredMarks] = useState([])
  const [filteredLessons, setFilteredLessons] = useState([])
  const [filteredHomework, setFilteredHomework] = useState([])
  const [pagination, setPagination] = useState({ page: 1, limit: 10 })
  /* CUSTOM FUNCTIONS */
  const checkFilteredLessons = () => {
    switch (lessonType) {
      case 'lessons':
        return (other.cardType === 'lessons' && !filters) ? lessons : filteredLessons
      case 'homework':
        return filteredHomework
      case 'marks':
        return filteredMarks
      default:
        return cardData || []
    }
  }
  
  const getCardContent = ({ cardType, data, ...props }) => {
    switch (cardType) {
      case 'lessons':
        return <GCTypeLessons data={data} {...props}/>
      case 'students':
        return <GCTypeStudents data={data} {...props}/>
      case 'payments':
        return <GCTypePayments data={data} {...props}/>
      default:
        return null
    }
  }
  
  /* API REQUEST */
  const getFilteredLessons = async (isPagination) => {
    if (pagination.page <= maxPages) {
      const apiData = await getLessons({
        page: pagination.page,
        limit: pagination.limit,
        group: !withoutGroupFilters ? selectedGroup?.id : '',
        ...filters
      })
  
      if (apiData?.rows) {
        setMaxPages(Math.ceil(apiData.count / apiData.limit))
        
        if (isPagination) {
          setFilteredLessons(prev => _.unionBy(prev, apiData.rows, 'id'))
        } else {
          setFilteredLessons(apiData.rows)
          setPagination(prev => ({ ...prev, page: 1 }))
        }
      }
    }
  }
  const getFilteredHomework = async (isPagination) => {
    if (pagination.page <= maxPages) {
      const apiData = await getLessons({
        page: pagination.page,
        limit: pagination.limit,
        group: !withoutGroupFilters ? selectedGroup?.id : '',
        ...filters
      })
  
      if (apiData?.rows) {
        const homeworkList = apiData.rows.filter(work => !work.type)
  
        setMaxPages(Math.ceil(apiData.count / apiData.limit))
        
        if (homeworkList.length === 0) {
          const teacherHomeworks = apiData.rows.filter(work => work.dateHomeWork)
  
          if (isPagination) {
            setFilteredHomework(prev => _.unionBy(prev, teacherHomeworks, 'id'))
          } else {
            setFilteredHomework(teacherHomeworks)
            setPagination(prev => ({ ...prev, page: 1 }))
          }
        } else {
          if (isPagination) {
            setFilteredHomework(prev => _.unionBy(prev, homeworkList, 'id'))
          } else {
            setFilteredHomework(homeworkList)
            setPagination(prev => ({ ...prev, page: 1 }))
          }
        }
      }
    }
  }
  const getFilteredMarks = async (isPagination) => {
    if (pagination.page <= maxPages) {
      const apiData = await getStudentLessonWork({
        page: pagination.page,
        limit: pagination.limit,
        groups: !withoutGroupFilters ? selectedGroup?.id : '',
        ...filters
      })
  
      if (apiData) {
        setMaxPages(Math.ceil(apiData.count / apiData.limit))
  
        if (isPagination) {
          setFilteredHomework(prev => _.unionBy(prev, apiData, 'id'))
        } else {
          setFilteredMarks(apiData)
          setPagination(prev => ({ ...prev, page: 1 }))
        }
      }
    }
  }
  
  const filteredData = ({ isPagination }) => {
    switch (lessonType) {
      case 'lessons':
        getFilteredLessons(isPagination)
        break
      case 'homework':
        getFilteredHomework(isPagination)
        break
      case 'marks':
        getFilteredMarks(isPagination)
        break
      default: return
    }
  }
  
  const handleScroll = (e) => {
    const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
    
    if (bottom) {
      if (pagination.page < maxPages) {
        setPagination(prev => ({ ...prev, page: pagination.page + 1 }))
      }
    }
  }
  
  /* EFFECTS */
  useEffect(() => {
    filteredData({ isPagination: true })
  }, [pagination])
  useEffect(() => {
    filteredData({ isPagination: false })
    //eslint-disable-next-line
  }, [lessons, lessonWork, selectedGroup, lessonType, filters?.student])
  
  return (
    <GCWrap onScroll={handleScroll} className={other.className} withCardTitle={cardTitle} isSlider={withMobileSlider}>
      {cardTitle && <GCTitle variant="h2" className={withMobileSlider ? 'p-15' : ''} children={cardTitle}/>}
      
      {checkFilteredLessons().length > 0 ? (
        <>
          {withMobileSlider && checkDevice('min-lg') ? (
            <GCSlider>
              {checkFilteredLessons().map((data, index) => (
                <GCContentWrap key={index} onClick={linkEvent && linkEvent(data)}>
                  <GCInnerContentWrap>
                    {getCardContent({ data, isStudents, ...other })}
                  </GCInnerContentWrap>
                </GCContentWrap>
              ))}
            </GCSlider>
          ) : checkFilteredLessons().map((data, index) => (
            <GCContentWrap
              key={index}
              isStudents={isStudents}
              onClick={linkEvent && linkEvent(data)}
              className={isSelected === data.id ? 'is-selected' : ''}
            >
              <GCInnerContentWrap>
                {getCardContent({ data, isStudents, ...other })}
              </GCInnerContentWrap>
            </GCContentWrap>
          ))}
        </>
      ) : (
        <EmptyText>Данные отсутствуют</EmptyText>
      )}
    </GCWrap>
  )
}
