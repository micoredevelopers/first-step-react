import React, { useState } from 'react'
import { ModalInnerContent } from '../Modals'
import { StudentLetter } from './Styles/CardStudentInfoStyled'
import {
  ICWrap,
  ICText,
  EditBtn,
  ICTitle,
  ICLeftWrap,
  ICRightWrap,
  ICDetailedWrap
} from './Styles/InfoCardStyled'


export const InfoCard = ({ info, ModalContent, modalProps, contentProps }) => {
  const [openModal, setOpenModal] = useState(false)
  
  const handleOpenModal = () => {
    setOpenModal(true)
  }
  const handleCloseModal = () => {
    setOpenModal(false)
  }
  
  return (
    <>
      <ICWrap>
        <ICLeftWrap>
          <StudentLetter style={{ margin: '0 20px 0 0' }} children={info.name.split('')[0]}/>
          <ICDetailedWrap>
            <ICTitle variant="h2" children={info.fullName}/>
            <ICText variant="h5" children={info.email}/>
            <ICText variant="h5" children={info.phone}/>
          </ICDetailedWrap>
        </ICLeftWrap>
        <ICRightWrap>
          <EditBtn src="/assets/images/EditIcon.svg" alt="Edit" onClick={handleOpenModal}/>
        </ICRightWrap>
      </ICWrap>
      
      <ModalInnerContent open={openModal} close={handleCloseModal} {...modalProps}>
        <ModalContent close={handleCloseModal} {...contentProps}/>
      </ModalInnerContent>
    </>
  )
}

