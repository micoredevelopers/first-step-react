import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'
import {FormInput} from "../../Inputs/index";

export const CardWrap = styled.div`
  height: 100%;
  overflow: auto;
  position: relative;
  
  ${down('md')} {
    min-height: 140px;
    background-color: transparent;
    transition: transform 0.5s ease-in-out 0s;
    
    &.is-changed {
      transition-delay: 1s;
      transform: translateX(0);
    }
  }
`

export const CardWrapInfo = styled.div`
  height: 100%;
  padding: 30px;
  overflow: auto;
  border-radius: 16px;
  background-color: #ffffff;

  ${down('md')} {
    padding: 20px;
    border-radius: 16px;
    background-color: #fff;
  }
`

export const CardTopInfo = styled.div`
  display: flex;
  margin-bottom: 15px;
  padding-bottom: 20px;
  flex-direction: column;
  border-bottom: 1px solid #e6e6e6;
`

export const Name = styled(Typography)`
  font-size: 20px;
  margin-bottom: 10px;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 18px;
  }
`

export const Description = styled(Typography)`
  font-size: 16px;
  line-height: 22px;
  margin-bottom: 20px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 15px;
  }
`

export const ChipsWrap = styled.div`
  display: flex;
  overflow: auto;
  align-items: center;
`

export const ChipIcon = styled.img`
  width: 16px;
  height: 14px;
  margin-right: 5px;
`

export const CardBottomInfo = styled.div`
  display: flex;
  margin-bottom: 15px;
  padding-bottom: 20px;
  flex-direction: column;
  border-bottom: 1px solid #e6e6e6;
`

export const TopStudentInfo = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 10px;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
`

export const PostDate = styled(Typography)`
  font-size: 14px;
  font-weight: 400;
  text-align: left;
  line-height: 22px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
`

export const HomeworkImageWrap = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  
  p {
    color: #8f9398;
    font-size: 16px;
    line-height: 22px;
    font-family: "Avenir Next Cyr";
  }
`

export const HomeworkImage = styled.img`
  width: 100%;
  margin-bottom: 15px;
  object-fit: contain;
`

export const CardMarkInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const CommentForm = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const CommentInput = styled(FormInput)`
  max-width: 100%;
  border: 1px solid #e6e6e6;
  border-radius: 6px;
  margin: 12px 0; 
`

export const StudentNameWrap = styled.div`
  display: flex;
  padding: 0 15px;
  align-items: center;
  margin-bottom: 20px;
  justify-content: flex-start;
`

export const NotSelectedWrap = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  position: absolute;
  border-radius: 16px;
  background-color: #fff;
`

export const NotSelectedText = styled.p`
  top: 50%;
  left: 50%;
  z-index: 1;
  width: 200px;
  font-size: 16px;
  font-weight: 400;
  line-height: 22px;
  text-align: center;
  position: absolute;
  font-family: "Avenir Next Cyr";
  transform: translate(-50%, -50%);
  color: ${({ theme }) => theme.colors.grey};
  
  ${down('lg')} {
    font-size: 15px;
  }
`
