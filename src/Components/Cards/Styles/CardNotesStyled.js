import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'

export const NotesWrap = styled.div`
  height: 100%;
  padding: 30px;
  overflow: auto;
  //min-height: 235px;
  position: relative;
  border-radius: 16px;
  background-size: cover;
  background: url('/assets/images/NoteLayout.png') repeat center;
  
  ${down('md')} {
    padding: 30px 15px;
  }
`

export const NoteTitle = styled(Typography)`
  font-size: 28px;
  font-weight: 700;
  margin-bottom: 0;
  font-family: Comfortaa;
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 20px;
  }
`

export const NoteInputsWrap = styled.div`
  display: flex;
  height: calc(100% - 45px);
`
