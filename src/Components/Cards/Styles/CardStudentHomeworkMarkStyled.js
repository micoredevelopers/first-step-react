import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'
import MuiStyled from '@material-ui/core/styles/styled'
import { FormInput } from '../../Inputs'

export const CSHMWrap = styled.div`
  width: 100%;
  overflow: auto;
  position: relative;
  border-radius: 16px;
  height: calc(100% - 65px);
  background-color: #ffffff;
  
  ${down('md')} {
    height: auto;
    margin-bottom: 20px;
  }
`

export const NotSelectedText = styled.p`
  top: 50%;
  left: 50%;
  z-index: 1;
  width: 200px;
  font-size: 16px;
  font-weight: 400;
  line-height: 22px;
  text-align: center;
  position: absolute;
  font-family: "Avenir Next Cyr";
  transform: translate(-50%, -50%);
  color: ${({ theme }) => theme.colors.grey};
`

export const CSHMInfoWrap = styled.div`
  display: flex;
  margin-bottom: 20px;
  padding: 30px 30px 0;
  flex-direction: column;
  justify-content: space-between;
  
  ${down('md')} {
    padding: 20px 20px 0;
    margin-bottom: 20px;
  }
`

export const TeacherName = styled(Typography)`
  font-size: 20px;
  font-weight: 500;
  margin-bottom: 10px;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 18px;
    margin-bottom: 20px;
  }
`

export const HomeworkDesc = styled.p`
  font-size: 16px;
  font-weight: 400;
  line-height: 22px;
  margin-bottom: 20px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 15px;
  }
`

export const HomeworkImageWrap = styled.div`
  width: 100%;
  height: 350px;
  margin-bottom: 15px;
`

export const HomeworkImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`

export const CSHMUploadWrap = styled.div`
  width: 100%;
  padding: 20px;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  border-top: 1px solid #e6e6e6;
`

export const UploadHomeworkBtn = MuiStyled(FormInput)(({ theme }) => ({
  height: 40,
  width: '100%',
  
  '& label': {
    fontSize: 14,
    color: '#fff',
    width: '100%',
    height: '100%',
    display: 'flex',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.palette.primary.main,
    border: `1px solid ${theme.palette.primary.main}`
  },
  [theme.breakpoints.down('md')]: {
    maxWidth: 210
  }
}))
