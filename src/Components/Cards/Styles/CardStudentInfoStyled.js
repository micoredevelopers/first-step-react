import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'

export const CSIWrap = styled.div`
  overflow: auto;
  max-height: 255px;
  border-radius: 16px;
  background-color: #ffffff;
`

export const CSITopInfo = styled.div`
  display: flex;
  align-items: center;
  padding: 30px 0 15px;
  flex-direction: column;
`

export const StudentLetter = styled.div`
  width: 80px;
  height: 80px;
  display: flex;
  color: #ffffff;
  min-width: 80px;
  font-size: 30px;
  align-items: center;
  margin-bottom: 22px;
  border-radius: 100%;
  justify-content: center;
  text-transform: uppercase;
  font-family: "Avenir Next Cyr";
  background-color: ${({ theme }) => theme.colors.main};
  
  ${down('md')} {
    width: 60px;
    height: 60px;
    font-size: 23px;
    min-width: 60px;
    margin-bottom: 12px;
  }
`

export const StudentName = styled(Typography)`
  font-size: 20px;
  font-weight: 500;
  text-align: center;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 16px;
  }
`

export const CSILessonsWrap = styled.div`
  display: flex;
  flex-direction: column;
`

export const CSILessonRow = styled.div`
  display: flex;
  padding: 11px 15px;
  align-items: center;
  border-top: 1px solid #e6e6e6;
  justify-content: space-between;
  
  ${down('md')} {
    padding: 10px;
  }
`

export const LessonName = styled(Typography)`
  font-size: 16px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 14px;
  }
`

export const LessonStatusIcon = styled.img`
  width: 15px;
  height: 15px;
`
