import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'
import { CustomBtn } from '../../Buttons'
import { setStatusColor } from '../../Selects'

export const CSMWrap = styled.div`
  height: 235px;
  border-radius: 16px;
  background-color: #fff;
`

export const CSMMark = styled(Typography)`
  font-size: 48px;
  font-weight: 700;
  margin-bottom: 10px;
  font-family: Comfortaa;
  color: ${props => setStatusColor(props)};
  
  ${down('md')} {
    font-size: 40px;
    margin-bottom: 5px;
  }
`

export const CSMDate = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  span {
    font-size: 16px;
    text-align: center;
    font-family: "Avenir Next Cyr";
    color: ${({ theme }) => theme.colors.grey};
  }
  
  ${down('md')} {
    margin-bottom: 10px;
    
    span {
      font-size: 12px;
    }
  }
`

export const CSMTitle = styled(Typography)`
  color: #ffffff;
  font-size: 16px;
  font-weight: 400;
  max-width: 146px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  text-transform: capitalize;
  font-family: "Avenir Next Cyr";
  
  ${down('md')} {
    max-width: 100%;
    font-size: 15px;
    font-weight: 700;
    white-space: normal;
    font-family: Comfortaa;
  }
`

export const DateArrow = styled.img`
  width: 7px;
  height: 11px;
  margin-right: 8px;
  transform: rotate(${({ down }) => down ? 180 : 0}deg);
  
  ${down('md')} {
    width: 5px;
    height: 8px;
    margin-right: 5px;
  }
`

export const CSMTopInfo = styled.div`
  display: flex;
  padding: 7px 13px;
  flex-direction: row;
  align-items: center;
  border-radius: 16px 16px 0 0;
  justify-content: space-between;
  background-color: ${({ theme }) => theme.colors.main};
  
  ${down('md')} {
    padding: 13px 10px 10px;
    justify-content: center;
  }
`

export const CSMContent = styled.div`
  display: flex;
  padding: 0 15px;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  height: calc(100% - 44px);
  
  ${down('md')} {
    height: calc(100% - 41px);
  }
`

export const CSMDateRow = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 25px;
  justify-content: space-between;
  
  ${down('md')} {
    margin-bottom: 0;
  }
`

export const CSMSubjectName = styled(Typography)`
  font-size: 14px;
  overflow: hidden;
  text-align: center;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  text-overflow: ellipsis;
  -webkit-box-orient: vertical;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.black};
`

export const CSMBtn = styled(CustomBtn)`
  width: 100%;
  height: 40px;
  max-width: 90%;
  min-width: auto;
  min-height: 40px;
  background-color: transparent !important;
  border: 1px solid ${({ theme }) => theme.colors.main};
  
  &:hover {
    background-color: ${({ theme }) => theme.colors.main} !important;
  
    span {
      color: #fff !important;
    }
  }
  
  &.active {
    background-color: ${({ theme }) => theme.colors.main} !important;
  
    span {
      color: #fff !important;
    }
  }
  
  span {
    transition: all 0.4s ease-in-out;
    color: ${({ theme }) => theme.colors.main} !important;
  }
  
  ${down('md')} {
    margin-top: 25px;
  }
`
