import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'
import MuiStyled from '@material-ui/core/styles/styled'
import { CustomBtn } from '../../Buttons'
import { FormInput } from '../../Inputs'

const getStatusColor = ({ status, theme, isBackground }) => {
  switch (status) {
    case 'paid':
      return theme.colors.main
    case 'wait':
      return isBackground ? 'transparent' : theme.colors.warning
    case 'new':
    case 'canceled':
      return isBackground ? 'transparent' : theme.colors.error
    default: return theme.colors.error
  }
}

export const CSPWrap = styled.div`
  display: flex;
  border-radius: 16px;
  align-items: center;
  flex-direction: column;
  padding: 30px 30px 20px;
  background-color: #ffffff;
  
  ${down('md')} {
    padding: 30px 15px 15px;
  }
`

export const PaymentStatusWrap = styled.div`
  width: 80px;
  height: 80px;
  display: flex;
  border-radius: 100%;
  align-items: center;
  margin-bottom: 22px;
  justify-content: center;
  border: 1px solid ${props => getStatusColor(props)};
  background-color: ${props => getStatusColor({ isBackground: true, ...props })};
  
  ${down('md')} {
    width: 60px;
    height: 60px;
    margin-bottom: 12px;
  }
`

export const PaymentIcon = styled.img`
  width: auto;
  height: auto;
  
  ${down('md')} {
    height: 18px;
  }
`

export const PaymentMonth = styled(Typography)`
  font-size: 20px;
  font-weight: 500;
  margin-bottom: 10px;
  text-transform: capitalize;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 16px;
  }
`

export const PaymentData = styled(Typography)`
  font-size: 16px;
  font-weight: 400;
  line-height: 22px;
  text-align: center;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
  
  ${down('md')} {
    font-size: 12px;
    line-height: normal;
    margin-bottom: 15px;
  }
`

export const PaymentBtn = styled(CustomBtn)`
  width: 100%;
  height: 40px;
  margin: 0 auto;
  min-height: 40px;
  max-width: 195px;
  min-width: auto;
  border: 1px solid ${({ theme }) => theme.colors.main};
  background-color: ${({ status, theme }) => (status === 'needCheck' || status === 'new') ? theme.colors.main : 'transparent'} !important;
  
  span {
    font-size: 14px !important;
    color: ${({ status, theme }) => (status === 'needCheck' || status === 'new') ? '#fff' : theme.colors.main} !important;
  }
  
  ${down('md')} {
    height: 32px;
    min-width: auto;
    min-height: 32px;
  
    span {
      font-size: 12px !important;
    }
  }
`

export const BtnS = {
  width: '100%',
  minWidth: '100%',
  maxWidth: '100%'
}

export const UploadCheckBtn = MuiStyled(FormInput)(({ theme }) => ({
  height: 40,
  width: '100%',
  maxWidth: 220,
  marginBottom: 0,
  '&:hover': {
    '& label': {
      borderColor: '#00244a',
      backgroundColor: '#00244a'
    }
  },
  '& label': {
    fontSize: 14,
    color: '#fff',
    width: '100%',
    height: '100%',
    display: 'flex',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    transition: 'all 0.5s ease-in-out',
    backgroundColor: theme.palette.primary.main,
    border: `1px solid ${theme.palette.primary.main}`
  },
  [theme.breakpoints.down('md')]: {
    height: 32,
    
    '& label': {
      fontSize: '12px !important'
    }
  }
}))

export const PaymentBtnsRow = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`
