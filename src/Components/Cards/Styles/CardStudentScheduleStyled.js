import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'

export const CardStudentWrap = styled.div`
  width: 115%;
  flex-grow: 1;
  overflow: auto;
  max-height: 235px;
  border-radius: 16px;
  margin-bottom: 30px;
  padding: 40px 40px 0;
  background-color: #fff;
  
  ${down('md')} {
    width: 100%;
    max-height: none;
    margin-bottom: 20px;
    padding: 30px 0 20px;
  }
`

export const CardStudentTitle = styled(Typography)`
  font-size: 28px;
  font-weight: 700;
  margin-bottom: 20px;
  font-family: Comfortaa;
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 18px;
    padding: 0 15px;
  }
`

export const CardStudentContentWrap = styled.div`
  width: 100%;
  margin-bottom: 15px;
  border-bottom: 1px solid #e6e6e6;
  cursor: ${({ isHomework }) => isHomework ? 'pointer': 'default'};
  
  ${down('md')} {
    margin-bottom: 0;
    padding-bottom: 20px;
  }
`

export const CardStudentContentTop = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  margin-bottom: 10px;
  align-items: center;
  justify-content: space-between;
`

export const CardStudentContentTitle = styled(Typography)`
  color: #000c18;
  font-size: 20px;
  font-weight: 500;
  font-family: "Avenir Next Cyr Medium";
  
  ${down('md')} {
    font-size: 18px;
  }
`

export const CardStudentContentChips = styled.div`
  display: flex;
  overflow: auto;
  flex-direction: row;
  align-items: center;
`

export const CardStudentContentDescription = styled(Typography)`
  color: #8f9398;
  font-size: 16px;
  font-weight: 400;
  line-height: 22px;
  margin-bottom: 25px;
  font-family: "Avenir Next Cyr";
  
  ${down('md')} {
    margin-bottom: 20px;
  }
`

export const RightArrow = styled.img`
  width: 16px;
  margin: 8px;
  height: 16px;
  min-width: 16px;
  transform: rotate(-90deg);
`
