import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'

export const EmptyText = styled(Typography)`
  top: 50%;
  left: 50%;
  z-index: 1;
  font-size: 16px;
  text-align: center;
  position: absolute;
  font-family: "Avenir Next Cyr";
  transform: translate(-50%, -50%);
  color: ${({ theme }) => theme.colors.grey};
  
  ${down('lg')} {
    font-size: 15px;
  }
`

export const GCWrap = styled.div`
  height: 100%;
  overflow-y: auto;
  overflow-x: hidden;
  position: relative;
  border-radius: 16px;
  background-color: #ffffff;
  padding: ${({ withCardTitle }) => withCardTitle ? '30px' : '15px 30px 30px'};
  
  ${down('lg')} {
    max-height: 50vh;
    min-height: 140px;
    padding: ${({ withCardTitle, isSlider }) => withCardTitle ? `30px ${isSlider ? '0' : '15px'}` : `0 15px 20px`};
  }
`

export const GCTitle = styled(Typography)`
  font-size: 28px;
  font-weight: 700;
  margin-bottom: 0;
  font-family: Comfortaa;
  color: ${({ theme }) => theme.colors.black};
  
  ${down('lg')} {
    font-size: 20px;
    
    &.p-15 {
      padding: 0 15px;
    }
  }
`

export const ActionIcon = styled.img`
  opacity: 0;
  width: 15px;
  height: 15px;
  cursor: pointer;
  margin-left: 15px;
  visibility: hidden;
  transition: all 0.4s ease-in-out;
`

export const GCContentWrap = styled.div`
  position: relative;
  border-bottom: 1px solid #e6e6e6;
  cursor: ${({ onClick }) => onClick ? 'pointer' : 'default'};
  padding: ${({ isStudents }) => isStudents ? '15px 0' : '15px 0 20px'};
  
  &.is-selected {
    .action-icon {
      opacity: 1;
      visibility: visible;
    }
  
    &:before {
      top: 0;
      right: 0;
      bottom: 0;
      z-index: 0;
      content: '';
      left: -30px;
      height: 100%;
      position: absolute;
      width: calc(100% + 60px);
      background-color: rgba(0, 61, 124, 0.06);
    }
  }
`

export const GCInnerContentWrap = styled.div`
  z-index: 1;
  position: relative;
`

export const GCTopWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: ${({ isStudents }) => isStudents ? '0px' : '10px'};
`

export const GCActionsWrap = styled.div`
  display: flex;
  align-items: center;
`

export const GCLessonName = styled(Typography)`
  font-size: 20px;
  position: relative;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
  
  span {
    font-size: 16px;
    margin-right: 10px;
    font-family: "Avenir Next Cyr";
    color: ${({ theme }) => theme.colors.grey};
  }
  
  ${down('lg')} {
    font-size: 18px;
    
    span {
      font-size: 14px;
    }
  }
`

export const GCChipsMarkWrap = styled.div`
  display: flex;
  align-items: center;
`

export const ArrowLink = styled.img`
  width: 8px;
  height: 16px;
`

export const GCDescription = styled(Typography)`
  font-size: 16px;
  line-height: 22px;
  margin-bottom: 15px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
  
  ${down('lg')} {
    font-size: 15px;
    
    &.mb-0 {
      margin-bottom: 0;
    }
  }
`

export const GCChipsWrap = styled.div`
  width: 100%;
  display: flex;
  overflow: auto;
  justify-content: flex-start;
`
