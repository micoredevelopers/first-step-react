import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'

export const ICWrap = styled.div`
  height: 140px;
  padding: 30px;
  display: flex;
  margin-bottom: 30px;
  border-radius: 16px;
  align-items: center;
  background-color: #ffffff;
  justify-content: space-between;
  
  ${down('lg')} {
    padding: 15px;
    height: 100px;
    margin-bottom: 15px;
  }
`

export const ICLeftWrap = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  align-content: center;
`

export const ICRightWrap = styled.div`

`

export const ICDetailedWrap = styled.div`
  display: flex;
  flex-direction: column;
`

export const ICTitle = styled(Typography)`
  font-size: 20px;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('lg')} {
    font-size: 18px;
  }
`

export const ICText = styled(Typography)`
  font-size: 16px;
  line-height: 22px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
  
  ${down('lg')} {
    font-size: 15px;
  }
`

export const EditBtn = styled.img`
  width: 32px;
  height: 30px;
  min-width: 32px;
  cursor: pointer;
  
  ${down('lg')} {
    width: 16px;
    height: 15px;
    min-width: 16px;
  }
`
