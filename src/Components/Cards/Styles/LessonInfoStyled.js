import styled from 'styled-components'
import Typography from '@material-ui/core/Typography'

export const LessonInfoWrap = styled.div`
  padding: 0 15px;
  margin-bottom: 20px;
`

export const LessonInfoTopRow = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  justify-content: flex-start;
`

export const ArrowMain = styled.img`
  width: 10px;
  height: 20px;
  min-width: 10px;
  margin-right: 10px;
`

export const LessonName = styled(Typography)`
  font-size: 26px;
  font-weight: 700;
  font-family: Comfortaa;
  color: ${({ theme }) => theme.colors.main};
`

export const LessonInfoDesc = styled(Typography)`
  font-size: 15px;
  margin-bottom: 20px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
`
