import React from 'react'
import Grid from '@material-ui/core/Grid'
import { CardStudentInfo } from '../Cards'
import { useStudentsStore } from '../../Store/Context'
import { checkDevice } from '../../Helpers'

export const StudentClassContainer = () => {
  const { students } = useStudentsStore()
  
  return (
    <Grid item xs={12}>
      <Grid container spacing={checkDevice('min-lg') ? 2 : 6}>
        {students.length > 0 && students.map(student => (
          <Grid key={student.id} item xs={6} lg={3}>
            <CardStudentInfo student={student}/>
          </Grid>
        ))}
      </Grid>
    </Grid>
  )
}
