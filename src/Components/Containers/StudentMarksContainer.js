import React, { useEffect, useState } from 'react'
import Grid from '@material-ui/core/Grid'
import styled from '@material-ui/core/styles/styled'
import { CardStudentHomeworkMark, CardStudentMark } from '../Cards'
import { useLessonsStore, useLessonWorkStore } from '../../Store'
import { checkDevice } from '../../Helpers'

const MobileS = checkDevice('min-lg') && ({
  container: {
    padding: '0',
    position: 'relative'
  },
  item: {
    transition: 'transform 0.5s ease-in-out',
    
    '&.lesson-grid': {
      position: 'relative',
      transitionDelay: '0.5s',
      transform: 'translateX(0%)',
      
      '&.is-changed': {
        marginTop: 111,
        transitionDelay: '0s',
        transform: `translateX(calc(-100% - 15px))`
      }
    },
    '&.homework-grid': {
      zIndex: 1,
      position: 'absolute',
      transitionDelay: '0s',
      transform: `translateX(calc(-100% - 15px))`,
      
      '&.is-changed': {
        transitionDelay: '0.5s',
        transform: `translateX(0%)`
      }
    }
  }
})

const SMGridContainer = styled(Grid)({
  height: '100%',
  ...MobileS.container
})

const SMGridItem = styled(Grid)({
  top: 0,
  left: 0,
  right: 0,
  overflow: 'auto',
  height: 'calc(100% + 15px)',
  ...MobileS.item
})

export const StudentMarksContainer = ({ update }) => {
  const [page, setPage] = useState(1)
  const { lessons } = useLessonsStore()
  const { pagination, showLessons, showHomework } = useLessonWorkStore()
  
  const handleScroll = async (e) => {
    const h = e.target.offsetHeight
    const scrollT = e.nativeEvent.target.scrollTop
    const scrollB = scrollT + h
    const el = e.target.children[0]
  
    if (scrollB === el.offsetHeight) {
      if (page !== pagination.maxPages) {
        setPage(page + 1)
        update({ page: page + 1 })
      }
    }
  }
  
  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    
    return (
      window.removeEventListener('scroll', handleScroll)
    )
    // eslint-disable-next-line
  }, [])
  
  return (
    <SMGridContainer container spacing={checkDevice('min-lg') ? 2 : 6} onScroll={handleScroll}>
      <SMGridItem item xs={12} lg={8} className={`lesson-grid ${!showLessons ? 'is-changed' : ''}`}>
        <Grid container spacing={checkDevice('min-lg') ? 2 : 6}>
          {lessons.length > 0 && lessons.map((lesson, index) => (
            <Grid key={index} item xs={6} lg={4}>
              <CardStudentMark lesson={lesson}/>
            </Grid>
          ))}
        </Grid>
      </SMGridItem>
      <SMGridItem item xs={12} lg={4} className={`homework-grid ${showHomework ? 'is-changed' : ''}`}>
        <CardStudentHomeworkMark update={update}/>
      </SMGridItem>
    </SMGridContainer>
  )
}
