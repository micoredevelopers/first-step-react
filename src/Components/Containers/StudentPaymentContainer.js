import React, { useEffect } from 'react'
import { toast } from 'react-toastify'
import Grid from '@material-ui/core/Grid'
import { getPayments } from '../../Api'
import { CardStudentPayment } from '../Cards'
import { usePaymentsStore, usePaymentsDispatch } from '../../Store'
import { checkDevice } from '../../Helpers'

export const StudentPaymentContainer = ({ filters }) => {
  const dispatch = usePaymentsDispatch()
  const { payments } = usePaymentsStore()
  
  const update = async () => {
    const apiData = await getPayments(filters)
  
    if (apiData) {
      const sortedPayments = apiData.rows.sort((prev, next) => (
        new Date(next.payDate) - new Date(prev.payDate)
      ))

      dispatch({
        payments: sortedPayments,
        type: 'SET_PAYMENTS_LIST_SUCCESS'
      })
    } else {
      toast.error('Ошибка при загрузке списка платежей!')
    }
  }
  
  useEffect(() => {
    update()
    // eslint-disable-next-line
  }, [filters])
  
  return (
    <Grid container spacing={checkDevice('min-lg') ? 2 : 6}>
      {!!payments?.length && payments.map(payment => (
        <Grid key={payment.id} item xs={6} lg={3}>
          <CardStudentPayment update={update} payment={payment}/>
        </Grid>
      ))}
    </Grid>
  )
}
