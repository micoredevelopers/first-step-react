import React from 'react'
import { GroupListWrap, SelectedGroup, ClearBtn, LightTooltip } from './Styles'
import { useGroupsDispatch, useGroupsStore, useLessonsDispatch, useLessonWorkDispatch } from '../../Store'

export const GroupsList = () => {
  const dispatch = useGroupsDispatch()
  const lessonsDispatch = useLessonsDispatch()
  const dispatchLessonWork = useLessonWorkDispatch()
  const { groups, selectedGroup } = useGroupsStore()
  
  const changeGroup = (group) => () => {
    dispatch({
      selectedGroup: group,
      type: 'SET_SELECTED_GROUP_SUCCESS'
    })
    dispatchLessonWork && dispatchLessonWork({ type: 'CLEAR_SELECTED_WORKS' })
    lessonsDispatch && lessonsDispatch({ type: 'CLEAR_SELECTED_LESSON_SUCCESS' })
  }
  const clearGroups = () => {
    dispatch({ type: 'CLEAR_SELECTED_GROUP_SUCCESS' })
  }
  
  return groups.length > 0 && (
    <>
      <GroupListWrap>
        {groups.map((group, index) => group.name.length > 1 ? (
          <LightTooltip key={index} title={group.name} placement="top" arrow>
            <SelectedGroup onClick={changeGroup(group)} isGroupSelected={selectedGroup?.id === group.id}>
              <span>{group.name}</span>
            </SelectedGroup>
          </LightTooltip>
        ) : (
          <SelectedGroup key={index} onClick={changeGroup(group)} isGroupSelected={selectedGroup?.id === group.id}>
            <span>{group.name}</span>
          </SelectedGroup>
        ))}
      </GroupListWrap>
      {selectedGroup && <ClearBtn onClick={clearGroups}>Сбросить</ClearBtn>}
    </>
  )
}
