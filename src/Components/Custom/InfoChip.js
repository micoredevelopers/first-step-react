import React from 'react'
import { LightTooltip, ChipWrap } from './Styles'

export const InfoChip = ({ type, children, tooltip, ...props }) => {
  return tooltip ? (
    <LightTooltip title={tooltip} placement="top" arrow>
      <ChipWrap type={type} {...props}>
        <span>{children}</span>
      </ChipWrap>
    </LightTooltip>
  ) : (
    <ChipWrap type={type} {...props}>
      <span>{children}</span>
    </ChipWrap>
  )
}
