import React, { useState } from 'react'
import { useLocation } from 'react-router'
import { makeStyles } from '@material-ui/core/styles'
import { FormInput, FormSwitch } from '../Inputs'
import { GroupsList } from './GroupsList'
import { SectionTitle } from './SectionTitle'
import { ModalInnerContent } from '../Modals'
import { useTeachersStore } from '../../Store'
import { checkDevice, DesktopView, formatDate, MobileView } from '../../Helpers'
import { CustomBtn, GroupMobileBtn, GlobalMobileFilters } from '../Buttons'
import {
  GridR,
  LeftWrap,
  TitleRow,
  RightWrap,
  ContainerR,
  ContentGridR,
  MobileAddBtn,
  SectionLayout,
  TitleLeftWrap,
  TitleRightWrap
} from './Styles'
import { FormSelect } from '../Selects'
import { MonthList } from '../../Constants'

const useStyles = makeStyles({
  searchInput: {
    marginBottom: 0,

    '& img': {
      left: 20,
      zIndex: 1,
      width: 12,
      height: 12,
      top: '50%',
      minWidth: 12,
      position: 'absolute',
      transform: 'translateY(-50%)'
    }
  }
})

export const SectionLayoutBase = (props) => {
  const {
    title,
    customS,
    filters,
    children,
    btnTitle,
    btnProps,
    ModalProps,
    withSwitch,
    withSearch,
    modalSubmit,
    filtersList,
    ModalContent,
    handleSearch,
    handleSwitch,
    handleSelect,
    clearFilters,
    updateFilters,
    noBottomOffset,
    withGroupFilters,
    withSelectFilter,
    withGlobalFilters
  } = props
  const classes = useStyles()
  const Location = useLocation()
  const currentMonth = formatDate(new Date(), 'yyyy-MM')
  const { selectedTeacher } = useTeachersStore()
  const [open, setOpen] = useState(false)
  const [search, setSearch] = useState('')
  const [select, setSelect] = useState(currentMonth)
  const showGroups = (Location.pathname === '/admin/schedule' && checkDevice('xl')) ? selectedTeacher : withGroupFilters
  /* MODAL EVENTS */
  const openModal = () => {
    setOpen(true)
  }
  const closeModal = () => {
    setOpen(false)
  }
  const submitClose = () => {
    setOpen(false)
    modalSubmit()
  }
  
  /* SEARCH EVENT */
  const handleSearchChange = (e) => setSearch(e.target.value)
  const handleSwitchChange = (data) => handleSwitch(data)
  const handleSelectChange = (e) => {
    handleSelect(e.target)
    setSelect(e.target.value)
  }
  
  return (
    <>
      <SectionLayout style={customS?.sectionR}>
        <ContainerR maxWidth={false} style={customS?.containerR}>
          <GridR style={customS?.gridR}>
            <DesktopView>
              {showGroups && (
                <LeftWrap>
                  <GroupsList filters={filters}/>
                </LeftWrap>
              )}
            </DesktopView>
  
            <RightWrap withGroupFilters={showGroups} noBottomOffset={noBottomOffset} withTitle={title}>
              <TitleRow style={customS?.titleRow} isTitle={title}>
                {title && (
                  <TitleLeftWrap>
                    <SectionTitle variant="h1" style={{ marginBottom: 0, ...customS?.title }}>
                      {title}
                    </SectionTitle>
                  </TitleLeftWrap>
                )}
    
                <TitleRightWrap style={customS?.titleRightWrap}>
                  {withGlobalFilters && (
                    <MobileView>
                      <GlobalMobileFilters update={updateFilters} filters={filters} filtersList={filtersList} clear={clearFilters}/>
                    </MobileView>
                  )}
                  {showGroups && (
                    <MobileView>
                      <GroupMobileBtn filters={filters}/>
                    </MobileView>
                  )}
                  {btnTitle && (
                    <>
                      <DesktopView>
                        <CustomBtn title={btnTitle} onClick={openModal} {...btnProps}/>
                      </DesktopView>
                      <MobileView>
                        <MobileAddBtn title={<img src="/assets/images/AddIcon.svg" alt="Add"/>} onClick={openModal}/>
                      </MobileView>
                    </>
                  )}
                  {withSwitch && (
                    <FormSwitch
                      label="Несданное дз"
                      name="withoutStudentLessonWork"
                      handleChange={handleSwitchChange}
                    />
                  )}
                  {withSelectFilter && (
                    <FormSelect
                      name="month"
                      value={select}
                      options={MonthList}
                      style={{ width: 220 }}
                      clearValue={currentMonth}
                      placeholder="Выберите месяц:"
                      onChange={handleSelectChange}
                    />
                  )}
                  {withSearch && (
                    <FormInput
                      name="search"
                      value={search}
                      placeholder="Поиск"
                      onKeyPress={handleSearch}
                      onChange={handleSearchChange}
                      className={classes.searchInput}
                      icon="/assets/images/SearchIcon.svg"
                      style={{ ...customS?.searchInput }}
                    />
                  )}
                </TitleRightWrap>
              </TitleRow>
    
              {children && (
                <ContentGridR container spacing={0} style={customS?.contentGridR} className={!title ? 'no-title' : ''}>
                  {children}
                </ContentGridR>
              )}
            </RightWrap>
          </GridR>
        </ContainerR>
      </SectionLayout>

      {(ModalContent && open) && (
        <ModalInnerContent open={open} close={closeModal} {...ModalProps}>
          <ModalContent close={submitClose}/>
        </ModalInnerContent>
      )}
    </>
  )
}
