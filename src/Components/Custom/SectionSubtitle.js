import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'

export const SectionSubTitle = styled(Typography)`
  font-size: 18px;
  max-width: 445px;
  margin-bottom: 55px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
  
  ${down('md')} {
    font-size: 15px;
    margin-bottom: 20px;
  }
`
