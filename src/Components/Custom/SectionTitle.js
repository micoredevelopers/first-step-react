import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Typography from '@material-ui/core/Typography'

export const SectionTitle = styled(Typography)`
  font-size: 48px;
  font-weight: 700;
  margin-bottom: 10px;
  font-family: Comfortaa, sans-serif;
  color: ${({ theme }) => theme.colors.main};
  
  ${down('md')} {
    font-size: 26px;
    margin-bottom: 15px;
  }
`
