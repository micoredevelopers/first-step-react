import React from 'react'
import styled from 'styled-components'
import { StudyingSlider } from '../Slider/StudyingSlider'

export const StudyingWrap = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 9999;
  position: fixed;
  background-color: rgba(0, 0, 0, 0.3);
`

export const Studying = ({ steps, onEndStudying }) => {
  const endStudying = () => {
    onEndStudying()
  }
  
  return steps.length > 0 && (
    <StudyingWrap>
      <StudyingSlider studyingSteps={steps} handleEndSteps={endStudying}/>
    </StudyingWrap>
  )
}

