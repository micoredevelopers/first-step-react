import styled from 'styled-components'
import Tooltip from '@material-ui/core/Tooltip'
import withStyles from '@material-ui/core/styles/withStyles'

const setStatusColor = ({ type, theme }) => {
  switch (type) {
    case 'file':
    case 'photo':
    case 'group':
      return theme.colors.main
    case 'type':
      return theme.colors.warning
    case 'st_mark':
      return '#fff'
    case 'date':
    case 'short_date':
      return theme.colors.secondary
    default: return theme.colors.gray
  }
}

export const LightTooltip = withStyles(() => ({
  tooltip: {
    fontSize: 14,
    color: '#000c18',
    textAlign: 'center',
    padding: '10px 20px',
    backgroundColor: '#fff',
    // textTransform: 'capitalize',
    fontFamily: "Avenir Next Cyr",
    boxShadow: '0 5px 12px rgba(0, 12, 24, 0.1)',
  },
  arrow: {
    color: '#fff'
  }
}))(Tooltip)

export const ChipWrap = styled.div`
  height: 30px;
  display: flex;
  padding: 0 15px;
  margin-right: 10px;
  align-items: center;
  border-radius: 15px;
  justify-content: center;
  border: 1px solid ${props => setStatusColor(props)};
  max-width: ${({ type }) => type === 'group' ? '55px' : 'unset'};
  cursor: ${({ type }) => (type === 'file' || type === 'photo') ? 'pointer' : 'default'};
  min-width: ${({ type }) => (type === 'file' || type === 'photo') ? '110px' : type === 'date' ? '160px' : '55px'};
  
  span {
    font-size: 16px;
    font-weight: 700;
    overflow: hidden;
    white-space: nowrap;
    display: inline-block;
    vertical-align: bottom;
    text-overflow: ellipsis;
    font-family: Comfortaa, sans-serif;
    color: ${props => setStatusColor(props)};
    text-transform: ${({ type }) => type === 'date' ? 'normal' : 'capitalize'};
  }
`
