import styled from 'styled-components'

export const GroupListWrap = styled.div`
  width: 80px;
  height: auto;
  display: flex;
  overflow: auto;
  padding: 30px 15px;
  border-radius: 16px;
  margin-bottom: 10px;
  flex-direction: column;
  background-color: #ffffff;
  max-height: calc(100% - calc(6vh - 25px));
`

export const SelectedGroup = styled.button`
  position: relative;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 48px;
  height: 48px;
  min-height: 48px;
  border-radius: 100%;
  border: 1px solid ${({ theme }) => theme.colors.main};
  margin-bottom: 30px;
  overflow: hidden;
  
  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 0;
    border-radius: 100%;
    transform: ${({ isGroupSelected }) => isGroupSelected ? 'translateX(0%)' : 'translateX(-100%)'};
    transition: all 0.4s ease-in-out;
    background-color: ${({ theme }) => theme.colors.main};
  }
  
  &:hover {
    &:before {
      transform: translateX(-50%);
    }
    
    span {
     color: #fff;
    }
  }
  
  &:last-child {
    margin-bottom: 0;
  }
  
  span {
    position: relative;
    display: block;
    font-family: "Avenir Next Cyr", sans-serif;
    font-size: 20px;
    text-transform: uppercase;
    line-height: normal;
    z-index: 1;
    transition: all 0.35s ease-in-out;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    color: ${({ theme, isGroupSelected }) => isGroupSelected ? '#fff' : theme.colors.main}
  }
`

export const ClearBtn = styled.div`
  display: flex;
  color: ${({ theme }) => theme.colors.grey};
  font-family: "Avenir Next Cyr", sans-serif;
  font-size: 14px;
  justify-content: center;
  width: 100%;
  max-width: 80px;
  cursor: pointer;
  transition: all .35s ease-in-out;
  
  &:hover {
    color: #000c18;
  }
`
