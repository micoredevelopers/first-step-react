import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import MuiStyled from '@material-ui/core/styles/styled'
import { CustomBtn } from '../../Buttons'

export const ContainerR = MuiStyled(Container)(({ theme }) => ({
  height: '100%',
  maxWidth: '100%',
  padding: '0 100px',
  
  [theme.breakpoints.down('md')]: {
    padding: 0
  }
}))

export const GridR = MuiStyled('div')({
  height: '100%',
  maxWidth: 1920,
  display: 'flex',
  margin: '0 auto',
  flexWrap: 'nowrap',
  position: 'relative',
  flexDirection: 'column',
  justifyContent: 'flex-start'
})

export const ContentGridR = MuiStyled(Grid)(({ theme }) => ({
  margin: '0 -15px',
  width: 'calc(100% + 30px)',
  height: 'calc(100% - 96px)',
  
  '&.no-title': {
    height: 'calc(100% - 6vh)'
  },
  
  [theme.breakpoints.down('md')]: {
    margin: 0,
    width: '100%',
    height: 'auto',
  
    '&.no-title': {
      height: 'auto'
    },
  }
}))

export const SectionLayout = styled.section`
  padding-top: 6vh;
  position: relative;
  padding-bottom: 6vh;
  height: calc(100vh - 100px);
  
  ${down('md')} {
    height: auto;
    padding-top: 30px;
    padding-bottom: 30px;
  }
`

export const TitleRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: ${({ isTitle }) => isTitle ? '40px' : '0'};
  
  ${down('md')} {
    top: 0;
    left: 0;
    right: 0;
    z-index: 990;
    padding: 0 15px;
    position: relative;
    margin-bottom: 20px;
    transition: all 0.5s ease-in-out 0.5s;
    
    &.is-changed {
      position: absolute;
      transition-delay: 0s;
      transform: translateX(-100%);
    }
  }
`

export const LeftWrap = styled.div`
  top: 0;
  left: 0;
  bottom: 0;
  z-index: 5;
  height: 100%;
  position: absolute;
  padding-top: 142px;
  margin-right: 100px;
  
  ${down('md')} {
    z-index: -1;
    padding-top: 0;
    margin-right: 0;
  }
`

export const RightWrap = styled.div`
  width: 100%;
  display: flex;
  position: relative;
  flex-direction: column;
  justify-content: flex-start;
  padding-left: ${({ withGroupFilters }) => withGroupFilters ? '180px' : '0px'};
  height: calc(100vh -
    ${({ noBottomOffset, withTitle }) => noBottomOffset ? '6vh' : withTitle ? '12vh' : '0px'} -
    ${({ withTitle }) => withTitle ? '100px' : '151px'});

  ${down('md')} {
    height: auto;
    padding-left: 0;
  }
`

export const TitleRightWrap = styled.div`
  display: flex;
  align-items: center;
  
  ${down('md')} {
    width: auto;
    flex-direction: row;
    align-items: center;
  }
`

export const TitleLeftWrap = styled.div`
  display: flex;
  align-items: center;
  
  ${down('md')} {
    width: auto;
  }
`

export const MobileAddBtn = styled(CustomBtn)`
  width: auto;
  height: auto;
  padding: 12px;
  min-width: auto;
  background-color: transparent !important;
  
  &:hover {
    background-color: transparent !important;
  }
`
