import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import {useGroupsStore, useLessonsStore} from '../../Store'
import { CustomForm } from '../index'
import { createHomework, getLessons, uploadFiles } from '../../Api'

const HomeworkCreateFields = ({ groups, selectedGroup, subjects, selectedSubject, changeGroup, setSelectedSubject }) => {
  return [
    {
      name: 'group',
      type: 'select',
      options: groups,
      isValid: ['isEmpty'],
      onChange: changeGroup,
      placeholder: 'Группа*',
      size: { xs: 12, lg: 6 },
      defaultValue: selectedGroup?.id ?? ''
    },
    {
      name: 'subject',
      autoFocus: false,
      options: subjects,
      openOnFocus: true,
      type: 'autocomplete',
      isValid: ['isEmpty'],
      optionLabel: 'subject',
      size: { xs: 12, lg: 6 },
      changedFieldName: ['comment'],
      disabled: subjects.length === 0,
      placeholder: 'Урок*',
      selectValue: {subject: selectedSubject?.subject},
      defaultValue: selectedSubject?.id,
      onChange: (data) => {
        setSelectedSubject(data.value)
      }
    },
    {
      rows: 6,
      type: 'input',
      multiline: true,
      name: 'comment',
      size: { xs: 12 },
      isValid: ['isEmpty'],
      style: { maxWidth: '100%' },
      placeholder: 'Комментарий к дз*',
      defaultValue: selectedSubject?.homeworkDescription ?? ''
    },
    {
      type: 'file',
      name: 'files',
      multiple: true,
      size: { xs: 12 },
      accept: 'image/*',
      label: 'Прикрепить файлы',
      icon: '/assets/images/FileIconGrey.svg',
      defaultValue: selectedSubject?.homeworkFiles ?? []
    }
  ]
}

export const CreateHomeworkFields = ({ close, isEdit }) => {
  const { groups, selectedGroup } = useGroupsStore()
  const { selectedLesson } = useLessonsStore()
  const [subjects, setSubjects] = useState([])
  const [selectedSubject, setSelectedSubject] = useState(selectedLesson)
  const getLessonsByGroup = async (groupId) => {
    const apiData = await getLessons({ group: groupId || selectedGroup?.id, withoutHomeWork: isEdit ? 0 : 1 })
    
    if (apiData) {
      await setSubjects(apiData.rows)
    } else {
      toast.error('Ошибка при загрузке спсика уроков!')
    }
  }
  
  const changeGroup = async ({ value }) => {
    await getLessonsByGroup(value)
  }
  const changeLesson = ({ value }) => {
    const selectedSubject = subjects.filter(lesson => lesson.id === value)[0]
  
    setSelectedSubject(selectedSubject)
  }
  
  const handleCreateHomework = async (data) => {
    if (data.files.length > 0 && !data.files[0]?.token) {
      await uploadFiles(data.files)
        .then(async (res) => {
          if (!res.errors) {
            const sendData = {
              description: data.comment,
              files: res.data.token ?? res.data.map(file => file.token)
            }
            await createHomework({ lessonId: data.subject, data: sendData })
              .then((res) => {
                if (res) {
                  toast.success('Домашнее задание успешно создано!')
                  close()
                }
              })
          } else {
            toast.error('При создании домашнего задания произошла ошибка!')
          }
        })
    } else {
      const sendData = {
        description: data.comment,
        files: data.files?.map(file => file.token)
      }
      await createHomework({ lessonId: data.subject, data: sendData })
        .then((res) => {
          if (res) {
            toast.success('Домашнее задание успешно создано!')
            close()
          }
        })
    }
  }
  
  useEffect(() => {
    selectedGroup && getLessonsByGroup()
    //eslint-disable-next-line
  }, [])

  useEffect(() => {
    isEdit && !selectedSubject && subjects && changeLesson({value: selectedLesson?.id})
  },[selectedGroup, selectedSubject, subjects])

  return (
    <CustomForm
      btnTitle={isEdit ? 'Сохранить' : 'Создать'}
      handleSubmit={handleCreateHomework}
      fields={HomeworkCreateFields({ groups, selectedGroup, subjects, selectedSubject, changeGroup, changeLesson, setSelectedSubject })}
    />
  )
}
