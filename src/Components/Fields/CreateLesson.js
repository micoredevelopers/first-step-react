import React from 'react'
import { toast } from 'react-toastify'
import addWeeks from 'date-fns/addWeeks'
import addMinutes from 'date-fns/addMinutes'
import differenceInMinutes from 'date-fns/differenceInMinutes'
import { CustomForm } from '../index'
import { formatDate } from '../../Helpers'
import { TypeOptions } from '../../Constants'
import { useGroupsStore, useStaticListsStore } from '../../Store'
import { createLesson, duplicateLesson, updateLesson } from '../../Api'

const LessonCreateFields = ({ groups, selectedGroup, teachers, lesson }) => {
  // const weekdays = [...Array(7).keys()].map(i => {
  //   const day = locale.localize.day(i, { width: 'wide' })
  //
  //   return ({
  //     value: i + 1,
  //     name: day.charAt(0).toUpperCase() + day.slice(1)
  //   })
  // })
  const lessonEndTime = lesson ? addMinutes(new Date(lesson.date), lesson.lengthMinutes) : null
  
  return [
    {
      type: 'date',
      name: 'day',
      placeholder: 'День*',
      isValid: ['isEmpty'],
      size: { xs: 12, lg: 6 },
      defaultValue: lesson?.date ?? null
    },
    {
      type: 'select',
      name: 'group',
      options: groups,
      isValid: ['isEmpty'],
      placeholder: 'Группа*',
      size: { xs: 12, lg: 6 },
      defaultValue: selectedGroup?.id || lesson?.group.id || ''
    },
    
    {
      type: 'time',
      name: 'time',
      isValid: ['isEmpty'],
      size: { xs: 12, lg: 6 },
      placeholder: 'Начало урока*',
      defaultValue: lesson?.date ?? null
    },
    {
      type: 'time',
      name: 'lessonEndTime',
      isValid: ['isEmpty'],
      size: { xs: 12, lg: 6 },
      placeholder: 'Конец урока*',
      defaultValue: lessonEndTime
    },
    
    {
      name: 'type',
      type: 'select',
      options: TypeOptions,
      isValid: ['isEmpty'],
      size: { xs: 12, lg: 6 },
      placeholder: 'Тип урока*',
      defaultValue: lesson?.type ?? ''
    },
    {
      type: 'input',
      name: 'periodCount',
      size: { xs: 12, lg: 6 },
      placeholder: 'Кол-во повторов',
      defaultValue: lesson?.period ?? ''
    },
    
    {
      type: 'select',
      size: { xs: 12 },
      name: 'replacement',
      placeholder: 'Замена',
      style: { maxWidth: '100%' },
      defaultValue: lesson?.replacement?.id ?? '',
      options: teachers.map(teacher => ({ ...teacher, format: opt => opt.fullName }))
    },
    // {
    //   type: 'select',
    //   multiple: true,
    //   defaultValue: [],
    //   name: 'periodDay',
    //   options: weekdays,
    //   size: { xs: 12, lg: 6 },
    //   placeholder: 'День повтора'
    // },
    
    {
      type: 'input',
      name: 'subject',
      size: { xs: 12 },
      autoFocus: false,
      autoComplete: 'off',
      isValid: ['isEmpty'],
      placeholder: 'Тема урока*',
      style: { maxWidth: '100%' },
      defaultValue: lesson?.subject ?? ''
    },
    {
      rows: 6,
      type: 'input',
      multiline: true,
      size: { xs: 12 },
      name: 'description',
      style: { maxWidth: '100%' },
      placeholder: 'Описание урока',
      defaultValue: lesson?.description ?? ''
    }
  ]
}

export const CreateLessonFields = ({ lesson, close, update }) => {
  const { teachers } = useStaticListsStore()
  const { groups, selectedGroup } = useGroupsStore()
  
  const handleDuplicateLesson = async (count, day, time, lesson) => {
    for (let i = 1; i <= count; i++) {
      const newWeek = addWeeks(new Date(day), i)
      const fullDate = `${formatDate(newWeek, 'yyyy-MM-dd')} ${time}`

      await duplicateLesson(lesson.id, fullDate)
    }
  }
  
  const handleSubmit = async (data) => {
    const { day, time, teachers, timeSend, daySend, lessonEndTime, periodDay, periodCount, ...other } = data
    const timeFull = formatDate(time, 'HH:mm')
    const dateFull = formatDate(day, 'yyyy-MM-dd')
    const difference = differenceInMinutes(lessonEndTime, time)
    const sendData = {
      lengthMinutes: difference,
      date: `${dateFull} ${timeFull}`,
      ...other
    }
    
    if (lesson) {
      await updateLesson(lesson.id, sendData)
        .then(async (res) => {
          if (periodCount) {
            await handleDuplicateLesson(periodCount, day, timeFull, res)
              .then(() => {
                toast.success('Урок обновлен!')
                close()
                update()
              })
          } else {
            toast.success('Урок обновлен!')
            close()
            update()
          }
        })
    } else {
      await createLesson(sendData)
        .then(async (res) => {
          if (res) {
            if (periodCount) {
              await handleDuplicateLesson(periodCount, day, timeFull, res)
                .then(() => {
                  toast.success('Урок создан!')
                  close()
                })
            } else {
              toast.success('Урок создан!')
              close()
            }
          }
        })
    }
  }
  
  return (
    <CustomForm
      handleSubmit={handleSubmit}
      btnTitle={lesson ? 'Редактировать' : 'Создать'}
      fields={LessonCreateFields({ groups, selectedGroup, teachers, lesson })}
    />
  )
}
