import React, { useEffect, useState } from 'react'
import { get } from 'lodash'
import { CustomForm } from '../index'
import { createStudent, getGroups, updateStudent } from '../../Api'
import { useStaticListsStore, useStudentsDispatch, useTeachersDispatch, useTeachersStore } from '../../Store'

const StudentCreateFields = ({ student, groups, teachers, changeTeacher }) => {
  return [
    {
      name: 'name',
      type: 'input',
      placeholder: 'Имя*',
      isValid: ['isEmpty'],
      size: { xs: 12, lg: 6 },
      defaultValue: student?.name ?? ''
    },
    {
      type: 'input',
      name: 'lastName',
      isValid: ['isEmpty'],
      placeholder: 'Фамилия*',
      size: { xs: 12, lg: 6 },
      defaultValue: student?.lastName ?? ''
    },
    {
      type: 'input',
      name: 'middleName',
      placeholder: 'Отчество',
      size: { xs: 12, lg: 6 },
      defaultValue: student?.middleName ?? ''
    },
    {
      type: 'input',
      name: 'email',
      placeholder: 'Почта*',
      size: { xs: 12, lg: 6 },
      isValid: ['isEmpty', 'isEmail'],
      defaultValue: student?.email ?? ''
    },
    {
      mask: true,
      type: 'input',
      name: 'phone',
      isValid: ['isPhone'],
      placeholder: 'Телефон',
      size: { xs: 12, lg: 6 },
      style: { maxWidth: '100%' },
      defaultValue: student?.phone ?? ''
    },
    {
      type: 'input',
      name: 'password',
      placeholder: 'Пароль*',
      size: { xs: 12, lg: 6 },
      style: { maxWidth: '100%' },
      defaultValue: student?.password ?? '',
      isValid: student ? [] : ['isEmpty', 'isPass']
    },
    {
      type: 'select',
      name: 'teacher',
      isValid: ['isEmpty'],
      size: { xs: 12, lg: 6 },
      placeholder: 'Учитель*',
      onChange: changeTeacher,
      changedFieldName: ['group'],
      defaultValue: student?.teacher?.id ?? '',
      options: teachers.map(teacher => ({ ...teacher, format: option => option.fullName }))
    },
    {
      name: 'group',
      type: 'select',
      options: groups,
      isValid: ['isEmpty'],
      placeholder: 'Группа*',
      size: { xs: 12, lg: 6 },
      disabled: groups.length === 0,
      defaultValue: student?.group?.id ?? ''
    }
  ]
}

export const CreateStudentFields = ({ close, update, student }) => {
  const teachersDispatch = useTeachersDispatch()
  const studentsDispatch = useStudentsDispatch()
  const { teachers } = useStaticListsStore()
  const { selectedTeacher } = useTeachersStore()
  const [teacherGroups, setTeacherGroups] = useState([])
  
  const handleSubmit = async ({ teacher, ...otherData }) => {
    if (student) {
      const sendData = {
        ...otherData,
        enable: student.enable
      }
      const apiData = await updateStudent(`${student.id}?attribute_groups=student_assoc`, sendData)
      
      if (apiData) {
        close()
        update && update()
        studentsDispatch({
          selectedStudent: apiData,
          type: 'SET_SELECTED_STUDENT_SUCCESS'
        })
      }
    } else {
      const apiData = await createStudent(otherData)
      
      if (apiData) {
        close()
      }
    }
  }
  
  const changeTeacher = async ({ name, value }, data) => {
    const selectedTeacher = data.options.filter(option => option.id === value)[0]
    const apiData = await getGroups({ teachers: selectedTeacher.id, limit: 50 })
    
    if (apiData) {
      teachersDispatch({
        selectedTeacher,
        type: 'SET_SELECTED_TEACHER_SUCCESS'
      })
      setTeacherGroups(apiData)
    }
  }
  
  useEffect(() => {
    student && teachersDispatch({
      type: 'SET_SELECTED_TEACHER_SUCCESS',
      selectedTeacher: student.teacher ? teachers.filter(teacher => teacher.id === student.teacher.id)[0] : null
    })
    //eslint-disable-next-line
  }, [student])
  
  return (
    <CustomForm
      handleSubmit={handleSubmit}
      btnTitle={student ? 'Изменить' : 'Создать'}
      fields={StudentCreateFields({
        student,
        teachers,
        changeTeacher,
        groups: (student && selectedTeacher) ? get(selectedTeacher, 'studentGroups', []) : teacherGroups
      })}
    />
  )
}
