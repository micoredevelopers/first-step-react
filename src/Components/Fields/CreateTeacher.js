import React from 'react'
import { CustomForm } from '../index'
import { createTeacher, updateTeacher } from '../../Api'
import { useTeachersDispatch } from '../../Store/Context'

const TeacherCreateFields = ({ teacher }) => {
  return [
    {
      name: 'name',
      type: 'input',
      placeholder: 'Имя*',
      isValid: ['isEmpty'],
      size: { xs: 12, lg: 6 },
      defaultValue: teacher?.name ?? ''
    },
    {
      type: 'input',
      name: 'lastName',
      isValid: ['isEmpty'],
      placeholder: 'Фамилия*',
      size: { xs: 12, lg: 6 },
      defaultValue: teacher?.lastName ?? ''
    },
    {
      type: 'input',
      name: 'middleName',
      isValid: ['isEmpty'],
      placeholder: 'Отчество*',
      size: { xs: 12, lg: 6 },
      defaultValue: teacher?.middleName ?? ''
    },
    {
      type: 'input',
      name: 'email',
      placeholder: 'Почта*',
      size: { xs: 12, lg: 6 },
      isValid: ['isEmpty', 'isEmail'],
      defaultValue: teacher?.email ?? ''
    },
    {
      type: 'input',
      name: 'password',
      placeholder: 'Пароль*',
      size: { xs: 12, lg: 6 },
      defaultValue: teacher?.password ?? '',
      isValid: teacher ? [] : ['isEmpty', 'isPass']
    },
    {
      mask: true,
      type: 'input',
      name: 'phone',
      isValid: ['isPhone'],
      placeholder: 'Телефон',
      size: { xs: 12, lg: 6 },
      defaultValue: teacher?.phone ?? ''
    },
    {
      size: { xs: 12 },
      defaultValue: '',
      type: 'chipsInput',
      name: 'studentGroups',
      placeholder: 'Группы',
      style: { maxWidth: '100%' },
      defaultChips: teacher?.studentGroups?.map(group => group) ?? []
    }
  ]
}

export const CreateTeacherFields = ({ close, update, teacher }) => {
  const teachersDispatch = useTeachersDispatch()
  
  const handleSubmit = async ({ studentGroups, chips, defaultChips, ...otherData }) => {
    if (teacher) {
      const sendData = {
        ...otherData,
        enable: teacher.enable,
        studentGroups: [...chips, ...defaultChips.map(group => group.name)]
      }
      const apiData = await updateTeacher(teacher.id, sendData)
      
      if (apiData) {
        teachersDispatch({
          selectedTeacher: apiData,
          type: 'SET_SELECTED_TEACHER_SUCCESS'
        })
        close()
        update && update()
      }
    } else {
      const sendData = {
        ...otherData,
        studentGroups: studentGroups.split(' ')
      }
      const apiData = await createTeacher(sendData)

      if (apiData) {
        close()
      }
    }
  }
  
  return (
    <CustomForm
      handleSubmit={handleSubmit}
      fields={TeacherCreateFields({ teacher })}
      btnTitle={teacher ? 'Изменить' : 'Создать'}
    />
  )
}

