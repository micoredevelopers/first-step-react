import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { toast } from 'react-toastify'
import styled from 'styled-components'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { CustomBtn } from '../Buttons'
import { updateGroup } from '../../Api'
import { AutocompleteInput, FormInput } from '../Inputs'
import { DateSelect, FormSelect, TimeSelect } from '../Selects'
import { checkValidation, formatDate, MobileView } from '../../Helpers'

const useStyles = makeStyles({
  chipsInput: {
    flexDirection: 'column',
    alignItems: 'flex-start'
  }
})

const FormWrap = styled.form`
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
  max-width: 455px;
`

const ErrorsWrap = styled.div`
  width: 100%;
  display: flex;
  margin: 0 auto;
  max-width: 220px;
  align-items: center;
  flex-direction: column;
`

const ErrorText = styled.small`
  width: 100%;
  display: block;
  font-size: 12px;
  margin-top: 6px;
  text-align: center;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.error};
  
  &:first-child {
    margin-top: 10px;
  }
`

const FilesContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`

const FileWrap = styled.div`
  display: flex;
  margin-right: 15px;
  align-items: center;
  margin-bottom: 10px;
`

const FileDelete = styled.img`
  width: 10px;
  height: 10px;
  cursor: pointer;
  min-width: 10px;
  margin-right: 10px;
`

const FileName = styled(Typography)`
  font-size: 12px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
`

export const CustomForm = ({ fields, handleSubmit, btnTitle = 'Создать', formStyle, withClear, clearFn }) => {
  const classes = useStyles()
  const [errors, setErrors] = useState(null)
  const [fieldsData, setFieldsData] = useState(null)
  const submitForm = (e) => {
    e && e.preventDefault()
    const errs = fields.map(({ isValid, name, placeholder, label }) => {
      const errors = checkValidation({ validTypes: isValid, name, value: fieldsData[name], placeholder: placeholder || label })
      
      setErrors(prev => ({ ...prev, ...errors }))
      return errors
    })
    const isError = errs.some(err => Object.values(err).length > 0 && Object.values(err)[0] !== '')
    
    if (!isError) {
      handleSubmit(fieldsData)
    }
  }
  const handleClear = () => {
    setErrors(null)
    setFieldsData(prev => {
      let prevObj = prev
      
      fields.forEach(field => {
        prevObj[field.name] = ''
      })
      
      return prevObj
    })
    clearFn()
  }
  const customChange = (field, onChange) => (e, val) => {
    const { name, value } = e.target

    onChange({ name, value: value ?? val }, field)
    field.type === 'autocomplete' ? handleAutocompleteChange(field)(e, val) : handleInputChange(field)(e)
  }
  
  /* CHANGE EVENTS */
  const handleInputChange = ({ isValid, placeholder, data }) => (e) => {
    const { name, value } = e.target
    const errors = checkValidation({ validTypes: isValid, name, value, placeholder })
   
    setErrors(prev => ({ ...prev, ...errors }))
    setFieldsData(prev => ({ ...prev, ...data, [name]: value }))
  }
  const handleChipInputChange = (e) => {
    const { name, value } = e.target
  
    setFieldsData(prev => ({
      ...prev,
      [name]: value,
      chips: [...value.split(' ')]
    }))
  }
  const handleAutocompleteChange = ({ isValid, placeholder, name }) => (e, val) => {
    const errors = checkValidation({ validTypes: isValid, name, value: val?.subject ?? '', placeholder })
  
    setErrors(prev => ({ ...prev, ...errors }))
    setFieldsData(prev => ({ ...prev, [name]: val?.id ?? '' }))
  }
  const handleDateChange = ({ isValid, placeholder, name }) => date => {
    const formattedDate = formatDate(date, 'dd')
    const errors = checkValidation({ validTypes: isValid, name, value: date, placeholder })
  
    setErrors(prev => ({ ...prev, ...errors }))
    setFieldsData(prev => ({ ...prev, [name]: date, daySend: formattedDate }))
  }
  const handleTimeChange = ({ isValid, placeholder, name }) => (time, formattedTime) => {
    const timeVal = time === 'Invalid Date' ? null : time
    const errors = checkValidation({ validTypes: isValid, name, value: timeVal, placeholder })
    
    setErrors(prev => ({ ...prev, ...errors }))
    setFieldsData(prev => ({ ...prev, [name]: timeVal, timeSend: formattedTime }))
  }
  const handleFileChange = ({ isValid, name, label }) => e => {
    const input = e.target
    const errors = checkValidation({ validTypes: isValid, name, value: input.files, placeholder: label })

    if (input.files.length < 6) {
      if (_.filter(input.files, (file) => file.type.includes('image')).length === 0) {
        toast.error('Загружен не верный формат файла! Доступные форматы файлов: .jpg, .jpeg, .png')
      } else {
        setErrors(prev => ({ ...prev, ...errors }))
        setFieldsData(prev => {
          const files = Object.values(input.files).map(file => file)

          return ({ ...prev, files })
        })
      }
    } else {
      toast.error('Можно загрузить максимум 5 файлов!')
    }
  }
  const handleDeleteFile = (index, { isValid, name, label }) => () => {
    const filesArr = fieldsData.files
    filesArr.splice(index, 1)
    const errors = checkValidation({ validTypes: isValid, name, value: filesArr, placeholder: label })
  
    setErrors(prev => ({ ...prev, ...errors }))
    setFieldsData(prev => ({ ...prev, files: [...filesArr] }))
  }
  const handleDeleteChip = (id) => () => {
    fieldsData.defaultChips.splice(fieldsData.defaultChips.findIndex(chip => chip.id === id), 1)
    setFieldsData(prev => ({ ...prev, defaultChips: fieldsData.defaultChips }))
  }
  const handleMoveChip = (chip) => async (link) => {
    const apiData = await updateGroup(chip.id, { name: chip.name, teacher: link.id })

    if (apiData) {
      handleDeleteChip(chip.id)()
      submitForm()
    }
  }
  /**/
  
  useEffect(() => {
    setFieldsData(prev => {
      let data = {}
      
      fields.forEach(field => {
        if (field.defaultChips) {
          data.chips = []
          data.defaultChips = field.defaultChips
        }
        data[field.name] = field.defaultValue
      })
      
      return ({ ...prev, ...data })
    })
    //eslint-disable-next-line
  }, [])
  
  useEffect(() => {
    setFieldsData(prev => {
      let data = {}
      
      fields.forEach(field => {
        if (field.changedFieldName) {
          field.changedFieldName.forEach(name => {
            data[name] = fields.filter(field => field.name === name)[0].defaultValue
          })
        }
      })
      
      return ({ ...prev, ...data })
    })
  }, [fields])
  return (
    <FormWrap onSubmit={submitForm} style={formStyle}>
      <Grid container spacing={3}>
        {fieldsData && fields.map(({ defaultValue, defaultChips, type, size, inputType, selectValue, ...field }, index) => {
          switch (type) {
            case 'input':
              return (
                <Grid key={index} item xs={size.xs} lg={size.lg}>
                  <FormInput
                    {...field}
                    type={inputType}
                    error={errors && !!errors[field.name]}
                    value={fieldsData[field.name] ?? defaultValue}
                    onChange={field.onChange ? customChange(field, field.onChange) : handleInputChange(field)}
                  />
                </Grid>
              )
            case 'autocomplete':
              return (
                <Grid key={index} item xs={size.xs} lg={size.lg}>
                  <AutocompleteInput
                    {...field}
                    value={selectValue}
                    error={errors && !!errors[field.name]}
                    onChange={field.onChange ? customChange({ type, ...field }, field.onChange) : handleAutocompleteChange(field)}
                  />
                </Grid>
              )
            case 'chipsInput':
              return (
                <Grid key={index} item xs={size.xs} lg={size.lg}>
                  <FormInput
                    {...field}
                    moveChip={handleMoveChip}
                    deleteChip={handleDeleteChip}
                    className={classes.chipsInput}
                    onChange={handleChipInputChange}
                    error={errors && !!errors[field.name]}
                    value={fieldsData[field.name] ?? defaultValue}
                    tooltip="Названия групп необходимо вводить через пробел"
                    chips={[...fieldsData.chips, ...fieldsData.defaultChips]}
                  />
                </Grid>
              )
            case 'date':
              return (
                <Grid key={index} item xs={size.xs} lg={size.lg}>
                  <DateSelect
                    {...field}
                    onChange={handleDateChange(field)}
                    error={errors && !!errors[field.name]}
                    value={fieldsData[field.name] ?? defaultValue}
                  />
                </Grid>
              )
            case 'time':
              return (
                <Grid key={index} item xs={size.xs} lg={size.lg}>
                  <TimeSelect
                    {...field}
                    onChange={handleTimeChange(field)}
                    error={errors && !!errors[field.name]}
                    value={fieldsData[field.name] ?? defaultValue}
                  />
                </Grid>
              )
            case 'select':
              return (
                <Grid key={index} item xs={size.xs} lg={size.lg}>
                  <FormSelect
                    {...field}
                    error={errors && !!errors[field.name]}
                    value={fieldsData[field.name] ?? defaultValue}
                    onChange={field.onChange ? customChange(field, field.onChange) : handleInputChange(field)}
                  />
                </Grid>
              )
            case 'file':
              return (
                <Grid key={index} item xs={size.xs} lg={size.lg}>
                  {fieldsData?.files.length > 0 && (
                    <FilesContainer>
                      {fieldsData[field.name].map((file, index) => {
                        const fileName = file.name || file.path.split('/')[1]
                  
                        return (
                          <FileWrap key={index}>
                            <FileDelete
                              alt="Delete icon"
                              src="/assets/images/DeleteIcon.svg"
                              onClick={handleDeleteFile(index, field)}
                            />
                            <FileName>{fileName}</FileName>
                          </FileWrap>
                        )
                      })}
                    </FilesContainer>
                  )}
                  <FormInput
                    {...field}
                    type={type}
                    onChange={handleFileChange(field)}
                  />
                </Grid>
              )
            default:
              return null
          }
        })}
        <Grid item xs={12}>
          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <CustomBtn type="submit" title={btnTitle} style={{ margin: '0 auto' }}/>
            {withClear && (
              <MobileView>
                {fieldsData && Object.values(fieldsData).some(val => val !== '') && (
                  <CustomBtn color="secondary" title="Сбросить" onClick={handleClear} style={{ margin: '15px auto 0' }}/>
                )}
              </MobileView>
            )}
          </div>
          {errors && (
            <ErrorsWrap>
              {Object.values(errors).map((err, index) => err !== '' && (
                <ErrorText key={index} children={err}/>
              ))}
            </ErrorsWrap>
          )}
        </Grid>
      </Grid>
    </FormWrap>
  )
}
