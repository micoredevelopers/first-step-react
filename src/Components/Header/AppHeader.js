import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Link, useLocation } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Container from '@material-ui/core/Container'
import { FormSelect } from '../Selects'
import { useGroupsStore } from '../../Store'
import { CustomBtn, LessonBtn, MenuButton } from '../Buttons'
import { getAccountLinks, getAccountMenuLinks } from './Config'
import { DesktopView, getUser, MobileView } from '../../Helpers'
import { ask, DialogSubTitle, ModalInnerContent } from '../Modals'
import { createMeeting, getMeeting, deleteMeeting } from '../../Api'
import {
  Logo,
  MenuLine,
  useStyles,
  LinksWrap,
  RightWrap,
  MenuModal,
  HeaderLink,
  AccountWrap,
  MenuModalLink,
  MenuMobileWrap,
  MenuModalLinksWrap
} from './Styles'

export const AppHeader = ({ account }) => {
  const user = getUser()
  const classes = useStyles()
  const Location = useLocation()
  const { groups } = useGroupsStore()
  const [meeting, setMeeting] = useState(null)
  const [openModal, setOpenModal] = useState(false)
  const [lessonGroup, setLessonGroup] = useState('')
  const [menuOpened, setMenuOpened] = useState(false)
  const allLinks = getAccountLinks(account).concat(getAccountMenuLinks(account))
  
  const handleModalOpen = () => {
    setOpenModal(true)
  }
  const handleCloseModal = () => {
    setOpenModal(false)
    setLessonGroup('')
  }

  const handleChangeGroup = async (e) => {
    const { value } = e.target
    
    setLessonGroup(value)
  }

  const getZoomUrl = async () => {
    const apiData = await getMeeting()
    
    apiData && setMeeting(apiData)
  }
  
  const deleteZoomLesson = async () => {
    ask({
      btnTitle: 'Завершить',
      title: 'Завершение урока',
      subTitle: `Вы уверены что хотите завершить урок?`,
      callback: async () => {
        const apiData = await deleteMeeting()
      
        if (apiData) {
          handleCloseModal()
          setMeeting(null)
          toast.success('Урок завершен!')
        }
      }
    })
  }

  const handleStartLesson = async () => {
    const apiData = await createMeeting(lessonGroup)
  
    if (apiData) {
      toast.success('Урок начат!')
      handleCloseModal()
      getZoomUrl()
    } else {
      toast.error('При запуске урока произошла ошибка или урок уже начат!')
    }
  }
  
  const handleOpenMenu = () => {
    setMenuOpened(!menuOpened)
  }
  
  const handleMenuItemClick = () => {
    setMenuOpened(false)
  }

  useEffect(() => {
    if (account !== 'admin') {
      getZoomUrl()
    }
  }, [account])
  
  useEffect(() => {
    const html = document.getElementsByTagName('html')[0]
    const body = document.getElementsByTagName('body')[0]
  
    if (menuOpened) {
      html.style.overflow = 'hidden'
      body.style.overflow = 'hidden'
    } else {
      html.style.overflow = 'visible'
      body.style.overflow = 'visible'
    }
  }, [menuOpened])
  
  return (
    <>
      <AppBar position="fixed" className={classes.root}>
        <Toolbar className={classes.toolBarRoot}>
          <Container maxWidth={false} className={classes.containerRoot}>
            <Grid container spacing={0} className={classes.gridContainerRoot}>
              <MobileView>
                {account === 'teacher' ? (
                  <LessonBtn title="Урок" onClick={handleModalOpen}/>
                ) : account === 'student' ? (
                  <LessonBtn
                    target="_blank"
                    href={meeting?.url}
                    disabled={!meeting?.meeting}
                    title={meeting?.meeting ? 'Войти' : 'Урока нет'}
                  />
                ) : null}
              </MobileView>
              
              <Link to="/">
                <Logo src="/assets/images/Logo.svg" alt="Logo"/>
              </Link>
              
              <MobileView>
                <MenuMobileWrap onClick={handleOpenMenu} className={menuOpened ? 'is-open' : ''}>
                  <MenuLine/>
                  <MenuLine/>
                  <MenuLine/>
                  <MenuLine/>
                </MenuMobileWrap>
              </MobileView>

              <DesktopView>
                <RightWrap>
                  <LinksWrap>
                    {getAccountLinks(account).map(({ to, title }) => (
                      <HeaderLink
                        to={to}
                        key={title}
                        children={title}
                        className={`${to.split('/')[2]}-link ${Location.pathname.includes(to) ? 'is-active' : ''}`}
                      />
                    ))}
                  </LinksWrap>
    
                  <AccountWrap>
                    <MenuButton
                      withIcon
                      withArrow
                      id="account_menu"
                      title={user?.name || 'Admin'}
                      linksList={getAccountMenuLinks(account)}
                    />
                  </AccountWrap>
    
                  {account === 'teacher' ? (
                    <CustomBtn
                      className="lesson-btn"
                      onClick={handleModalOpen}
                      title={meeting?.meeting ? 'Урок начат' : 'Начать урок'}
                    />
                  ) : account === 'student' ? (
                    <CustomBtn
                      target="_blank"
                      href={meeting?.url}
                      className="lesson-btn"
                      disabled={!meeting?.meeting}
                      title={meeting?.meeting ? 'Войти в урок' : 'Урок еще не начался!'}
                    />
                  ) : null}
                </RightWrap>
              </DesktopView>
            </Grid>
          </Container>
        </Toolbar>
      </AppBar>

      <ModalInnerContent
        open={openModal}
        close={handleCloseModal}
        title={meeting?.meeting ? 'Урок начат' : 'Запуск урока'}
        subTitle={!meeting?.meeting && 'Выберите свою группу, нажмите кнопку «начать». Урок откроется в соседнем окне'}
      >
        {meeting?.meeting ? (
          <>
            <CustomBtn
              target="_blank"
              href={meeting.url}
              title="Войти в урок"
              className="lesson-btn"
              style={{ margin: '5px 0 20px' }}
            />
            <DialogSubTitle variant="h6" style={{ margin: '0 auto', maxWidth: 300 }}>
              Чтобы запустить новый урок нужно <a onClick={deleteZoomLesson}>завершить предыдущий</a>
            </DialogSubTitle>
          </>
        ) : (
          <>
            <FormSelect
              name="group"
              options={groups}
              value={lessonGroup}
              onChange={handleChangeGroup}
              placeholder="Выберете группу:"
              style={{ maxWidth: 220, marginBottom: 15 }}
            />
            <CustomBtn disabled={!lessonGroup} title="Начать" onClick={handleStartLesson}/>
          </>
        )}
      </ModalInnerContent>
      
      <MenuModal className={menuOpened ? 'is-open' : ''}>
        <MenuModalLinksWrap>
          {allLinks.map((link, index) => (
            <MenuModalLink
              key={index}
              children={link.title}
              to={link.path ?? link.to ?? '/'}
              onClick={link.event ?? handleMenuItemClick}
            />
          ))}
        </MenuModalLinksWrap>
      </MenuModal>
    </>
  )
}
