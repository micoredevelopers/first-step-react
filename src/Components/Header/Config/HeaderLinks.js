export const getAccountLinks = (account) => {
  switch (account) {
    case 'teacher':
      return [
        {
          title: 'Расписание',
          to: '/teacher/schedule'
        },
        {
          title: 'Домашнее задание',
          to: '/teacher/homework'
        },
        {
          title: 'Уроки',
          to: '/teacher/lessons'
        }
      ]
    case 'student':
      return [
        {
          title: 'Оценки и дз',
          to: '/student/marks'
        },
        {
          title: 'Расписание',
          to: '/student/schedule'
        },
        {
          title: 'Мой класс',
          to: '/student/class'
        },
        {
          title: 'Оплата',
          to: '/student/payment'
        },
        // {
        //   title: 'Test',
        //   to: '/student/test'
        // }
      ]
    case 'admin':
      return [
        {
          title: 'Учителя',
          to: '/admin/teachers'
        },
        {
          title: 'Ученики',
          to: '/admin/students'
        },
        {
          title: 'Расписание',
          to: '/admin/schedule'
        },
        {
          title: 'Оплата',
          to: '/admin/payment'
        }
      ]
    default:
      return []
  }
}
