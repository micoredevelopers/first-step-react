import { LogOut } from '../../../Helpers'

export const getAccountMenuLinks = (account) => {
  switch (account) {
    case 'teacher':
      return [
        {
          isLink: true,
          title: 'FAQ',
          path: `/${account}/faq`
        },
        {
          isLink: true,
          title: 'Договор',
          path: `/${account}/agreement`
        },
        {
          isLogout: true,
          title: 'Выход',
          event: LogOut
        }
      ]
    case 'student':
      return [
        {
          isLink: true,
          title: 'FAQ',
          path: `/${account}/faq`
        },
        {
          isLogout: true,
          title: 'Выход',
          event: LogOut
        }
      ]
    case 'admin':
      return [
        {
          isLogout: true,
          title: 'Выход',
          event: LogOut
        }
      ]
    default: return []
  }
}
