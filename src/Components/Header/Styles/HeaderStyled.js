import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { down } from 'styled-breakpoints'
import makeStyles from '@material-ui/core/styles/makeStyles'

export const useStyles = makeStyles((theme) => ({
  root: {
    height: 100,
    zIndex: 999,
    backgroundColor: '#fff',
    boxShadow: 'none !important',
    [theme.breakpoints.down('md')]: {
      height: 70
    }
  },
  toolBarRoot: {
    height: '100%',
    display: 'flex',
    padding: '0 100px',
    alignItems: 'center',
    justifyContent: 'space-between',
    [theme.breakpoints.down('md')]: {
      padding: '0 15px'
    }
  },
  containerRoot: {
    padding: 0,
    maxWidth: 1920
  },
  gridContainerRoot: {
    display: 'flex',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
}))

export const Logo = styled.img`
  width: 70px;
  height: 70px;
  
  ${down('md')} {
    top: 50%;
    left: 50%;
    z-index: 1;
    width: 46px;
    height: 46px;
    position: absolute;
    transform: translate(-50%, -50%);
  }
`

export const RightWrap = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

export const LinksWrap = styled.div`
  display: flex;
  align-items: center;
  margin-right: 50px;
`

export const HeaderLink = styled(Link)`
  font-size: 16px;
  margin-right: 50px;
  position: relative;
  text-decoration: none;
  font-family: "Avenir Next Cyr";
  transition: all 0.35s ease-in-out;
  color: ${({ theme }) => theme.colors.black};
  
  &.is-active {
    color: ${({ theme }) => theme.colors.main};
    
    &:before {
      width: 100%;
    }
  }
  
  &:before {
    left: 0;
    right: 0;
    width: 0;
    content: '';
    height: 1px;
    bottom: -2px;
    position: absolute;
    transition: all 0.4s ease-in-out;
    background-color: ${({ theme }) => theme.colors.main};
  }
  
  &:hover {
    color:  ${({ theme }) => theme.colors.main}
  }
  
  &:last-child {
    margin-right: 0;
  }
`

export const AccountWrap = styled.div`
  margin-right: 50px;
`

/* MOBILE STYLES */
export const MenuMobileWrap = styled.div`
  width: 32px;
  height: 32px;
  display: flex;
  overflow: hidden;
  position: relative;
  flex-direction: column;
  justify-content: center;
  
  &.is-open {
    span {
      &:nth-child(1) {
        width: 0;
      }
      
      &:nth-child(2) {
        transform: rotate(45deg);
      }
      
      &:nth-child(3) {
        transform: translateY(-50%) rotate(-45deg);
      }
      
      &:nth-child(4) {
        width: 0;
      }
    }
  }
`

export const MenuLine = styled.span`
  height: 2px;
  border-radius: 1px;
  margin-bottom: 6px;
  transition: all 0.4s ease-in-out;
  background-color: ${({ theme }) => theme.colors.main};
  
  &:last-child {
    margin-bottom: 0;
  }
  
  &:nth-child(1) {
    width: 20px;
    margin-left: auto;
  }
  
  &:nth-child(2) {
    width: 32px;
  }
  
  &:nth-child(3) {
    left: 0;
    right: 0;
    top: 50%;
    width: 32px;
    position: absolute;
    transform: translateY(-50%);
  }
  
  &:nth-child(4) {
    width: 26px;
    margin-left: auto;
  }
`

export const MenuModal = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 998;
  display: flex;
  position: fixed;
  align-items: center;
  flex-direction: column;
  background-color: #fff;
  justify-content: center;
  transform: translateY(-100%);
  transition: all 0.5s ease-in-out;
  
  &.is-open {
    transform: translateY(0);
  }
`

export const MenuModalLinksWrap = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`

export const MenuModalLink = styled(Link)`
  font-size: 20px;
  font-weight: 400;
  margin-bottom: 35px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.black};
  
  &:last-child {
    left: 50%;
    bottom: 35px;
    margin-bottom: 0;
    position: absolute;
    transform: translateX(-50%);
    color: ${({ theme }) => theme.colors.error};
  }
`
