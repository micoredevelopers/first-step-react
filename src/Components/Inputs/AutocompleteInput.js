import React from 'react'
import clsx from 'clsx'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { AutocompleteS } from './Styles'

export const AutocompleteInput = ({ name, error, disabled, placeholder, onChange, options, optionLabel, openOnFocus, value, ...other}) => {
  const classes = AutocompleteS()
  return (
    <Autocomplete
      id={name}
      options={options}
      disabled={disabled}
      onChange={onChange}
      openOnFocus={openOnFocus}
      value={value}
      noOptionsText="Данные отсутствуют"
      getOptionLabel={opt => opt[optionLabel]}
      renderOption={(opt) => (
        <div className={classes.optionRoot}>
          {opt[optionLabel]}
        </div>
      )}
      ListboxProps={{ className: classes.listBoxRoot }}
      PaperComponent={props => <Paper {...props} className={classes.paperRoot}/>}
      renderInput={props => (
        <TextField
          {...props}
          variant="filled"
          placeholder={placeholder}
          className={classes.fieldRoot}
          inputProps={{
            ...props.inputProps,
            className: clsx(classes.inputRoot, error ? classes.err : ''),
          }}
        />
      )}
    />
  )
}
