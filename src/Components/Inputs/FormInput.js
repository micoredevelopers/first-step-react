import React  from 'react'
import MaskedInput from 'react-text-mask'
import { MenuButton } from '../Buttons'
import { useStaticListsStore } from '../../Store'
import { InfoChip, LightTooltip } from '../Custom'
import {
  InputIcon,
  MuiLabelS,
  MuiInputS,
  ChipAction,
  TooltipIcon,
  ChipsInputWrap,
  MuiFormControlS,
  ChipActionsWrap,
  ChipSelectedWrap
} from './Styles'

const TextMaskCustom = (props) => {
  const { inputRef, ...other } = props;
  
  return (
    <MaskedInput
      {...other}
      showMask
      placeholderChar="_"
      ref={ref => inputRef(ref ? ref.inputElement : null)}
      mask={['+', '3', '8', '0', ' ', '(', /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/]}
    />
  )
}

export const FormInput = (props) => {
  const {
    name,
    mask,
    icon,
    label,
    chips,
    error,
    value,
    accept,
    isValid,
    tooltip,
    multiple,
    onChange,
    moveChip,
    className,
    deleteChip,
    placeholder,
    type = 'text',
    ...other
  } = props
  const staticLists = useStaticListsStore()
  
  return (
    <MuiFormControlS style={props.style} className={className}>
      {icon && <MuiLabelS htmlFor={name}><InputIcon src={icon} alt="Input icon"/></MuiLabelS>}
      {label && <MuiLabelS htmlFor={name} children={label}/>}
      <MuiInputS
        id={name}
        icon={icon}
        type={type}
        name={name}
        error={error}
        value={value}
        onChange={onChange}
        inputProps={{
          'id': name,
          'accept': accept,
          'multiple': multiple,
          'placeholder': placeholder
          // 'ref': input => customInput = input
        }}
        inputComponent={mask && TextMaskCustom}
        {...other}
      />
      {tooltip && (
        <LightTooltip title={tooltip} placement="right" arrow>
          <TooltipIcon src="/assets/images/QuestionIcon.svg" alt="Help"/>
        </LightTooltip>
      )}
      {chips && (
        <ChipsInputWrap>
          {chips.map((chip, index) => {
            return (chip.name || chip) !== '' && (
              <ChipSelectedWrap key={index}>
                <InfoChip type="group" children={chip.name || chip} style={{ maxWidth: 'none' }}/>
                {chip.name && (
                  <ChipActionsWrap>
                    <MenuButton linksList={staticLists?.teachers} linkClick={moveChip(chip)} placeholder={{ title: 'Выберите учителя:' }}>
                      <ChipAction children="Перенести"/>
                    </MenuButton>
                    {!chip.students?.length > 0 && <ChipAction type="delete" onClick={deleteChip(chip.id)} children="Удалить"/>}
                  </ChipActionsWrap>
                )}
              </ChipSelectedWrap>
            )
          })}
        </ChipsInputWrap>
      )}
    </MuiFormControlS>
  )
}
