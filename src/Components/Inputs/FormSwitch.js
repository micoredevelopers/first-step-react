import React, { useState } from 'react'
import Switch from '@material-ui/core/Switch'
import { withStyles } from '@material-ui/core/styles'
import FormControlLabel from '@material-ui/core/FormControlLabel'

const CustomFormControl = withStyles(theme => ({
  root: {
    marginLeft: 0,
    display: 'flex',
    marginRight: 40,
    alignItems: 'center',
    flexDirection: 'row-reverse',
    [theme.breakpoints.down('lg')]: {
      marginRight: 0,
      marginBottom: 20
    }
  },
  label: {
    fontSize: 16,
    margin: '0 15px 0 0',
    fontFamily: 'Avenir Next Cyr'
  }
}))(({ classes, ...props }) => {
  return (
    <FormControlLabel
      classes={{
        root: classes.root,
        label: classes.label
      }}
      {...props}
    />
  )
})

const CustomSwitch = withStyles(() => ({
  root: {
    width: 62,
    height: 32,
    padding: 0
  },
  switchBase: {
    padding: 0,
    top: '50%',
    transform: 'translate(2px, -50%)',
    '&$checked': {
      color: '#fff',
      transform: 'translate(31px, -50%)',
      '& + $track': {
        opacity: 1,
        backgroundColor: '#003d7c'
      }
    }
  },
  thumb: {
    width: 29,
    height: 29,
    boxShadow: 'none',
    transition: 'all 0.3s ease-in-out'
  },
  track: {
    opacity: 1,
    borderRadius: 16,
    backgroundColor: '#bcbfc2',
    transition: 'all 0.3s ease-in-out'
  },
  checked: {}
}))(({ classes, ...props }) => {
  return (
    <Switch
      disableRipple
      classes={{
        root: classes.root,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
        switchBase: classes.switchBase
      }}
      focusVisibleClassName={classes.focusVisible}
      {...props}
    />
  )
})

export const FormSwitch = ({ name, label, handleChange }) => {
  const [state, setChecked] = useState({ checked: false })
  
  const handleChangeSwitch = (e) => {
    const { name, checked } = e.target
  
    handleChange({ name, checked })
    setChecked(prev => ({ ...prev, checked }))
  }
  
  return (
    <CustomFormControl
      label={label}
      control={<CustomSwitch checked={state.checked} onChange={handleChangeSwitch} name={name}/>}
    />
  )
}
