import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { toast } from 'react-toastify'
import { down } from 'styled-breakpoints'
import Input from '@material-ui/core/Input'
import { ApiFetch, noteUrl } from '../../Api'

const InputS = styled(Input)`
  width: 100%;
  padding: 0;
  height: auto;
  display: flex;
  font-size: 16px;
  margin-bottom: 0;
  border-radius: 6px;
  flex-direction: column;
  background-color: transparent;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.black};
  
  .MuiInputBase-input {
    padding: 10px 0;
  }
  
  ${down('md')} {
    font-size: 15px;
  }
`

export const NoteInput = () => {
  const [note, setNote] = useState({ comment: '' })

  const getComment = async () => {
    const apiCall = await ApiFetch(noteUrl)

    !apiCall.errors ?
      apiCall.data.comment && setNote(prev => ({ ...prev, comment: apiCall.data.comment })) :
      toast.error('Ошибка при загрузке заметок!')
  }

  const handleNoteChange = (e) => {
    const { value } = e.target

    setNote(prev => ({ ...prev, comment: value }))
  }
  const handleNoteSubmit = async () => {
    const apiCall = await ApiFetch(noteUrl, { method: 'POST', body: note })

    if (apiCall?.data) {
      toast.success('Заметка сохранена!')
    } else {
      toast.error('Произошла ошибка при сохранении заметки!')
    }
  }

  useEffect(() => {
    getComment()
  }, [])

  return (
    <InputS
      required
      rows={4}
      multiline
      type="text"
      name="comment"
      value={note.comment}
      onBlur={handleNoteSubmit}
      onChange={handleNoteChange}
      inputProps={{ 'id': 'comment' }}
    />
  )
}
