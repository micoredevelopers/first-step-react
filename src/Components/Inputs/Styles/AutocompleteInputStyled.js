import { makeStyles } from '@material-ui/core/styles'

export const AutocompleteS = makeStyles((theme) => ({
  fieldRoot: {
    '& .MuiInputBase-root': {
      height: 50,
      borderRadius: 6,
      padding: '0 !important',
      backgroundColor: '#fff',
      
      '&:before, &:after': {
        display: 'none'
      }
    },
    '& .MuiSvgIcon-root': {
      color: '#bcbfc2'
    }
  },
  inputRoot: {
    padding: 0,
    fontSize: 16,
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    margin: '0 70px 0 20px',
    textOverflow: 'ellipsis',
    fontFamily: "Avenir Next Cyr",

    '&::placeholder': {
      opacity: 1
    }
  },
  paperRoot: {
    maxWidth: 220,
    maxHeight: 250,
    borderRadius: 6,
    overflow: 'auto',
    boxShadow: '0 5px 10px rgba(0, 12, 24, 0.1)',
    
    '& .MuiAutocomplete-noOptions': {
      fontSize: 16,
      padding: '15px 20px 14px',
      fontFamily: "Avenir Next Cyr",
      color: 'color: rgb(0, 12, 24)',
    }
  },
  optionRoot: {
    fontSize: 16,
    width: '100%',
    color: '#000c18',
    padding: '15px 20px 14px',
    fontFamily: "Avenir Next Cyr",
    borderTop: '1px solid #e6e6e6'
  },
  listBoxRoot: {
    padding: 0,
    
    '& li': {
      padding: 0
    }
  },
  err: {
    color: theme.palette.error.main
  }
}))
