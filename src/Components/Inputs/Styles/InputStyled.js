import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Input from '@material-ui/core/Input'
import { Typography } from '@material-ui/core'
import InputLabel from '@material-ui/core/InputLabel'
import MuiStyled from '@material-ui/core/styles/styled'
import FormControl from '@material-ui/core/FormControl'

export const MuiFormControlS = MuiStyled(FormControl)(({ theme }) => ({
  width: '100%',
  maxWidth: 220,
  marginBottom: 0,
  display: 'flex',
  alignItems: 'center',
  position: 'relative',
  flexDirection: 'row',
  [theme.breakpoints.down('md')]: {
    maxWidth: '100%'
  }
}))

export const MuiLabelS = MuiStyled(InputLabel)(({ theme }) => ({
  fontSize: 16,
  cursor: 'pointer',
  transform: 'none',
  position: 'relative',
  fontFamily: "Avenir Next Cyr",
  color: theme.palette.text.secondary,
  [theme.breakpoints.down('md')]: {
    fontSize: 15
  }
}))

export const MuiInputS = MuiStyled(Input)(({ theme, ...props }) => ({
  fontSize: 16,
  width: '100%',
  borderRadius: 6,
  marginBottom: 0,
  alignItems: 'center',
  backgroundColor: '#fff',
  fontFamily: "Avenir Next Cyr",
  height: props.multiline ? '100%' : '50px',
  display: props.type === 'file' ? 'none' : 'flex',
  color: props.error ? theme.palette.error.main : theme.palette.text.primary,
  padding: props.multiline ? '15px 20px 0' : props.icon ? '0 20px 0 40px' : '0 20px',
  
  '&:before, &:after': {
    display: 'none'
  },
  
  '& input, & textarea': {
    '&::placeholder': {
      opacity: 1,
      color: props.error ? theme.palette.error.main : theme.palette.text.secondary
    }
  },
  
  [theme.breakpoints.down('md')]: {
    width: '100%',
    fontSize: 15,
    height: props.multiline ? '100%' : 46
  }
}))

export const InputIcon = styled.img`
  //top: 50%;
  //left: 20px;
  //z-index: 2;
  width: 18px;
  height: 18px;
  min-width: 18px;
  margin-right: 10px;
  //position: absolute;
  //transform: translateY(-50%);
  
  ${down('md')} {
    width: 12px;
    height: 12px;
    min-width: 12px;
    margin-right: 5px;
  }
`

export const ChipsInputWrap = styled.div`
  width: 100%;
  display: flex;
  overflow: auto;
  margin-top: 10px;
  align-items: center;
  padding-bottom: 15px;
  justify-content: flex-start;
`

export const ChipSelectedWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const ChipActionsWrap = styled.div`
  display: flex;
  margin-right: 25px;
  flex-direction: column;
  align-items: flex-start;
`

export const ChipAction = styled(Typography)`
  font-size: 14px;
  cursor: pointer;
  font-family: "Avenir Next Cyr";
  color: ${({ theme, type }) => type === 'delete' ? theme.colors.error : theme.colors.main};
`

export const TooltipIcon = styled.img`
  top: 25px;
  z-index: 1;
  width: 22px;
  height: 22px;
  right: -35px;
  cursor: pointer;
  position: absolute;
  transform: translateY(-50%);
  
  ${down('lg')} {
    top: 23px;
    right: 15px;
  }
`
