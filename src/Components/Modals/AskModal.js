import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import { EventEmitter } from 'events'
import { CustomBtn } from '../Buttons'
import { ModalInnerContent } from './ModalInnerContent'

export const BtnRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  
  ${down('lg')} {
    flex-direction: column;
  }
`

export const BtnAccept = styled(CustomBtn)`
  margin-left: 15px;
  background-color: ${({ theme }) => theme.colors.error} !important;
  
  &:hover {
    background-color: #b30e05 !important;
  }
  
  ${down('lg')} {
    margin-left: 0;
    margin-top: 15px;
  }
`

const questionEvent = new EventEmitter()
const initState = { title: '', subTitle: '', btnTitle: '', isOpen: false, callback: undefined }
export const ask = (options) => questionEvent.emit('ask', options)

export const AskModal = () => {
  const [state, setState] = useState(initState)
  
  const resetModal = () => setState(initState)
  const handleAsk = (options) => {
    setState(prev => ({ ...prev, isOpen: true, ...options }))
  }
  const setAnswer = (answer) => () => {
    if (answer) {
      state.callback && state.callback(answer)
      resetModal()
    } else {
      resetModal()
    }
  }
  
  /* Effects */
  useEffect(() => {
    questionEvent.on('ask', handleAsk)
    
    return () => {
      questionEvent.removeListener('ask', handleAsk)
    }
  }, [])
  
  return (
    <ModalInnerContent id="ask_modal" open={state.isOpen} close={setAnswer(false)} title={state.title} subTitle={state.subTitle}>
      <BtnRow>
        <CustomBtn title="Отменить" onClick={setAnswer(false)}/>
        <BtnAccept title={state.btnTitle} onClick={setAnswer(true)}/>
      </BtnRow>
    </ModalInnerContent>
  )
}

