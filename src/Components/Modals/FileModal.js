import React from 'react'
import { getToken } from '../../Helpers'
import { filesDownloadUrl } from '../../Api'
import { CustomBtn, ModalInnerContent } from '../index'

export const FileModal = ({ files, open, close }) => {
  const authToken = getToken()
  // const ext = files.map(file => file.ext).join(',')
  const tokens = files.map(file => file.token).join(',')
  const name = files.map(file => file.path.split('/')[1]).join(',')
  const downloadFilesUrl = `${filesDownloadUrl}/${tokens}?token=${authToken}`
  
  return (
    <ModalInnerContent
      open={open}
      title="Файл"
      close={close}
      id="file-homework"
      subTitle={files.length === 0 ? 'Нет файлов прикрепленных к домашнему заданию' : name}
    >
      {files.length > 0 && <CustomBtn title="Скачать" href={downloadFilesUrl} target="_blank"/>}
    </ModalInnerContent>
  )
}
