import React from 'react'
import styled from 'styled-components'
import { CustomForm } from '../Form'

export const ModalWrap = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 998;
  position: fixed;
  padding-top: 100px;
  background-color: #efeff0;
  transform: translateY(-100%);
  transition: all 0.6s ease-in-out;
  
  &.is-opened {
    transform: translateY(0);
  }
`

export const ModalRow = styled.div`
  display: flex;
  padding: 0 15px;
  flex-direction: row;
  align-items: center;
  margin-bottom: 20px;
  justify-content: space-between;
`

export const ModalTitle = styled.h1`
  font-size: 26px;
  font-weight: 700;
  line-height: normal;
  font-family: Comfortaa;
  color: ${({ theme }) => theme.colors.main};
`

export const ModalClose = styled.div`
  padding: 8px;
  margin: -8px;
  display: flex;
  align-items: center;
  justify-content: center;
  
  img {
    width: 16px;
    height: 16px;
  }
`

export const FiltersModal = ({ open, closeModal, submit, filters, filtersList, clear }) => {
  const formFields = filtersList.filter(filter => filter.filter && filter).map(filter => ({
    size: { xs: 12 },
    name: filter.filter.id,
    placeholder: filter.label,
    onChange: filter.onChange,
    options: filter.filter.options || filter.options,
    type: filter.filter.type === 'search' ? 'input' : filter.filter.type,
    defaultValue: (filters && filters[filter.filter.id]) ? filters[filter.filter.id] : ''
  }))
  
  const handleSaveFilters = (data) => {
    closeModal()
    submit(data)
  }
  const handleClearFilters = () => {
    closeModal()
    clear()
  }
  
  return (
    <ModalWrap className={open ? 'is-opened' : ''}>
      <ModalRow>
        <ModalTitle children="Фильтр"/>
        <ModalClose onClick={closeModal}>
          <img src="/assets/images/Close.svg" alt="Close"/>
        </ModalClose>
      </ModalRow>
      <CustomForm
        withClear
        fields={formFields}
        btnTitle="Сохранить"
        clearFn={handleClearFilters}
        handleSubmit={handleSaveFilters}
      />
    </ModalWrap>
  )
}
