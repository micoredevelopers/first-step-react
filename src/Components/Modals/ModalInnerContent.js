import React from 'react'
import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Dialog from '@material-ui/core/Dialog'
import { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import { checkDevice } from '../../Helpers'

const useStyles = makeStyles((theme) => ({
  dialogRoot: {
    top: '0 !important',
    left: '0 !important',
    right: '0 !important',
    bottom: '0 !important'
  },
  dialogTitle: {
    padding: 0,
    marginBottom: 5,
    maxWidth: '100%',
    '& h2': {
      fontSize: 48,
      fontWeight: 700,
      color: '#000c18',
      textAlign: 'center',
      fontFamily: 'Comfortaa',
      [theme.breakpoints.down('md')]: {
        fontSize: 26
      }
    },
    [theme.breakpoints.down('md')]: {
      marginBottom: 10
    }
  },
  dialogContent: {
    padding: 0,
    width: '100%',
    overflow: 'visible',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column'
  },
  dialogActions: {
    padding: 0
  }
}))

export const DialogWrap = styled.div`
  display: flex;
  padding: 100px 0;
  flex-direction: column;
  background-color: #efeff0; 
  justify-content: flex-start;
  
  ${down('md')} {
    padding: 45px 15px 50px;
  }
`

export const DialogSubTitle = styled(Typography)`
  width: 100%;
  font-size: 16px;
  max-width: 520px;
  font-weight: 400;
  text-align: center;
  margin: 0 auto 35px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
  
  ${down('md')} {
    font-size: 15px;
    margin-bottom: 30px;
  }
  
  a {
    cursor: pointer;
    font-size: inherit;
    font-weight: inherit;
    font-family: inherit;
    color: ${({ theme }) => theme.colors.main};
  }
`

const CloseButton = styled.button`
  position: absolute;
  display: flex;
  align-items: center;
  top: 25px;
  right: 25px;
  width: 41px;
  height: 41px;
  background-color: transparent;
  border: none;
  cursor: pointer;
  opacity: 0.2;
  outline: none !important;
  
  span {
    display: block;
    position: absolute;
    top: 50%;
    width: 100%;
    height: 3px;
    border-radius: 1px;
    background-color: #222226;
    z-index: 1;
    
    &:nth-child(1) {
      transform: translateY(-50%) rotate(45deg);
    }
    
    &:nth-child(2) {
      transform: translateY(-50%) rotate(-45deg);
    }
  }
  
  ${down('md')} {
    top: 20px;
    right: 20px;
    width: 25px;
    height: 25px;
    
    span {
      height: 2px;
    }
  }
`

export const ModalInnerContent = ({ id, close, open, title, subTitle, children }) => {
  const classes = useStyles()
  
  return (
    <Dialog
      fullWidth
      open={open}
      maxWidth="lg"
      onClose={close}
      aria-labelledby={id}
      className={classes.dialogRoot}
      fullScreen={checkDevice('min-lg')}
    >
      <DialogWrap>
        {title && <DialogTitle id={id} className={classes.dialogTitle}>{title}</DialogTitle>}
        {subTitle && <DialogSubTitle variant="h6">{subTitle}</DialogSubTitle>}
        <DialogContent className={classes.dialogContent}>
          {children}
        </DialogContent>
        <DialogActions className={classes.dialogActions}>
          <CloseButton onClick={close}>
            <span/>
            <span/>
          </CloseButton>
        </DialogActions>
      </DialogWrap>
    </Dialog>
  )
}
