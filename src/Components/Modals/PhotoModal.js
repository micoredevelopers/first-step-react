import React, {useState} from 'react'
import styled, {css} from 'styled-components'
import { down } from 'styled-breakpoints'
import { checkDevice, getToken } from '../../Helpers'
import { filesDownloadUrl, fileUrl } from '../../Api'
import { ModalSlider, ModalInnerContent, CustomBtn } from '../index'

const slideSettings = {
  dots: true,
  speed: 700,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1
}

export const BtnRows = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  ${down('sm')} {
    flex-direction: column;
  }
`

const PhotoWrapper = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  padding-bottom: 30px;
`

const PhotoButton = styled.img`
  cursor: pointer;
  width: 35px;
  height: 35px;
  ${props => props.disabled && (
      css`
        opacity: .3;
        pointer-events: none;
      `
  )}
  ${down('md')} {
    width: 25px;
    height: 25px;
  };
  ${(props) => {
    switch (props.position){
      case'left':
        return css`
          margin-right: 40px;
          ${down('md')} {
            margin-right: 20px;
          };
        `
      case'right':
        return css`
          margin-left: 40px;
          ${down('md')} {
            margin-left: 20px;
          };
        `
      default:
        return css``
    }
  }}
`

const ResizeWrapper = styled.div`
  position: absolute;
  bottom: 10px;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
  justify-content: center;
  gap: 8px;
`

export const PhotoModal = ({ isPhotoFiles, slides, open, close, title, subTitle, btnOptions }) => {
  const authToken = getToken()
  const tokens = isPhotoFiles && slides.map(file => file.token).join(',')
  const otherFiles = isPhotoFiles && slides.filter(file => !file.isImg)
  const downloadImgUrl = isPhotoFiles && `${slides.length > 1 ? filesDownloadUrl : fileUrl}/${tokens}?token=${authToken}`

  const [rotateValue, setRotateValue] = useState(0);
  const [scaleValue, setScaleValue] = useState(1);

  const handleRotate = (side) => () => {
    setScaleValue(1)
    let nextRotateValue = rotateValue
    switch (side){
      case 'left':
        --nextRotateValue
        break
      case 'right':
        nextRotateValue++
        break
      default: break
    }
    if(nextRotateValue % 4){
      setRotateValue(nextRotateValue)
    }else {
      setRotateValue(0)
    }
  }

  const handleResize = (mode) => () => {
    switch (mode){
      case '+':
        setScaleValue((prevState => +(prevState + 1.2).toFixed(2)))
          break;
      case '-':
        setScaleValue((prevState => +(prevState - 1.2).toFixed(2)))
        break;
      default: break
    }
  }
  
  return (
    <ModalInnerContent
      open={open}
      title={title}
      close={close}
      id="photo-modal"
      subTitle={(isPhotoFiles && otherFiles.length > 0) ?
        `Невозможно просмотреть содержимое файла ${otherFiles[0].path}` :
        subTitle}
    >
      <PhotoWrapper>
        {otherFiles.length === 0 && <ModalSlider rotate={rotateValue} scale={scaleValue} settings={slideSettings} slides={slides}/>}
        <ResizeWrapper>
          <PhotoButton onClick={handleRotate('left')} position='left' src="/assets/images/rotate-ccw.svg"/>
          <PhotoButton disabled={scaleValue === 1} onClick={handleResize('-')} position='relative' src="/assets/images/zoom-out.svg"/>
          <PhotoButton disabled={scaleValue === 7} onClick={handleResize('+')} position='relative' src="/assets/images/zoom-in.svg"/>
          <PhotoButton onClick={handleRotate('right')} position='right' src="/assets/images/rotate-cw.svg"/>
        </ResizeWrapper>
      </PhotoWrapper>
      <BtnRows>
        {btnOptions && btnOptions.map((btn, index) => (
          <CustomBtn
            key={index}
            color={btn.color}
            title={btn.title}
            onClick={btn.event}
            style={{ margin: checkDevice('min-sm') ? '0 auto 15px' : '0 15px 0 0' }}
          />
        ))}
        {(isPhotoFiles && slides.length > 0) && <CustomBtn title="Скачать все" href={downloadImgUrl} target="_blank"/>}
      </BtnRows>
    </ModalInnerContent>
  )
}
