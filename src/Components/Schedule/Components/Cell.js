import React from 'react'
import { MonthDay } from '../Styles'
import { formatDate } from '../../../Helpers'

export const Cell = ({ date }) => {
  const day = formatDate(date, 'd')

  return <MonthDay>{day}</MonthDay>
}
