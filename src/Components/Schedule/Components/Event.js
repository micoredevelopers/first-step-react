import React from 'react'
import { EventCard } from '../Styles'
import { formatDate } from '../../../Helpers'

export const Event = (props) => {
  return (
    <EventCard>
      <div className="left-wrap">
        {props.group && <div className="group-wrap"><span>{props.group.name}</span></div>}
        <div className="subject-text">{props.subject}</div>
      </div>
      <div className="time-wrap"><span>{formatDate(props.date, 'HH:mm')}</span></div>
    </EventCard>
  )
}
