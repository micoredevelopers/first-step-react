import React, { useState } from 'react'
import { toast } from 'react-toastify'
import { ask } from '../../Modals'
import { InfoChip } from '../../Custom'
// import { FormInput } from '../../Inputs'
import { MenuButton } from '../../Buttons'
import { MenuIcon } from '../../Table/Styles'
import { StyledTheme } from '../../../Styles/Theme'
import {
  ActionLink,
  ActionsWrap,
  PopupHeader,
  QICCHipsWrap,
  PopupContent,
  QICLessonName,
  QICLessonDescription
} from '../Styles'
import { deleteLesson, updateLesson } from '../../../Api'
import { MobileView, DesktopView, formatDate, getAccountType } from '../../../Helpers'
// import { ChipSelect } from '../../Selects'
// import { TypeOptions } from '../../../Constants'
// import { StyledTheme } from '../../../Styles/Theme'

const MenuS = {
  title: {
    fontSize: 14,
    color: '#003d7c',
    cursor: 'pointer',
    lineHeight: '20px',
    margin: '0 20px 5px 0'
  }
}

const MenuLinks = ({ goToLesson, deleteLesson, editLesson }) => [
  {
    id: 'goToLessons',
    event: goToLesson,
    title: 'Перейти к уроку'
  },
  {
    id: 'edit',
    event: editLesson,
    title: 'Редактировать'
  },
  {
    id: 'delete',
    title: 'Удалить',
    event: deleteLesson,
    style: { color: '#d51006' }
  }
]

export const QuickInfoContent = (props) => {
  const {
    id,
    type,
    update,
    History,
    subject,
    typeName,
    viewOnly,
    teachers,
    openModal,
    description,
    replacement,
    ...other
  } = props
  const account = getAccountType()
  const [sendData, setSendData] = useState({
    type,
    subject,
    description,
    replacement: replacement ? replacement.id : ''
  })
  const { StartTime, EndTime, Id, Guid, elementType, ...otherLesson } = other
  const lesson = { ...otherLesson, id, type, subject, typeName, description, replacement }
  const formattedTeachers = teachers.map(teacher => ({ ...teacher, format: opt => opt.fullName }))
  
  const updateLessonData = async (params) => {
    const apiData = await updateLesson(id, { ...sendData, ...params })

    if (apiData) {
      toast.success('Урок успешно обновлен!')
      update()
    }
  }
  
  const handleDeleteLesson = async () => {
    ask({
      btnTitle: 'Удалить',
      title: 'Удаление урока',
      subTitle: `Вы уверены что хотите удалить урок "${subject}"?`,
      callback: async () => {
        const apiData = await deleteLesson(id)

        if (apiData) {
          toast.success('Урок успешно удален!')
          update()
        }
      }
    })
  }
  
  const handleMenuClick = (teacher) => {
    setSendData(prev => ({ ...prev, replacement: teacher.id }))
    updateLessonData({ replacement: teacher.id })
  }
  
  const goToLesson = () => {
    switch (account) {
      case 'admin':
        History.push(`/admin/teachers/${lesson.teacher.id}`, { lesson })
        break
      case 'teacher':
        History.push('/teacher/lessons', { lessonId: id })
        break
      case 'student':
        History.push('/student/marks', { lessonId: id })
        break
      default: return
    }
  }
  
  // const handleInputSubmit = () => {
  //   window.innerWidth > 1060 && updateLessonData()
  // }
  // const handleInputChange = (e) => {
  //   const { name, value } = e.target
  //
  //   setSendData(prev => ({ ...prev, [name]: value }))
  // }
  // const handleTypeChange = (e) => {
  //   const { name, value } = e.target
  //   console.log(name, value)
  // }
  // const formatType = (option) => option.name.split('')[0]
  
  return (
    <>
      <PopupHeader>
        <QICLessonName>{subject}</QICLessonName>
        <MobileView>
          {!viewOnly && (
            <MenuButton
              id="quick_info_menu"
              placement="bottom-end"
              linksList={MenuLinks({ goToLesson, editLesson: openModal(lesson), deleteLesson: handleDeleteLesson })}
            >
              <MenuIcon src="/assets/images/MenuIcon.svg" alt="Menu action"/>
            </MenuButton>
          )}
        </MobileView>
        <DesktopView>
          {/*<FormInput*/}
          {/*  name="subject"*/}
          {/*  disabled={viewOnly}*/}
          {/*  className="object-text"*/}
          {/*  value={sendData.subject}*/}
          {/*  onBlur={handleInputSubmit}*/}
          {/*  onChange={handleInputChange}*/}
          {/*  style={{ marginBottom: 0, padding: 0 }}*/}
          {/*/>*/}
          {/*<ChipSelect*/}
          {/*  name="type"*/}
          {/*  value={type}*/}
          {/*  theme={StyledTheme}*/}
          {/*  format={formatType}*/}
          {/*  options={TypeOptions}*/}
          {/*  onChange={handleTypeChange}*/}
          {/*/>*/}
          <InfoChip
            type="type"
            tooltip={typeName}
            theme={StyledTheme}
            children={typeName.split('')[0]}
          />
        </DesktopView>
      </PopupHeader>
      <PopupContent>
        <QICLessonDescription>{description}</QICLessonDescription>
        <MobileView>
          <QICCHipsWrap>
            {!viewOnly && <InfoChip type="group" tooltip="Группа" theme={StyledTheme} children={other.group.name}/>}
            <InfoChip
              type="type"
              tooltip={typeName}
              theme={StyledTheme}
              children={typeName.split('')[0]}
            />
            <InfoChip type="date" theme={StyledTheme}>
              {formatDate(other.StartTime, 'dd MMM, HH:mm')}
            </InfoChip>
          </QICCHipsWrap>
          <MenuButton
            id="replacement"
            customStyles={MenuS}
            linkClick={handleMenuClick}
            linksList={formattedTeachers}
            placeholder={{ title: 'Замена', val: replacement?.name }}
            title={replacement ? `Заменяет ${replacement.name}` : 'Замена'}
          />
        </MobileView>
        <DesktopView>
          {/*<FormInput*/}
          {/*  rows={4}*/}
          {/*  multiline*/}
          {/*  name="description"*/}
          {/*  disabled={viewOnly}*/}
          {/*  className="content-text"*/}
          {/*  onBlur={handleInputSubmit}*/}
          {/*  onChange={handleInputChange}*/}
          {/*  style={{ padding: 0, maxWidth: '100%' }}*/}
          {/*  value={sendData.description ? sendData.description : ''}*/}
          {/*/>*/}
          <ActionsWrap>
            {!viewOnly && (
              <>
                <ActionLink onClick={handleDeleteLesson} className="btn-delete" children="Удалить"/>
                <MenuButton
                  id="replacement"
                  customStyles={MenuS}
                  linkClick={handleMenuClick}
                  linksList={formattedTeachers}
                  placeholder={{ title: 'Замена', val: replacement?.fullName }}
                  title={replacement ? `Заменяет ${replacement.fullName}` : 'Замена'}
                />
                <ActionLink onClick={openModal(lesson)} children="Редактировать"/>
              </>
            )}
            <ActionLink onClick={goToLesson} children="Перейти к уроку"/>
          </ActionsWrap>
        </DesktopView>
      </PopupContent>
    </>
  )
}
