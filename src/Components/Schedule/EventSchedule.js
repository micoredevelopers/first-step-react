import React, { useEffect, useRef, useState } from 'react'
import {
  Month,
  Inject,
  MonthAgenda,
  ViewDirective,
  ViewsDirective
} from '@syncfusion/ej2-react-schedule'
import { useHistory } from 'react-router'
import { ModalInnerContent } from '../Modals'
import { CreateLessonFields } from '../Fields'
import { DateSelect, FormSelect } from '../Selects'
import { Cell, Event, QuickInfoContent } from './Components'
import { checkDevice, DesktopView, formatDate, getUser } from '../../Helpers'
import {
  useLessonsStore,
  useTeachersStore,
  useLessonsDispatch,
  useStaticListsStore
} from '../../Store'
import {
  DateArrow,
  SelectRow,
  FiltersRow,
  SelectArrow,
  MonthNameRow,
  DatePickerRow,
  ScheduleComponentS
} from './Styles'

export const EventSchedule = ({ update, handleChangeDate, handleChangeSelect, viewOnly }) => {
  const user = getUser()
  const History = useHistory()
  let scheduleObj = useRef(null)
  const { teachers } = useStaticListsStore()
  const lessonsDispatch = useLessonsDispatch()
  const { selectedTeacher } = useTeachersStore()
  const { lessons, selectedLesson } = useLessonsStore()
  const newLessons = lessons.map(lesson => {
    const { date, group, ...other } = lesson
    
    if (viewOnly) {
      return {
        date,
        EndTime: date,
        StartTime: date,
        ...other
      }
    } else {
      return {
        date,
        group,
        EndTime: date,
        StartTime: date,
        ...other
      }
    }
  })
  const [openModal, setOpenModal] = useState(false)
  const [scheduleData, setScheduleData] = useState(new Date())
  const [currentMonth, setCurrentMonth] = useState(formatDate(new Date(), 'MMMM'))
  
  const handleDateChange = date => {
    handleChangeDate(date)
    
    if (scheduleObj !== null) {
      scheduleObj.selectedDate = date
      scheduleObj.dataBind()
    }
    
    setScheduleData(date)
    setCurrentMonth(formatDate(date, 'MMMM'))
  }
  const handleChangeTeacher = (e) => {
    handleChangeSelect(e)
  }
  const handleNavDateMobile = (date) => {
    handleChangeDate(date)
    
    if (scheduleObj !== null) {
      scheduleObj.selectedDate = date
      scheduleObj.dataBind()
    }
    
    setScheduleData(date)
    setCurrentMonth(formatDate(date, 'MMMM'))
  }
  const handleNavDate = (direction) => () => {
    const currentDate = new Date(scheduleObj.selectedDate)
    const nextDate = new Date(currentDate.getFullYear(),
      direction === 'prev' ? currentDate.getMonth() - 1 : currentDate.getMonth() + 1, 1)
    
    handleChangeDate(nextDate)
    
    if (scheduleObj !== null) {
      scheduleObj.selectedDate = nextDate
      scheduleObj.dataBind()
    }
    
    setScheduleData(nextDate)
    setCurrentMonth(formatDate(nextDate, 'MMMM'))
  }
  
  const updateSchedule = () => {
    if (scheduleObj) {
      scheduleObj.dataBind()
      scheduleObj.refresh()
    }
    update && update()
  }
  
  /* MODAL EVENT */
  const handleOpenModal = (lesson) => () => {
    setOpenModal(true)
    lessonsDispatch({
      selectedLesson: lesson,
      type: 'SET_SELECTED_LESSON_SUCCESS'
    })
  }
  const handleCloseModal = () => {
    setOpenModal(false)
  }
  
  return (
    <>
      <FiltersRow>
        {user.type === 'admin' && (
          <DesktopView>
            <SelectRow>
              <FormSelect
                name="teachers"
                placeholder="Учитель"
                value={selectedTeacher?.id}
                onChange={handleChangeTeacher}
                options={teachers.map(teacher => ({
                  ...teacher,
                  format: option => option.fullName
                }))}
                icon={() => <SelectArrow src="/assets/images/DateArrow.svg" alt="Arrow"/>}
              />
            </SelectRow>
          </DesktopView>
        )}
        <DatePickerRow>
          <DateArrow className="prev" onClick={handleNavDate('prev')}>
            <img src="/assets/images/DateArrow.svg" alt="Prev"/>
          </DateArrow>
          <DateSelect
            format="MMMM yyyy"
            name="scheduleDate"
            value={scheduleData}
            onChange={handleDateChange}
          />
          <DateArrow className="next" onClick={handleNavDate('next')}>
            <img src="/assets/images/DateArrow.svg" alt="Next"/>
          </DateArrow>
        </DatePickerRow>
      </FiltersRow>
      <DesktopView>
        <MonthNameRow>{currentMonth}</MonthNameRow>
      </DesktopView>
      {!openModal && (
        <ScheduleComponentS
          locale="ru"
          height="800px"
          allowResizing={false}
          showHeaderBar={false}
          ref={s => scheduleObj = s}
          eventSettings={{
            allowAdding: false,
            allowEditing: false,
            enableIndicator: false,
            dataSource: newLessons ? newLessons : [],
            template: props => checkDevice('min-lg') ? (
              <QuickInfoContent
                History={History}
                teachers={teachers}
                viewOnly={viewOnly}
                update={updateSchedule}
                openModal={handleOpenModal}
                {...props}
              />
            ) : <Event {...props}/>
          }}
          cellHeaderTemplate={Cell}
          quickInfoTemplates={checkDevice('xl') ? {
            content: props => (
              <QuickInfoContent
                History={History}
                teachers={teachers}
                viewOnly={viewOnly}
                update={updateSchedule}
                openModal={handleOpenModal}
                {...props}
              />
            )
          } : null}
          allowVirtualScrolling={false}
          showQuickInfo={checkDevice('xl')}
          navigating={props => handleNavDateMobile(props.currentDate)}
        >
          <ViewsDirective>
            <ViewDirective option={checkDevice('min-lg') ? 'MonthAgenda' : 'Month'} firstDayOfWeek={1}/>
          </ViewsDirective>
          <Inject services={[Month, MonthAgenda]}/>
        </ScheduleComponentS>
      )}
  
      {(openModal && selectedLesson) && (
        <ModalInnerContent
          open={openModal}
          id="lesson_edit_modal"
          close={handleCloseModal}
          title="Редактирование урока"
          subTitle="Все поля, кроме описания и замены, обязательны к заполнению"
        >
          <CreateLessonFields lesson={selectedLesson} close={handleCloseModal} update={update}/>
        </ModalInnerContent>
      )}
    </>
  )
}
