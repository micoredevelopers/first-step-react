import styled from 'styled-components'
import { ScheduleComponent } from '@syncfusion/ej2-react-schedule'

export const ScheduleComponentS = styled(ScheduleComponent)`
  border: none;
  background-color: #fff;
  height: 100% !important;
  border-radius: 0 0 16px 16px;

  .e-schedule-toolbar {
    margin-bottom: 0;
    padding: 40px 40px 5px;
    border: none;
    box-shadow: none;
    background: #fff;
  }

  .e-toolbar {
    .e-toolbar-items {
      background: #fff;
      
      &.e-tbar-pos {
        height: auto;
        min-height: auto;
        
        .e-toolbar-left {
          position: relative;
          margin: 0 auto;
          padding: 0 40px;
        }
      }
      
      .e-toolbar-item {
        &.e-prev, &.e-next {
          position: absolute;
          top: 50%;
          transform: translateY(-50%);
        }
        
        &.e-prev {
          left: 0;
        }
        
        &.e-next {
          right: 0;
        }
      }
    
      .e-toolbar-right {
        display: none;
      }
    }
  }

  .e-more-event-header {
    display: none;
  }
  
  .e-more-popup-wrapper {
    border: none;
    width: 185px;
    padding: 10px 0;
    background-color: #fff;
    box-shadow: 0 0 12px rgba(0, 12, 24, 0.1);
    
    .e-more-event-content {
      padding: 0;
    }
    
    .e-more-appointment-wrapper {
      margin: 0;
    }
    
    .e-appointment {
      padding: 0;
      height: auto;
      border: none;
      margin: 0 0 5px;
      border-radius: 0;
      background-color: transparent;
      
      &.e-appointment-border {
        opacity: 0.4;
        box-shadow: none;
      }
      
      &:focus {
        opacity: 0.4;
        box-shadow: none;
      }
      
      &:last-child {
        margin-bottom: 0;
      }
    }
  }
  
  .e-month-view {
    .e-appointment {
      border: none;
      height: 25px;
      margin-bottom: 5px;
      background: transparent;
      
      &.e-appointment-border {
        opacity: 0.4;
        box-shadow: none;
      }
      
      &:focus {
        opacity: 0.4;
        box-shadow: none;
      }
      
      .e-appointment-details {
        &>div {
          width: 100%;
        }
      }
    }
  
    .e-current-date {
      .e-date-header {
        width: 30px;
        height: 30px;
        padding: 0;
        margin: 5px 0 5px auto;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: ${({ theme }) => theme.colors.main};
        
        &>div {
          color: #fff;
        }
      }
     } 
  
    .e-work-cells {
      background-color: #fff;
      border-color: #e6e6e6;
      
      &:hover {
        color: ${({ theme }) => theme.colors.black};
        background-color: #fff;
      }
    }
     
    .e-date-header-wrap {
      table {
        td {
          padding: 0 10px 10px;
          text-align: right;
          border-left: none;
          
          &.e-current-day {
            color: ${({ theme }) => theme.colors.main};
          }
          
          span {
            color: #000c18;
            font-size: 20px;
            font-weight: 400;
            text-transform: lowercase;
            font-family: "Avenir Next Cyr", sans-serif;
          }
        }
      }
    }
  
    .e-date-header {
      margin: 0;
      width: 100%;
      height: auto;
      padding: 10px;
      display: flex;
      justify-content: flex-end;
      
      &:hover {
        text-decoration: none;
      }
    }
    
    .e-more-indicator {
      color: #003d7c;
      font-size: 14px;
      font-weight: 400;
      text-align: center;
      line-height: normal;
      font-family: "Avenir Next Cyr";
    }
  }
  
  .e-quick-popup-wrapper {
    box-shadow: 0 0 12px rgba(0, 12, 24, 0.1);
  }
  
  .e-event-popup {
    padding: 20px;
    
    .e-popup-header {
      display: none;
      background-color: transparent;
    }
    
    .e-popup-content {
      padding: 0;
    }
  }
  
  @media screen and (max-width: 1060px) {
    border-radius: 0;
    background-color: transparent;
    
    .e-month-view .e-date-header-wrap table td,
    .e-month-agenda-view .e-date-header-wrap table td {
      border-bottom: none;
      padding: 0 5px 10px;
      background-color: transparent;
      
      &.e-current-day {
        span {
          color: #003d7c;
        }
      }
      
      span {
        color: #8f9398;
        font-size: 16px;
        text-transform: lowercase;
        font-family: "Avenir Next Cyr";
      }
    }
    
    .e-month-agenda-view .e-week-number-wrapper .e-schedule-table,
    .e-month-agenda-view .e-content-table {
      border-bottom: none;
    }
    
    .e-month-view .e-work-cells,
    .e-month-agenda-view .e-work-cells {
      color: #000c18;
      padding-top: 5px;
      padding-bottom: 20px;
      background-color: transparent;
      border-bottom: 1px solid #e6e6e6;
      
      &.e-selected-cell {
        color: #fff;
        background-color: transparent;
        
        .e-date-header {
          border-radius: 100%;
          background-color: #003d7c;
        }
      }
      
      &.e-other-month {
        color: #8f9398;
      }
      
      &.e-current-date {
        .e-date-header {
          color: #003d7c;
          background-color: transparent;
        }
      }
    }
    
    .e-month-agenda-view .e-appointment-indicator {
      background-color: #8f9398;
    }
    
    .e-month-agenda-view .e-date-header {
      margin: 0 auto;
    }
    
    .e-month-agenda-view .e-wrapper-container {
      height: auto !important;
      
      .e-appointment-wrap {
        padding: 0;
        height: auto !important;
        background-color: transparent;
      }
      
      .e-appointment-container {
        min-height: auto;
      
        &.e-no-event {
          display: none;
        }
      }
      
      .e-agenda-item {
        &.e-month-agenda-view {
          padding: 0;
          margin-bottom: 20px;
        }
      }
      
      .e-appointment {
        border-left: none;
        padding: 22px 20px;
        border-radius: 16px;
        background-color: #fff;
      }
    }
  }
`

export const MonthDay = styled.div`
  font-size: 20px;
  text-align: center;
  font-family: "Avenir Next Cyr";
  
  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
`

export const EventCard = styled.div`
  width: 100%;
  padding: 5px;
  height: auto;
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: space-between;
  background-color: rgba(0, 61, 124, 0.1);
  
  .left-wrap {
    width: 100%;
    max-width: 65%;
    display: flex;
    align-items: center;
  }
  
  .group-wrap {
    width: 20px;
    height: 20px;
    display: flex;
    min-width: 20px;
    margin-right: 5px;
    border-radius: 10px;
    align-items: center;
    justify-content: center;
    background-color: #003d7c;
    
    span {
      color: #ffffff;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      font-size: 12px;
      font-weight: 700;
      text-transform: uppercase;
      font-family: 'Comfortaa', sans-serif;
    }
  }
  
  .subject-text {
    width: 100%;
    color: #000c18;
    min-width: 50px;
    font-size: 14px;
    font-weight: 400;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    font-family: "Avenir Next Cyr", sans-serif;
  }
  
  .time-wrap {
    width: 50px;
    display: flex;
    align-items: center;
    border-radius: 10px;
    justify-content: center;
    background-color: #d51006;
    
    span {
      color: #ffffff;
      font-family: Comfortaa;
      font-size: 12px;
      font-weight: 700;
    }
  }
`

export const PopupHeader = styled.div`
  display: flex;
  margin-bottom: 5px;
  align-items: center;
  justify-content: space-between;
  
  .object-text {
    .MuiInputBase-root {
      padding: 0;
      height: auto;
      color: #000c18;
      font-size: 16px;
      font-family: "Avenir Next Cyr Medium";
    }
  }
`

export const PopupContent = styled.div`
  .content-text {
    padding: 0;
    color: #8f9398;
    font-size: 14px;
    font-weight: 400;
    line-height: 20px;
    margin-bottom: 15px;
    font-family: "Avenir Next Cyr";
  }
  
  .btn-wrap {
    display: flex;
    align-items: center;
    justify-content: flex-start;
  }
  
  .btn-action {
    font-size: 14px;
    cursor: pointer;
    font-weight: 400;
    line-height: 20px;
    margin-right: 20px;
    font-family: "Avenir Next Cyr";
  
    &.btn-delete {
      color: #d51006;
    }
    
    &.btn-replace {
      color: #003d7c;
    }
    
    &:last-child {
      margin-right: 0;
    }
  }
`



export const DatePickerRow = styled.div`
  padding: 7px 0;
  max-width: 230px;
  align-items: center;
  border-radius: 34px;
  display: inline-flex;
  justify-content: center;
  background-color: #ffffff;
  
  .MuiFormControl-root {
    width: auto;
    height: auto;
    display: flex;
    flex: 0 0 65%;
    
    &:before {
      display: none;
    }
  }
  
  .MuiInputBase-root {
    padding: 0;
  }
  
  .MuiInputBase-input {
    padding: 0;
    height: auto;
    color: #003d7c;
    font-size: 20px;
    font-weight: 600;
    text-align: center;
    text-transform: capitalize;
  }
  
  @media screen and (max-width: 1060px) {
    top: 0;
    padding: 0;
    width: 100%;
    max-width: none;
    border-radius: 0;
    margin-bottom: 0;
    position: relative;
    background-color: transparent;
    justify-content: space-between;
    
    .MuiFormControl-root {
      background-color: transparent;
    }
    
    .MuiFormControl-root {
      flex: 0 0 auto;
    }
    
    .MuiInputBase-input {
      font-size: 20px;
      text-align: left;
      color: ${({ theme }) => theme.colors.black};
      
      @media screen and (max-width: 1060px) {
        font-size: 16px;
      }
    }
  }
`

export const DateArrow = styled.div`
   padding: 10px;
   display: flex;
   cursor: pointer;
   align-items: center;
   
   &.next {
      transform: rotate(-180deg);
   }
   
   img {
      width: 8px;
      height: 15px;
      min-width: 8px;
   }
   
   @media screen and (max-width: 1060px) {
      top: 50%;
      position: absolute;
      transform: translateY(-50%);
      
      &.prev {
        right: 40px;
      }
      
      &.next {
        right: 0;
        transform: translateY(-50%) rotate(-180deg);
      }
   }
`

export const MonthNameRow = styled.div`
  color: #000c18;
  font-size: 28px;
  font-weight: 700;
  background-color: #fff;
  padding: 40px 40px 15px;
  text-transform: capitalize;
  border-radius: 16px 16px 0 0;
  font-family: Comfortaa, sans-serif;
`

export const FiltersRow = styled.div`
  top: -92px;
  z-index: 5;
  display: flex;
  position: absolute;
  align-items: center;
  left: calc(50% + 40px);
  transform: translateX(-50%);
  justify-content: space-between;
  
  @media screen and (max-width: 1060px) {
    top: 0;
    left: 0;
    transform: none;
    padding: 0 15px;
    position: relative;
    margin-bottom: 15px;
  }
`

export const SelectRow = styled.div`
  margin-right: 15px;
  
   @media screen and (max-width: 1060px) {
    width: 100%;
   }
  
  .MuiFormControl-root {
    width: 210px;
    
    @media screen and (max-width: 1060px) {
      width: 100%;
      height: 36px; 
    }
  }
  
  .MuiInputBase-root {
    border-radius: 34px;
  }
  
  .MuiSelect-root {
    margin: 0;
    color: #003d7c;
    font-size: 20px;
    justify-content: center;
    font-family: "Avenir Next Cyr Medium";
    
    &:focus {
      border-radius: 34px;
    }
    
    @media screen and (max-width: 1060px) {
      font-size: 16px;
    }
  }
`

export const SelectArrow = styled.img`
  top: 50%;
  z-index: 0;
  width: 8px;
  right: 20px;
  height: 15px;
  cursor: pointer;
  position: absolute;
  pointer-events: none;
  transform: translateY(-50%) rotate(-90deg);
`

export const QICLessonName = styled.h2`
  color: #000c18;
  font-size: 18px;
  font-weight: 500;
  padding-right: 25px;
  white-space: pre-wrap;
  font-family: "Avenir Next Cyr Medium";
`

export const QICLessonDescription = styled.p`
  color: #8f9398;
  font-size: 15px;
  white-space: pre-wrap;
  font-family: "Avenir Next Cyr";
`

export const QICCHipsWrap = styled.div`
  display: flex;
  margin: 20px 0;
  flex-direction: row;
  justify-content: flex-start;
`

export const ActionsWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 15px;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
`

export const ActionLink = styled.p`
  color: #003d7c;
  font-size: 14px;
  cursor: pointer;
  line-height: 20px;
  text-align: center;
  margin-right: 20px;
  margin-bottom: 5px;
  font-family: "Avenir Next Cyr";
  
  &.btn-delete {
    color: #d51006;
  }
`
