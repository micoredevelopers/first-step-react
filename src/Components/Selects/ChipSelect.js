import React from 'react'
import styled from 'styled-components'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles(() => ({
  selectPaper: {
    marginTop: 13,
    borderRadius: 6,
    overflow: 'visible',
    backgroundColor: '#fff',
    boxShadow: '0 -5px 12px rgba(0, 12, 24, 0.1)'
  },
  selectList: {
    padding: 0,
    maxHeight: 208,
    overflow: 'auto',

    '& li': {
      '&.Mui-disabled': {
        display: 'none'
      }
    }
  }
}))

const MenuItemS = styled(MenuItem)`
  width: auto;
  display: flex;
  font-size: 14px;
  overflow: visible;
  padding: 15px 20px;
  align-items: center;
  justify-content: center;
  border-top: 1px solid #e6e6e6;
  font-family: 'Comfortaa', sans-serif;
  color: ${({ theme }) => theme.colors.black};
  
  &:hover {
    background-color: transparent !important;
  }
  
  &:nth-child(2) {
    position: relative;
    border-top: none;
    
    &:before {
      width: 0;
      top: -9px;
      left: 50%;
      height: 0;
      z-index: 1;
      content: '';
      position: absolute;
      border-style: solid;
      transform: translateX(-50%);
      border-width: 0 6px 9px 6px;
      border-color: transparent transparent #fff transparent;
    }
  }
  
  &.Mui-selected {
    background-color: transparent;
    color: #bcbfc2;
  }
`

const SelectS = styled(Select)`
  z-index: 5;
  width: 55px;
  height: 30px;
  min-width: 55px;
  cursor: pointer;
  box-shadow: none;
  margin-right: 10px;
  position: relative;
  border-radius: 15px;
  background-color: transparent;
  transition: all 0.4s ease-in-out;
  color: ${props => setStatusColor(props)};
  border: 1px solid ${props => setStatusColor(props)};

  &:before, &:after {
    display: none
  }
  
  &:hover {
    background-color: #fff;
  }
  
  &.no-event {
    cursor: default;
    pointer-events: none;
  }
  
  &.is-absent {
   cursor: default;
   pointer-events: none;
   color: ${({ theme }) => theme.colors.secondary};
   border: 1px solid ${({ theme }) => theme.colors.secondary};
  }
  
  &.Mui-disabled {
    color: ${({ theme }) => theme.colors.disabled};
    border: 1px solid ${({ theme }) => theme.colors.disabled};
  }
  
  .MuiSelect-icon {
    display: none
  }
  
  .MuiSelect-root {
    height: 100%;
    display: flex;
    padding: 0;
    font-size: 16px;
    font-weight: 700;
    line-height: normal;
    align-items: center;
    justify-content: center;
    font-family: 'Comfortaa', sans-serif;
    color: ${({ theme }) => theme.colors.grey};
    
    &:focus {
      border-radius: 15px;
      background-color: transparent
    }
  }
  
  .MuiSelect-select {
    color: inherit;
  }
`

export const setStatusColor = ({ value, theme }) => {
  if (value === '-') {
    return theme.colors.grey
  } else if (typeof value === 'string') {
    switch (value) {
      case '1':
        return theme.colors.error
      case '0':
        return theme.colors.success
      default:
        return theme.colors.grey
    }
  } else {
    if (value < 7) {
      return theme.colors.error
    } else if (value >= 7 && value <= 9) {
      return theme.colors.warning
    } else if (value > 9) {
      return theme.colors.success
    } else {
      return theme.colors.grey
    }
  }
}

export const ChipSelect = ({ id, name, placeholder, value, onChange, options, isAbsent, noevent, format, ...props }) => {
  const classes = useStyles()

  const MenuProps = {
    classes: {
      paper: classes.selectPaper,
      list: classes.selectList
    },
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'center'
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'center'
    },
    getContentAnchorEl: null
  }
  
  return (
    <SelectS
      onChange={onChange}
      MenuProps={MenuProps}
      inputProps={{ name }}
      value={value ? value : placeholder}
      className={isAbsent ? 'is-absent' : noevent ? 'no-event' : ''}
      {...props}
    >
      {placeholder && <MenuItem disabled value={placeholder}>{placeholder}</MenuItem>}
      {options.map(option => (
        <MenuItemS
          disableRipple
          key={option.value}
          theme={props.theme}
          value={option.value}
        >
          {option.name}
        </MenuItemS>
      ))}
    </SelectS>
  )
}
