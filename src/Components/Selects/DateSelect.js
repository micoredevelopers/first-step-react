import 'date-fns'
import React from 'react'
import ru from 'date-fns/locale/ru'
import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import DateFnsUtils from '@date-io/date-fns'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { MuiPickersUtilsProvider, DatePicker } from '@material-ui/pickers'

const DatePickerCustom = styled(DatePicker)`
   width: 100%;
   height: 50px;
   max-width: 220px;
   position: relative;
   border-radius: 6px;
   background-color: #fff;
   
   &:before {
      content: '';
      position: absolute;
      top: 50%;
      right: 15px;
      transform: translateY(-50%);
      width: 0;
      height: 0;
      border-style: solid;
      border-width: 5px 5px 0 5px;
      border-color: ${({ theme }) => theme.colors.lightGrey} transparent transparent transparent;
      z-index: 5;
   }
  
  .MuiInputBase-root {
     padding: 0 20px;
     height: 100%;
     cursor: pointer;
  
     &:before, &:after {
      display: none;
     }
  }
  
  .MuiInputBase-input {
    cursor: pointer;
    font-size: 16px;
    font-family: "Avenir Next Cyr";
    color: ${({ theme, value, error }) => error ? theme.colors.error : value ? theme.colors.black : theme.colors.grey};
   }
   
  ${down('md')} {
    width: 100%;
    max-width: 100%;
    
    .MuiInputBase-input {
      font-size: 15px;
    }
  }
`

const useStyles = makeStyles(() => ({
  root: {
    '& p, & span': {
      fontFamily: "Avenir Next Cyr",
      fontSize: 14
    },
    '& .MuiPickersCalendarHeader-transitionContainer p': {
      color: '#000',
      fontSize: 20,
      fontWeight: 700,
      fontFamily: 'Comfortaa',
      textTransform: 'capitalize'
    },
    '& .MuiPickersCalendarHeader-dayLabel': {
      color: '#8f9398',
      fontSize: 12,
      textTransform: 'capitalize'
    }
  },
  paperRoot: {
    boxShadow: '0 0 12px rgba(0, 12, 24, 0.1)'
  }
}))

export const DateSelect = ({ name, placeholder, value, format = "dd MMM", error, onChange }) => {
  const classes = useStyles()

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ru}>
      <DatePickerCustom
        autoOk
        id={name}
        error={error}
        value={value}
        disableToolbar
        format={format}
        variant="inline"
        onChange={onChange}
        emptyLabel={placeholder}
        invalidDateMessage="Неправильный формат даты"
        PopoverProps={{ classes: { paper: classes.paperRoot, root: classes.root } }}
      />
    </MuiPickersUtilsProvider>
  )
}
