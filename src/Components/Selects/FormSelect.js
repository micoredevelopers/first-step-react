import React  from 'react'
import clsx from 'clsx'
import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles(theme => ({
  root: {
    height: 50,
    width: '100%',
    maxWidth: 220,
    marginBottom: 0,
    [theme.breakpoints.down('md')]: {
      height: 46,
      maxWidth: '100%'
    }
  },
  selectList: {
    padding: 0,
    '& li': {
      '&.Mui-disabled': {
        display: 'none'
      }
    }
  },
  selectPaper: {
    maxWidth: '220px',
    maxHeight: '250px',
    borderRadius: '0 0 6px 6px',
    boxShadow: '0 5px 10px rgba(0, 12, 24, 0.1)'
  }
}))

const SelectItem = styled(MenuItem)`
  color: #000c18;
  font-size: 16px;
  padding: 15px 20px 14px;
  border-top: 1px solid #e6e6e6;
  font-family: "Avenir Next Cyr";
  
  &:hover {
    background-color: transparent !important;
  }
  
  &.Mui-selected {
    background-color: transparent;
    color: #bcbfc2;
  }
  
  ${down('md')} {
    font-size: 15px;
  }
`

const SelectS = styled(Select)`
  height: 100%;
  box-shadow: none;
  border-radius: 6px;
  background-color: #fff;
  
  &:before, &:after {
    display: none
  }
  
  &:hover {
    background-color: #fff;
  }
  
  &.Mui-focused {
    background-color: #fff;
    border-radius: 6px 6px 0 0;
  }
  
  &.Mui-disabled {
    background-color: rgba(0, 0, 0, 0.1);
    
    &:hover {
      background-color: rgba(0, 0, 0, 0.1);
    }
  }
  
  .MuiSelect-root {
    height: 100%;
    display: flex;
    align-items: center;
    padding: 0 30px 0 20px;
    
    &:focus {
      border-radius: 6px;
      background-color: #fff
    }
    
    span {
      width: 100%;
      font-size: 16px;
      overflow: hidden;
      white-space: nowrap;
      display: inline-block;
      text-overflow: ellipsis;
      font-family: "Avenir Next Cyr";
      color: ${({ theme, value, inputProps, error }) => {
        return error ? theme.colors.error : value === inputProps.placeholder || value.length === 0 ? theme.colors.grey : theme.colors.black
      }};
      
      ${down('md')} {
        font-size: 15px;
      }
    }
  }
  
  .MuiSelect-icon {
    color: #bcbfc2
  }
`

export const FormSelect = (props) => {
  const {
    id,
    name,
    label,
    icon,
    value,
    style,
    error,
    options,
    onChange,
    itemName,
    multiple,
    disabled,
    className,
    itemValue,
    clearValue,
    placeholder
  } = props
  const classes = useStyles()
  const MenuProps = {
    classes: {
      paper: classes.selectPaper,
      list: classes.selectList
    },
    anchorOrigin: {
      vertical: 'bottom',
      horizontal: 'center',
    },
    transformOrigin: {
      vertical: 'top',
      horizontal: 'center',
    },
    getContentAnchorEl: null
  }
  
  return (
    <FormControl variant="filled" className={clsx(classes.root, className)} style={style}>
      {label && <InputLabel htmlFor={id}>{label}</InputLabel>}
      <SelectS
        error={error}
        multiple={multiple}
        disabled={disabled}
        onChange={onChange}
        IconComponent={icon}
        MenuProps={MenuProps}
        displayEmpty={multiple}
        renderValue={(selected) => {
          const displayValue = options.filter(o => (o[itemValue] || o.value || o.id) === selected)[0]
  
          if (multiple) {
            const filteredSelected = options.filter(o => selected.includes(o.value))
            
            if (selected.length === 0) {
              return <span>{placeholder}</span>
            }
            
            return <span>{filteredSelected.map(o => o.name).join(', ')}</span>
          } else {
            return (
              <span>
                {displayValue
                  ? displayValue.format
                    ? displayValue.format(displayValue)
                    : (displayValue[itemName] || displayValue.name)
                  : selected}
              </span>
            )
          }
        }}
        value={value ? value : placeholder}
        inputProps={{ name, id, placeholder }}
      >
        {placeholder && <MenuItem disabled value={placeholder}>{placeholder}</MenuItem>}
        {!multiple && <SelectItem disableRipple value={clearValue ? clearValue : ''}>Сбросить</SelectItem>}
        {options.map((option, index) => (
          <SelectItem
            key={index}
            disableRipple
            value={option[itemValue] || option.value || option.id}
          >
            {option.format ? option.format(option) : (option[itemName] || option.name)}
          </SelectItem>
        ))}
      </SelectS>
    </FormControl>
  )
}
