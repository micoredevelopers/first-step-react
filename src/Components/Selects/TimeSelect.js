import React from 'react'
import 'date-fns'
import ru from 'date-fns/locale/ru'
import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import DateFnsUtils from '@date-io/date-fns'
import { MuiPickersUtilsProvider, KeyboardTimePicker } from '@material-ui/pickers'

const TimePickerCustom = styled(KeyboardTimePicker)`
   width: 100%;
   height: 50px;
   max-width: 220px;
   position: relative;
   border-radius: 6px;
   background-color: #fff;
  
  .MuiInputBase-root {
     height: 100%;
     cursor: text;
     padding: 0 0 0 20px;
  
     &:before, &:after {
      display: none;
     }
  }
  
  .MuiInputBase-input {
    font-size: 16px;
    font-family: "Avenir Next Cyr";
    color: ${({ theme, value, error }) => error ? theme.colors.error : value ? theme.colors.black : theme.colors.grey};
    
    &::placeholder {
      opacity: 1;
    }
   }
   
  ${down('md')} {
    width: 100%;
    max-width: 100%;
    
    .MuiInputBase-input {
      font-size: 15px;
    }
  }
`

export const TimeSelect = ({ name, value, placeholder, onChange, error }) => {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ru}>
      <TimePickerCustom
        autoOk
        id={name}
        ampm={false}
        mask="__:__"
        error={error}
        value={value}
        variant="inline"
        onChange={onChange}
        invalidDateMessage=""
        placeholder={placeholder}
      />
    </MuiPickersUtilsProvider>
  )
}
