import React from 'react'
import Slider from 'react-slick'
import styled, {css} from 'styled-components'
import { down } from 'styled-breakpoints'
import { apiUrl } from '../../Api'

const SliderWrap = styled.div`
  position: relative;
  width: 100%;
`

const CustomSlider = styled(Slider)`
  width: 100%;
  padding: 0 60px;
  .slick-slide {
    &>div {
      display: flex;
      justify-content: center;
    }
  }
  
  .slick-dots {
    display: flex;
    justify-content: center;
    align-items: center;
    bottom: -90px;
    
    li {
      width: 8px;
      height: 8px;
      margin: 0 5px;
      
      &.slick-active {
        button {
          &:before {
            opacity: 1;
            color: ${({ theme }) => theme.colors.main}
          }
        }
      }
      
      button {
        padding: 0;
        width: 100%;
        height: 100%;
        
        &:before {
          width: 8px;
          height: 8px;
          font-size: 8px;
          line-height: normal;
          opacity: 1;
          color: ${({ theme }) => theme.colors.lightGrey}
        }
      }
    }
  }
  
  ${down('md')} {
    padding: 0 50px;
    .slick-dots {
      bottom: -75px;
    }
  }
`

const SlideWrap = styled.div`
  width: 100%;
  height: 420px;
  margin: 0 auto 30px;
  overflow: auto;

  img {
     width: 100%;
     height: 100%;
     object-fit: contain;
  }
  
  ${down('md')} {
    height: 315px;
    margin: 0 auto 30px;
  }
`

const ArrowWrap = styled.div`
  width: auto;
  height: auto;
  z-index: 5;

  &:before {
    display: none;
  }
  
  &.slick-prev {
    left: 50px;
  }
  
  &.slick-next {
    right: 50px;
  }
  
  img {
    width: 17px;
    height: 32px;
  }
  
  ${down('md')} {
    &.slick-prev {
      left: 0;
    }
    
    &.slick-next {
      right: 0;
    }
    
    img {
      width: 13px;
      height: 25px;
    }
  }
`

const SliderImage = styled.img`
  transition: .2s all linear;
  ${({scale, rotate})=> (scale || rotate) && (
      css`
        transform: ${scale ? `scale(${scale})` : ''} ${rotate ? `rotate(${90 * rotate}deg)` : ''};
      `
  )}
`

const ArrowPrev = ({ className, onClick }) => (
  <ArrowWrap className={className} onClick={onClick}>
    <img src="/assets/images/ArrowPrev.svg" alt="Arrow prev"/>
  </ArrowWrap>
)

const ArrowNext = ({ className, onClick }) => (
  <ArrowWrap className={className} onClick={onClick}>
    <img src="/assets/images/ArrowNext.svg" alt="Arrow next"/>
  </ArrowWrap>
)

export const ModalSlider = ({ settings, slides, rotate, scale }) => {
  return (
    <SliderWrap>
      <CustomSlider {...{ prevArrow: <ArrowPrev/>, nextArrow: <ArrowNext/>, ...settings }}>
        {(slides && slides.length > 0) ? slides.map((slide, index) => (
          <SlideWrap key={index}>
            <SliderImage scale={scale} rotate={rotate} src={`${apiUrl}/${slide.path}`} alt="Homework"/>
          </SlideWrap>
        )) : null}
      </CustomSlider>
    </SliderWrap>
  )
}
