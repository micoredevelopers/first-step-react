import React, { useState, useEffect } from 'react'
import Slider from 'react-slick'
import styled from 'styled-components'
import Typography from '@material-ui/core/Typography'

export const SliderWrap = styled.div`
  width: 320px;
  padding: 20px;
  position: absolute;
  background-color: #ffffff;
  top: ${({ position }) => position.top}px;
  left: ${({ position }) => position.left}px;
  box-shadow: ${({ placement }) => {
    switch (placement) {
      case 'top':
        return '0 3px'
      case 'left':
        return '-3px 0'
      default: return '0 -3px'
    }
  }} 20px rgba(0, 12, 24, 0.1);
  
  
  &:after {
    width: 0;
    left: 50%;
    height: 0;
    content: '';
    position: absolute;
    border-style: solid;
    transform: ${({ placement }) => {
      switch (placement) {
        case 'left':
        case 'right':
          return 'translateY(-50%)'
        default: return 'translateX(-50%)'
      }
    }};
    top: ${({ placement }) => {
      switch (placement) {
        case 'bottom':
          return '-9px'
        case 'right':
        case 'left':
          return '50%'
        default: return 'auto'
      }
    }};
    left: ${({ placement }) => {
      switch (placement) {
        case 'left':
          return 'auto'
        default: return '50%'
      }
    }};
    right: ${({ placement }) => {
      switch (placement) {
        case 'left':
          return '-9px'
        default: return 'auto'
      }
    }};
    bottom: ${({ placement }) => {
      switch (placement) {
        case 'top':
          return '-9px'
        default: return 'auto'
      }
    }};
    border-width: ${({ placement }) => {
      switch (placement) {
        case 'top':
          return '9px 7px 0 7px'
        case 'left':
          return '7px 0 7px 9px'
        default: return '0px 7px 9px 7px'
      }
    }};
    border-color: ${({ placement }) => {
      switch (placement) {
        case 'top':
          return '#fff transparent transparent transparent'
        case 'left':
          return 'transparent transparent transparent #fff'
        default:
          return 'transparent transparent #fff transparent'
      }
    }};
  }
`

export const CustomSlider = styled(Slider)`
  .slick-track {
    transition: none !important;
  }
  
  .slick-slide {
    outline: none;
  }

  .slick-dots {
    right: 0;
    bottom: 5px;
    width: auto;
    align-items: center;
    justify-content: center;
    display: flex !important;
    
    li {
      width: 8px;
      height: 8px;
      margin: 0 5px;
      min-width: 8px;
      
      &.slick-active {
        button {
          &:before {
            opacity: 1;
            color: ${({ theme }) => theme.colors.main}
          }
        }
      }
      
      button {
        padding: 0;
        width: 100%;
        height: 100%;
        
        &:before {
          width: 8px;
          height: 8px;
          font-size: 8px;
          line-height: normal;
          opacity: 1;
          color: ${({ theme }) => theme.colors.lightGrey}
        }
      }
    }
  }
`

export const SlideWrap = styled.div`
  outline: none;
`

export const Arrow = styled.div`
  top: auto;
  bottom: 0;
  z-index: 5;
  width: auto;
  height: auto;
  display: flex;
  cursor: pointer;
  transform: none;
  align-items: center;

  &:before {
    display: none;
  }
  
  &.slick-prev {
    left: 0;
    
    span {
      margin-left: 10px;
    }
  }
  
  &.slick-next {
    left: 75px;
    right: auto;
    
    span {
      margin-right: 10px;
    }
  }
  
  &.slick-disabled {
    span {
      color: ${({ theme }) => theme.colors.grey};
    }
  }
  
  &.is-last {
    left: 65px;
    cursor: pointer;
    pointer-events: all;
  
    span {
      margin-right: 5px;
      color: ${({ theme }) => theme.colors.main};
    }
  }
  
  img {
    width: 6px;
    height: 12px;
  }
  
  span {
    font-size: 14px;
    line-height: 20px;
    font-family: "Avenir Next Cyr";
    color: ${({ theme }) => theme.colors.main};
  }
`

export const SlideTitle = styled(Typography)`
  font-size: 16px;
  margin-bottom: 10px;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
`

export const SlideDesc = styled(Typography)`
  font-size: 14px;
  line-height: 20px;
  margin-bottom: 35px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
`

const ArrowPrev = ({ className, onClick }) => (
  <Arrow className={className} onClick={onClick}>
    <img src="/assets/images/ArrowPrev.svg" alt="Arrow prev"/>
    <span>Назад</span>
  </Arrow>
)

const ArrowNext = ({ className, onClick, isLast, endStudy }) => {
  return (
    <Arrow className={`${isLast ? 'is-last' : ''} ${className}`} onClick={isLast ? endStudy : onClick}>
      {isLast ? (
        <span>Завершить</span>
      ) : (
        <span>Далее</span>
      )}
      <img src="/assets/images/ArrowNext.svg" alt="Arrow next"/>
    </Arrow>
  )
}

export const StudyingSlider = ({ studyingSteps, handleEndSteps }) => {
  const [index, setIndex] = useState(0)
  const [height, setHeight] = useState(0)
  const [pos, setPos] = useState({ top: 0, left: 0 })
  
  const SliderSettings = {
    dots: true,
    speed: 100,
    infinite: false,
    slidesToShow: 1,
    draggable: false,
    slidesToScroll: 1,
    beforeChange: (currentIndex, nextIndex) => {
      setIndex(nextIndex)
    }
  }
  
  useEffect(() => {
    const slide = document.getElementsByClassName('slick-slide')[index]
    const slideH = slide.offsetHeight
    
    const step = studyingSteps[index]
    const el = step.id ? document.getElementById(step.id) : document.getElementsByClassName(step.className)[0]
    const elW = el.offsetWidth
    const elH = el.offsetHeight
    
    setHeight(slideH)
  
    if (step.position === 'bottom') {
      const topPos = el.getBoundingClientRect().top + elH + step.offset
      const leftPos = el.getBoundingClientRect().left + (elW * 0.5) - 160
      
      setPos({ top: topPos, left: leftPos })
    } else if (step.position === 'top') {
      const topPos = el.getBoundingClientRect().top - (slideH + 40) - step.offset
      const leftPos = el.getBoundingClientRect().left + (elW * 0.5) - 160
  
      setPos({ top: topPos, left: leftPos })
    } else if (step.position === 'left') {
      const topPos = el.getBoundingClientRect().top + (elH * 0.5) - ((slideH + 40) / 2)
      const leftPos = el.getBoundingClientRect().left - 320 - step.offset
  
      setPos({ top: topPos, left: leftPos })
    }
  }, [index, studyingSteps])
  
  return (
    <SliderWrap position={pos} placement={studyingSteps[index].position}>
      <CustomSlider
        style={{ height }}
        {...{
          prevArrow: <ArrowPrev/>,
          nextArrow: <ArrowNext isLast={index === studyingSteps.length - 1} endStudy={handleEndSteps}/>,
          ...SliderSettings
        }}
      >
        {studyingSteps.map((step, index) => (
          <SlideWrap key={index}>
            <SlideTitle variant="h2" children={step.title}/>
            <SlideDesc children={step.desc}/>
          </SlideWrap>
        ))}
      </CustomSlider>
    </SliderWrap>
  )
}
