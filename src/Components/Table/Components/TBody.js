import React from 'react'
import TableRow from '@material-ui/core/TableRow'
import TableBody from '@material-ui/core/TableBody'
import { MenuButton } from '../../Buttons'
import { TBodyCell, MenuS, MenuIcon } from '../Styles'

export const TBody = ({ tableData, tableHeader, tableActions }) => {
  return (
    <TableBody>
      {tableData.length > 0 && tableData.map((row, index) => (
        <TableRow key={index} hover={false} tabIndex={-1}>
          {tableHeader.map((column) => {
            const value = row[column.id]
      
            return (
              <TBodyCell key={column.id} align={column.align}>
                {column.format ? column.format(row) : value}
              </TBodyCell>
            )
          })}
          <TBodyCell key={index} align="center">
            <MenuButton
              id="table_menu"
              eventData={row}
              customStyles={MenuS}
              placement="bottom-end"
              linksList={tableActions}
              style={{ display: 'inline-block', padding: '0 10px' }}
            >
              <MenuIcon src="/assets/images/MenuIcon.svg" alt="Menu action"/>
            </MenuButton>
          </TBodyCell>
        </TableRow>
      ))}
    </TableBody>
  )
}

