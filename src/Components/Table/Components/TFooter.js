import React from 'react'
import { PaginationRow, PaginationCount, PaginationActions, ArrowWrap, ArrowPrev, ArrowNext } from '../Styles'

export const TFooter = ({ update, pagination }) => {
  const goToPrevPage = () => {
    if (pagination.page > 1) {
      update({ page: pagination.page - 1 })
    }
  }

  const goToNextPage = () => {
    if (pagination.page !== pagination.maxPages) {
      update({ page: pagination.page + 1 })
    }
  }
  
  return (
    <PaginationRow>
      <PaginationCount>
        {pagination.page} из {pagination.maxPages === 0 ? '1' : pagination.maxPages}
      </PaginationCount>
      <PaginationActions>
        {pagination.page > 1 && (
          <ArrowWrap onClick={goToPrevPage}>
            <ArrowPrev src="/assets/images/ArrowLeftMain.svg" alt="Prev"/>
          </ArrowWrap>
        )}
        {pagination.maxPages > pagination.page && (
          <ArrowWrap onClick={goToNextPage}>
            <ArrowNext src="/assets/images/ArrowLeftMain.svg" alt="Next"/>
          </ArrowWrap>
        )}
      </PaginationActions>
    </PaginationRow>
  )
}
