import React, { useState } from 'react'
import TableRow from '@material-ui/core/TableRow'
import TableHead from '@material-ui/core/TableHead'
import { FormInput } from '../../Inputs'
import { tHeadStyles, THeadCell } from '../Styles'
import { FormSelect } from '../../Selects'

export const THead = ({ update, tableHeader }) => {
  const classes = tHeadStyles()
  const [searchVal, setSearchVal] = useState(null)
  
  const handleSearchBlur = (e) => {
    if (e.key === 'Enter') {
      update({ page: 1, ...searchVal })
    }
  }
  const handleSearchChange = (e) => {
    const { name, value } = e.target
    
    setSearchVal(prev => ({ ...prev, [name]: value }))
  }
  const handleSelectChange = ({ filterEvent, clearId }) => (e) => {
    const { name, value } = e.target
    
    if (value === '') {
      update({ page: 1, [name]: value, [clearId]: '' })
      setSearchVal(prev => ({ ...prev, [name]: value, [clearId]: '' }))
    } else {
      update({ page: 1, [name]: value })
      setSearchVal(prev => ({ ...prev, [name]: value }))
    }
    
    filterEvent && filterEvent({ [name]: value })
  }
  
  return (
    <TableHead>
      <TableRow>
        {tableHeader.map((column) => (
          <THeadCell
            key={column.id}
            align={column.align}
            style={{ minWidth: column.minWidth, maxWidth: column.maxWidth }}
          >
            {(column.filter && column.filter.type === 'search') ? (
              <FormInput
                name={column.filter.id}
                placeholder={column.label}
                onKeyPress={handleSearchBlur}
                onChange={handleSearchChange}
                className={classes.THSearchInput}
                icon="/assets/images/SearchIcon.svg"
                value={(searchVal && searchVal[column.filter.id]) ?? ''}
              />
            ) : (column.filter && column.filter.type === 'select') ? (
              <FormSelect
                name={column.filter.id}
                placeholder={column.label}
                className={classes.THSelect}
                options={column.filter.options}
                itemValue={column.filter.itemValue}
                value={(searchVal && searchVal[column.filter.id]) ?? ''}
                onChange={handleSelectChange({ filterEvent: column.filter.filterEvent, clearId: column.filter.withClear })}
              />
            ) : `${column.label}`}
          </THeadCell>
        ))}
        <THeadCell key="table_action" align="center">
          Действие
        </THeadCell>
      </TableRow>
    </TableHead>
  )
}
