import React from 'react'
import { MenuButton } from '../../Buttons'
import { MenuIcon, MenuS } from '../Styles'
import { useLocation } from 'react-router-dom'
import { TCWrap, TCInfoRow, TCNameRow, TCSubNameRow, StatusIcon, TCTitle, TCSubTitle, TCGroups } from '../Styles/TCardStyled'

export const TCard = ({ data, actions }) => {
  const Location = useLocation()
  
  return data.length > 0 && data.map((row, index) => {
    const getStatusIcon = () => {
      if (Location.pathname.includes('payment')) {
        if (row) {
          switch (row.status) {
            case 'new':
              if (row.receiptFile) {
                return <img src="/assets/images/PaymentWait.svg" alt="Wait"/>
              } else {
                return <img src="/assets/images/DisabledIcon.svg" alt="Disabled"/>
              }
            case 'paid':
              return <img src="/assets/images/EnableIcon.svg" alt="Enable"/>
            default: return <img src="/assets/images/DisabledIcon.svg" alt="Disabled"/>
          }
        } else {
          return <img src="/assets/images/DisabledIcon.svg" alt="Disabled"/>
        }
      }
    }

    return (
      (
        <TCWrap key={index}>
          <TCInfoRow>
            <TCNameRow>
              <StatusIcon
                alt={getStatusIcon() ? row.status : row.enable ? 'Enable' : 'Disable'}
                src={getStatusIcon() ? getStatusIcon().props.src : `/assets/images/${row.enable ? 'Enable' : 'Disabled'}Icon.svg`}
              />
              <TCTitle children={row.fullName || row.student.fullName}/>
            </TCNameRow>
        
            <MenuButton
              id="card_menu"
              eventData={row}
              linksList={actions}
              customStyles={MenuS}
              placement="bottom-end"
              style={{ display: 'inline-block' }}
            >
              <MenuIcon src="/assets/images/MenuIcon.svg" alt="Menu action"/>
            </MenuButton>
          </TCInfoRow>
          <TCInfoRow style={{ alignItems: 'flex-start' }}>
            <TCSubNameRow>
              <TCSubTitle children={row.email || row.student.email}/>
            </TCSubNameRow>
        
            <TCGroups>
              {row.studentGroups ? row.studentGroups.map(group => group.name).join(', ') : row.group ? row.group.name : ''}
            </TCGroups>
          </TCInfoRow>
        </TCWrap>
      )
    )
  })
}


