import styled from 'styled-components'
import TableCell from '@material-ui/core/TableCell'
import { styled as MuiStyled } from '@material-ui/core/styles'

export const TBodyCell = MuiStyled(TableCell)(({ theme }) => ({
  padding: 22,
  fontSize: 16,
  lineHeight: '22px',
  borderBottomColor: '#e6e6e6',
  fontFamily: "Avenir Next Cyr",
  color: theme.palette.text.primary,
  
  '&:first-child': {
    paddingLeft: '40px'
  },
  '&:last-child': {
    paddingRight: '40px'
  }
}))

export const MenuS = {
  menuList: {
    maxHeight: 'none'
  },
  menuItemText: {
    textAlign: 'left'
  }
}

export const MenuIcon = styled.img`
  width: 20px;
  height: 13px;
`
