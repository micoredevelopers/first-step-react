import styled from 'styled-components'

export const TCWrap = styled.div`
  padding: 15px;
  border-radius: 16px;
  margin-bottom: 10px;
  background-color: #ffffff;
`

export const TCInfoRow = styled.div`
  display: flex;
  margin-bottom: 6px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  
  &:last-child {
    margin-bottom: 0;
  }
`

export const TCNameRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-right: 15px;
`

export const TCSubNameRow = styled.div`
  display: flex;
  flex-direction: column;
`

export const StatusIcon = styled.img`
  width: 15px;
  height: auto;
  margin-right: 10px;
`

export const TCTitle = styled.h2`
  font-size: 18px;
  font-weight: 500;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
`

export const TCSubTitle = styled.div`
  font-size: 15px;
  margin-bottom: 7px;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.grey};
  
  &:last-child {
    margin-bottom: 0;
  }
`

export const TCGroups = styled.p`
  font-size: 15px;
  max-width: 150px;
  text-align: right;
  padding-left: 15px;
  word-break: break-word;
  font-family: "Avenir Next Cyr";
  color: ${({ theme }) => theme.colors.black};
`
