import styled from 'styled-components'
import { down } from 'styled-breakpoints'

export const PaginationRow = styled.div`
  display: flex;
  padding: 10px 0;
  align-items: center;
  flex-direction: row;
  justify-content: flex-end;
  
  ${down('lg')} {
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 995;
    position: fixed;
    justify-content: center;
    background-color: #efeff0;
    border-top: 1px solid #e6e6e6;
  }
`

export const PaginationCount = styled.div`
  color: #9e9e9e;
  font-size: 16px;
  margin-right: 7px;
  font-family: "Avenir Next Cyr";
`

export const PaginationActions = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
`

export const ArrowWrap = styled.div`
  padding: 20px;
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: center;
  
  ${down('lg')} {
    padding: 7px;
  }
`

export const ArrowPrev = styled.img`
  width: 11px;
  height: 20px;
`

export const ArrowNext = styled.img`
  width: 11px;
  height: 20px;
  transform: rotate(180deg);
`
