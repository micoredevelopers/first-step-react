import makeStyles from '@material-ui/core/styles/makeStyles'
import { styled } from '@material-ui/core/styles'
import TableCell from '@material-ui/core/TableCell'

export const tHeadStyles = makeStyles({
  THSearchInput: {
    padding: 0,
    height: '100%',
    display: 'flex',
    marginBottom: 0,
    
    '& img': {
      left: 0
    },
    '& .MuiInputBase-root': {
      padding: 0,
      fontSize: 16,
      height: 'auto',
      lineHeight: '22px',
    },
    '& input': {
      padding: '0'
    }
  },
  THSelect: {
    height: 23,

    '& .MuiSelect-root': {
      paddingLeft: '0px !important'
    }
  }
})

export const THeadCell = styled(TableCell)(({ theme }) => ({
  padding: 22,
  fontSize: 16,
  lineHeight: '22px',
  backgroundColor: '#fff',
  borderBottomColor: '#e6e6e6',
  fontFamily: "Avenir Next Cyr",
  color: theme.palette.text.secondary,
  
  '&:first-child': {
    paddingLeft: '40px'
  },
  '&:last-child': {
    paddingRight: '40px'
  }
}))
