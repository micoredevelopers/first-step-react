import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import makeStyles from '@material-ui/core/styles/makeStyles'

export const tableStyles = makeStyles({
  root: {
    width: '100%'
  },
  container: {
    height: '100%',
    borderRadius: 16
  }
})

export const TableWrap = styled.div`
  width: 100%;
  border-radius: 16px;
  background-color: #fff;
  height: calc(100% - 84px);
  
  ${down('lg')} {
    height: auto;
    border-radius: 0;
    background-color: transparent;
  }
`
