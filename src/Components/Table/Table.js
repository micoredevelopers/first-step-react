import React from 'react'
import Table from '@material-ui/core/Table'
import TableContainer from '@material-ui/core/TableContainer'
import { tableStyles, TableWrap } from './Styles'
import { DesktopView, MobileView } from '../../Helpers'
import { TBody, TFooter, THead, TCard } from './Components'

export const TableC = ({ updateData, tableData, tableHeader, tableActions, pagination }) => {
  const classes = tableStyles()
  
  return (
    <TableWrap>
      <DesktopView>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <THead tableHeader={tableHeader} update={updateData}/>
            <TBody tableData={tableData} tableActions={tableActions} tableHeader={tableHeader}/>
          </Table>
        </TableContainer>
      </DesktopView>
      <MobileView>
        <TCard
          data={tableData}
          update={updateData}
          actions={tableActions}
        />
      </MobileView>
      <TFooter update={updateData} pagination={pagination}/>
    </TableWrap>
  )
}
