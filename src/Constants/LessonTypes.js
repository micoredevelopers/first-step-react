export const LessonTypes = {
  default: 'TYPE_DEFAULT',
  exam: 'TYPE_EXAM',
  test: 'TYPE_TEST',
  working: 'TYPE_WORKING_OUT'
}

export const TypeOptions = [
  { name: 'Обычный', shortName: 'Об', value: LessonTypes.default },
  { name: 'Экзамен', shortName: 'Э', value: LessonTypes.exam },
  { name: 'Контрольная', shortName: 'К', value: LessonTypes.test },
  { name: 'Отработка', shortName: 'От', value: LessonTypes.working }
]
