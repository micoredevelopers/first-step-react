export const MonthList = [
  { id: `${new Date().getFullYear()}-01`, name: 'Январь' },
  { id: `${new Date().getFullYear()}-02`, name: 'Февраль' },
  { id: `${new Date().getFullYear()}-03`, name: 'Март' },
  { id: `${new Date().getFullYear()}-04`, name: 'Апрель' },
  { id: `${new Date().getFullYear()}-05`, name: 'Май' },
  { id: `${new Date().getFullYear()}-06`, name: 'Июнь' },
  { id: `${new Date().getFullYear()}-07`, name: 'Июль' },
  { id: `${new Date().getFullYear()}-08`, name: 'Август' },
  { id: `${new Date().getFullYear()}-09`, name: 'Сентябрь' },
  { id: `${new Date().getFullYear()}-10`, name: 'Октябрь' },
  { id: `${new Date().getFullYear()}-11`, name: 'Ноябрь' },
  { id: `${new Date().getFullYear()}-12`, name: 'Декабрь' },
]
