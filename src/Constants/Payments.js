export const STATUSES = {
  EXPIRED: 'expired',
  LESS_10: 'less10',
  LESS_5: 'less5',
  LESS_2: 'less2'
}
