import React from 'react'
import { CustomView } from 'react-device-detect'
import { checkDevice } from './checkDevice'

export const DesktopView = (props) => {
  return (
    <CustomView condition={checkDevice('xl')} style={props.style} renderWithFragment>
      {props.children}
    </CustomView>
  )
}

