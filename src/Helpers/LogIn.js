import { session } from 'store2'

export const LogIn = (apiData) => {
  session.set('user', apiData)
  session.set('account', apiData?.type)
  session.set('token', apiData.token ? apiData.token : 'admin')
  window.location.reload()
}
