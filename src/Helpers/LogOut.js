import { session } from 'store2'

export const LogOut = () => {
  session.clearAll()
  window.location.reload()
}
