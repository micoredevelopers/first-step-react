import React from 'react'
import { CustomView } from 'react-device-detect'
import { checkDevice } from './checkDevice'

export const MobileView = (props) => {
  return (
    <CustomView condition={checkDevice('min-lg')} renderWithFragment>
      {props.children}
    </CustomView>
  )
}

