export const checkDevice = (device) => {
  switch (device) {
    case 'xs':
      return window.innerWidth < 400
    case 'sm':
      return window.innerWidth > 401 && window.innerWidth < 767
    case 'min-sm':
      return window.innerWidth < 576
    case 'md':
      return window.innerWidth > 768 && window.innerWidth < 991
    case 'lg':
      return window.innerWidth > 992 && window.innerWidth < 1199
    case 'min-lg':
      return window.innerWidth < 1040
    case 'xl':
      return window.innerWidth > 1200
  }
}
