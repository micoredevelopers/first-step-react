const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

const validatePhone = (phone) => {
  const reEmpty = /^[+]*[3][8][0][\s][(]?[_]{2}[)][\s]?[_]{3}[-]?[_]{2}[-][_]{2}$/g
  const re = /^[+]*[3][8][0][\s][(]?[0-9]{2}[)][\s]?[0-9]{3}[-]?[0-9]{2}[-][0-9]{2}$/g
  return reEmpty.test(phone) || re.test(phone)
}

export const checkValidation = ({ validTypes, name, value, placeholder }) => {
  let errors = {}
  
  validTypes && validTypes.forEach(type => {
    switch (type) {
      case 'isEmpty':
        if (value === '' || value === null || value?.length === 0 || !value) {
          errors[name] = `Поле ${placeholder.split('*')[0]} не должно быть пустым!`
        } else {
          errors[name] = ''
        }
        break
      case 'isEmail':
        if (!validateEmail(value)) {
          errors[name] = `Поле ${placeholder.split('*')[0]} заполнено не верно!`
        } else {
          errors[name] = ''
        }
        break
      case 'isPass':
        if (value.length < 6) {
          errors[name] = `Поле ${placeholder.split('*')[0]} должно содержать минимум 6 символов!`
        } else {
          errors[name] = ''
        }
        break
      case 'isPhone':
        if (value !== '' && !validatePhone(value)) {
          errors[name] = `Поле ${placeholder.split('*')[0]} заполнено не верно!`
        } else {
          errors[name] = ''
        }
        break
      default: return false
    }
  })
  
  return errors
}
