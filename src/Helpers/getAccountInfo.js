import { session } from 'store2'

export const getUser = () => session.get('user')

export const getAccountType = () => session.get('account')

export const getToken = () => session.get('token')
