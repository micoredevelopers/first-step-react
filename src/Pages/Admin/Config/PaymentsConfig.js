import React from 'react'
import { checkDevice } from '../../../Helpers'

export const PaymentsS = {
  sectionR: {
    paddingBottom: checkDevice('min-lg') ? 65 : 0
  },
  titleRow: {
    display: 'flex',
    alignItems: 'stretch',
    flexDirection: checkDevice('min-lg') ? 'column' : 'row'
  },
  titleRightWrap: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
}

export const THeader = [
  {
    id: 'name',
    label: 'ФИО ученика',
    format: row => row.student.fullName,
    filter: { id: 'studentFullName', type: 'search' }
  },
  {
    id: 'phone',
    minWidth: 205,
    label: 'Номер телефона',
    format: row => row.student.phone,
    filter: { id: 'studentPhone', type: 'search' }
  },
  {
    id: 'teacher',
    label: 'Учитель',
    format: row => row.student.teacher?.fullName,
    filter: { id: 'teacherFullName', type: 'search' }
  },
  {
    id: 'group',
    maxWidth: 150,
    label: 'Группа',
    align: 'center',
    format: row => row.student.group?.name,
    filter: { id: 'studentGroupName', type: 'search' }
  },
  {
    minWidth: 165,
    align: 'center',
    id: 'currentPayment',
    label: 'Статус оплаты',
    format: row => {
      switch (row.status) {
        case 'new':
          if (row.receiptFile) {
            return <img src="/assets/images/PaymentWait.svg" alt="Wait"/>
          } else {
            return <img src="/assets/images/DisabledIcon.svg" alt="Disabled"/>
          }
        case 'paid':
          return <img src="/assets/images/EnableIcon.svg" alt="Enable"/>
        case 'canceled':
          console.log('can');
          return <img src="/assets/images/DisabledRedIcon.svg" alt="Canceled"/>
        default: return <img src="/assets/images/DisabledIcon.svg" alt="Disabled"/>
      }
    }
  }
]

export const TActions = ({ setSelectedStudent, setCheckStatus, deleteStudent, openCheckModal }) => [
  {
    id: 'goTo',
    isLink: true,
    path: '/admin/students/',
    title: 'Перейти в профиль',
    event: (row) => {
      setSelectedStudent(row.student)
    }
  },
  {
    id: 'check',
    title: 'Последний чек',
    event: (row) => {
      openCheckModal(row)
    }
  },
  {
    id: 'disable',
    event: (row) => {
      setCheckStatus(row, !row.student.enable)
    },
    format: (row) => row.student.enable ? 'Отключить аккаунт' : 'Включить аккаунт'
  },
  {
    id: 'delete',
    title: 'Удалить',
    style: { color: '#d51006' },
    event: (row) => {
      deleteStudent(row)
    }
  }
]
