import React from 'react'

export const customS = {
  sectionR: {
    paddingBottom: 65
  }
}

export const ModalProps = {
  title: 'Создать ученика',
  id: 'student_create_modal',
  subTitle: 'После создания ученик получит на почту уникальный, сгенерированный пароль'
}

export const THeader = ({ teachers, groups, onTeacherChange }) => {
  return [
    {
      id: 'name',
      label: 'ФИО',
      minWidth: 200,
      filter: { id: 'fullName', type: 'search' },
      format: row => row.fullName
    },
    {
      id: 'phone',
      minWidth: 205,
      label: 'Номер телефона',
      filter: { id: 'phone', type: 'search' }
    },
    {
      id: 'email',
      label: 'Почта',
      filter: { id: 'email', type: 'search' }
    },
    {
      id: 'teacher',
      minWidth: 170,
      label: 'Учитель',
      format: row => {
        if (row.teacher) {
          return row.teacher.fullName
        } else {
          return ''
        }
      },
      filter: {
        id: 'teacher',
        type: 'select',
        withClear: 'group',
        filterEvent: onTeacherChange,
        options: teachers?.map(teacher => ({ ...teacher, format: opt => opt.fullName }))
      }
    },
    {
      id: 'group',
      label: 'Группа',
      align: 'center',
      format: row => {
        if (row.group) {
          return row.group.name
        } else {
          return ''
        }
      },
      filter: {
        type: 'select',
        options: groups?.length === 0 ? [{
          id: 1,
          name: 'Без группы'
        }] : groups,
        id: groups.length === 0 ? 'withoutTeacher' : 'group'
      }
    },
    {
      id: 'enable',
      label: 'Статус',
      align: 'center',
      format: row => {
        if (row.enable) {
          return <img src="/assets/images/EnableIcon.svg" alt="Enable"/>
        } else {
          return <img src="/assets/images/DisabledIcon.svg" alt="Disabled"/>
        }
      }
    }
  ]
}

export const TActions = ({ setSelectedStudent, studentsDispatch, deleteStudent, setStudentStatus, openEditModal }) => [
  {
    id: 'goTo',
    isLink: true,
    path: '/admin/students/',
    title: 'Перейти в профиль',
    event: (row) => {
      setSelectedStudent(row)
    }
  },
  {
    id: 'edit',
    title: 'Редактировать',
    event: (row) => {
      openEditModal()
      studentsDispatch({
        selectedStudent: row,
        type: 'SET_SELECTED_STUDENT_SUCCESS'
      })
    }
  },
  {
    id: 'disable',
    event: (row) => {
      setStudentStatus(row, !row.enable)
    },
    format: (row) => row.enable ? 'Отключить' : 'Включить'
  },
  {
    id: 'delete',
    title: 'Удалить',
    event: (row) => {
      deleteStudent(row)
    },
    style: { color: '#d51006' }
  }
]
