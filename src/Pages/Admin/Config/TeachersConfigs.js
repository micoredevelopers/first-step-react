import React from 'react'

export const THeader = [
  {
    id: 'name',
    label: 'ФИО',
    filter: { id: 'fullName', type: 'search' },
    format: row => row.fullName
  },
  {
    id: 'phone',
    minWidth: 205,
    label: 'Номер телефона',
    filter: { id: 'phone', type: 'search' }
  },
  {
    id: 'email',
    label: 'Почта',
    filter: { id: 'email', type: 'search' }
  },
  {
    id: 'groups',
    minWidth: 205,
    label: 'Группы',
    format: row => row.studentGroups?.map(group => group.name).join(', ')
  },
  {
    id: 'enable',
    label: 'Статус',
    align: 'center',
    format: row => {
      if (row.enable) {
        return <img src="/assets/images/EnableIcon.svg" alt="Enable"/>
      } else {
        return <img src="/assets/images/DisabledIcon.svg" alt="Disabled"/>
      }
    }
  }
]

export const TActions = ({ setSelectedTeacher, teachersDispatch, deleteTeacher, setTeacherStatus, openEditModal }) => [
  {
    id: 'goTo',
    isLink: true,
    path: '/admin/teachers/',
    title: 'Перейти в профиль',
    event: (row) => {
      setSelectedTeacher(row)
    }
  },
  {
    id: 'edit',
    title: 'Редактировать',
    event: (row) => {
      teachersDispatch({
        selectedTeacher: row,
        type: 'SET_SELECTED_TEACHER_SUCCESS'
      })
      openEditModal()
    }
  },
  {
    id: 'disable',
    event: (row) => {
      setTeacherStatus(row, !row.enable)
    },
    format: (row) => row.enable ? 'Отключить' : 'Включить'
  },
  {
    id: 'delete',
    title: 'Удалить',
    style: { color: '#d51006' },
    event: (row) => {
      deleteTeacher(row)
    }
  }
]

export const customS = {
  sectionR: {
    paddingBottom: 65
  }
}

export const ModalProps = {
  id: 'teachers_create_modal',
  title: 'Создать учителя',
  subTitle: 'После создания учитель получит на почту уникальный, сгенерированный пароль'
}
