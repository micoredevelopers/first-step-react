import React, { useState, useEffect } from 'react'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router'
import Grid from '@material-ui/core/Grid'
import { formatDate } from '../../Helpers'
import { useStudentsDispatch, useStudentsStore } from '../../Store'
import { deleteStudent, enableStudent, getPayments } from '../../Api'
import { ask, PhotoModal, SectionLayoutBase, TableC } from '../../Components'
import { THeader, TActions, PaymentsS } from './Config/PaymentsConfig'
import { PaymentModalButtons } from '../../Components/Cards/Components'

export const PaymentsAdmin = () => {
  const History = useHistory()
  const studentsDispatch = useStudentsDispatch()
  const { students, pagination } = useStudentsStore()
  const [payment, setPayment] = useState(null)
  const [openModal, setOpenModal] = useState(false)
  const [globalFilters, setGlobalFilters] = useState(null)

  /* UPDATE */
  const updateData = async (filters) => {
    const newFilters = { page: 1, ...filters }
    const apiData = await getPayments({
      orderByPaymentStatus: 1,
      month: formatDate(new Date(), 'yyyy-MM'),
      page: pagination.page,
      ...globalFilters,
      ...newFilters
    })
  
    filters && setGlobalFilters(prev => ({ ...prev, ...newFilters }))
    
    if (apiData) {
      const allPages = Math.ceil(apiData.count / apiData.limit)
  
      studentsDispatch({
        students: apiData.rows,
        type: 'SET_STUDENTS_LIST_SUCCESS',
        pagination: { page: apiData.page, limit: apiData.limit, maxPages: allPages }
      })
    }
  }
  
  /* TABLE ACTION EVENTS */
  const setSelectedStudent = (student) => {
    History.push(`/admin/students/${student.id}`, { student })
  }
  const handleDeleteStudent = async (student) => {
    ask({
      title: 'Удаление',
      btnTitle: 'Удалить',
      subTitle: `Удалить ученика?`,
      callback: async () => {
        const apiData = await deleteStudent(student.id)

        if (apiData) {
          toast.success('Ученик успешно удален!')
          updateData()
        }
      }
    })
  }
  const handleChangeStatus = async (student, status) => {
    ask({
      title: 'Аккаунт',
      btnTitle: `${!status ? 'Отключить' : 'Включить'}`,
      subTitle: `${!status ? 'Отключить' : 'Включить'} аккаунт к системе ${student.fullName}?`,
      callback: async () => {
        const apiData = await enableStudent(student.id, { enable: status })

        if (apiData) {
          toast.success(`Ученик успешно ${apiData.enable ? 'включен' : 'отключен'}!`)
          updateData()
        }
      }
    })
  }
  const handleClearFilters = () => {
    setGlobalFilters(null)
  }
  
  /* MODAL EVENTS */
  const openCheckModal = (currentPayment) => {
    setOpenModal(true)
    setPayment(currentPayment)
  }
  const closeCheckModal = () => {
    setPayment(null)
    setOpenModal(false)
  }
  
  /* FILTER EVENTS */
  const handleChangeMonth = ({ name, value }) => {
    console.log(value)
    updateData({ [name]: value })
  }
  
  /* EFFECTS */
  useEffect(() => {
    !globalFilters && updateData()
    //eslint-disable-next-line
  }, [globalFilters])
  
  return (
    <SectionLayoutBase
      title="Оплата"
      noBottomOffset
      withSelectFilter
      withGlobalFilters
      customS={PaymentsS}
      filtersList={THeader}
      updateFilters={updateData}
      handleSelect={handleChangeMonth}
      clearFilters={handleClearFilters}
    >
      <Grid item xs={12} style={{ padding: '0 15px', height: '100%' }}>
        <TableC
          tableData={students}
          tableHeader={THeader}
          updateData={updateData}
          pagination={pagination}
          tableActions={TActions({
            openCheckModal,
            setSelectedStudent,
            deleteStudent: handleDeleteStudent,
            setCheckStatus: handleChangeStatus
          })}
        />
      </Grid>
      {payment && (
        <PhotoModal
          isPhotoFiles
          open={openModal}
          close={closeCheckModal}
          slides={payment.receiptFile ? [payment.receiptFile] : []}
          title={!payment.receiptFile ? 'Чек еще не добавлен!' : 'Чек'}
          btnOptions={(payment.receiptFile && payment.status !== 'paid') ?
            PaymentModalButtons({ data: payment, update: updateData, closeModal: closeCheckModal }) : []}
        />
      )}
    </SectionLayoutBase>
  )
}
