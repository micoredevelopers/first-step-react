import React, { useEffect, useState } from 'react'
import Grid from '@material-ui/core/Grid'
import { useLocation, useHistory } from 'react-router'
import { getPayments, getStudents } from '../../Api'
import { useStudentsStore, useStudentsDispatch, } from '../../Store'
import { GeneralCard, InfoCard, SectionLayoutBase, CreateStudentFields } from '../../Components'
/* STYLES */
import { studentPageStyles, customS } from './Styles/StudentPageStyled'

export const StudentPage = () => {
  const History = useHistory()
  const Location = useLocation()
  const classes = studentPageStyles()
  const studentsDispatch = useStudentsDispatch()
  const [payments, setPayments] = useState([])
  const { selectedStudent, students } = useStudentsStore()
  const studentId = Number(Location.pathname.split('/')[3])
  const [filters, setFilters] = useState({ student: studentId, order: 'desc' })

  /* EVENTS */
  const changeStudent = (student) => () => {
    History.push(`/admin/students/${student.id}`, { student })
  }

  /* API CALL */
  const getPaymentsList = async () => {
    const apiData = await getPayments({ students: selectedStudent.id })
    
    if (apiData) {
      const sortedPayments = apiData.rows.sort((prev, next) => (
        new Date(next.payDate) - new Date(prev.payDate)
      ))

      setPayments(sortedPayments)
    }
  }
  const getStudentGroup = async () => {
    const apiData = await getStudents({ group: selectedStudent.group?.id, limit: 50 })
    
    if (apiData) {
      const filteredStudents = apiData.rows.filter(student => student.id !== selectedStudent.id)
  
      studentsDispatch({
        students: filteredStudents,
        type: 'SET_STUDENTS_LIST_SUCCESS'
      })
    }
  }

  /* EFFECTS */
  useEffect(() => {
    Location.state?.student && studentsDispatch({
      type: 'SET_SELECTED_STUDENT_SUCCESS',
      selectedStudent: Location.state.student
    })
    //eslint-disable-next-line
  }, [Location.state])
  useEffect(() => {
    if (selectedStudent) {
      getPaymentsList()
      getStudentGroup()
      setFilters(prev => ({ ...prev, student: selectedStudent.id }))
    }
    //eslint-disable-next-line
  }, [selectedStudent])

  return (
    <SectionLayoutBase filters={filters} customS={customS}>
      <Grid item xs={12} lg={6} className={classes.itemGridRoot}>
        {selectedStudent && (
          <InfoCard
            info={selectedStudent}
            ModalContent={CreateStudentFields}
            contentProps={{ student: selectedStudent }}
            modalProps={{ id: 'student_edit', title: 'Редактировать ученика' }}
          />
        )}
        <GeneralCard
          showData
          showType
          showChipMark
          cardType="lessons"
          cardTitle="Оценки"
          lessonType="lessons"
          className={classes.mb15}
          filters={{ ...filters, byStudent: 1 }}
        />
        <GeneralCard
          isLink
          withArrow
          isStudents
          cardTitle="Класс"
          cardType="students"
          withoutDescription
          cardData={students}
          className={classes.mt15}
          linkEvent={changeStudent}
        />
      </Grid>
      <Grid item xs={12} lg={6} className={classes.itemGridRoot}>
        <GeneralCard
          showData
          filters={filters}
          cardType="lessons"
          lessonType="lessons"
          cardTitle="Расписание"
          className={classes.mb15}
        />
        <GeneralCard
          showData
          isStudents
          filters={filters}
          cardTitle="Оплаты"
          cardType="payments"
          cardData={payments}
          update={getPaymentsList}
          className={classes.mt15}
        />
      </Grid>
    </SectionLayoutBase>
  )
}
