import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router'
import Grid from '@material-ui/core/Grid'
import { deleteStudent, enableStudent, getGroups, getStudents } from '../../Api'
import { THeader, TActions, ModalProps, customS } from './Config/StudentsConfig'
import { useStaticListsStore, useStudentsDispatch, useStudentsStore } from '../../Store'
import { ask, ModalInnerContent, SectionLayoutBase, TableC, CreateStudentFields } from '../../Components'

export const Students = () => {
  const History = useHistory()
  const { teachers } = useStaticListsStore()
  const studentsDispatch = useStudentsDispatch()
  const [groups, setGroups] = useState([])
  const [openModal, setOpenModal] = useState(false)
  const [globalFilters, setGlobalFilters] = useState(null)
  const { students, pagination, selectedStudent } = useStudentsStore()
  
  /* UPDATE */
  const updateData = async (filters) => {
    const apiData = await getStudents({ page: pagination.page, ...globalFilters, ...filters })
    
    filters && setGlobalFilters(prev => ({ ...prev, ...filters }))
    
    if (apiData) {
      const allPages = Math.ceil(apiData.count / apiData.limit)
      
      studentsDispatch({
        students: apiData.rows,
        type: 'SET_STUDENTS_LIST_SUCCESS',
        pagination: { page: apiData.page, limit: apiData.limit, maxPages: allPages }
      })
    }
  }
  
  /* TABLE ACTION EVENTS */
  const setSelectedStudent = (student) => {
    History.push(`/admin/students/${student.id}`, { student })
  }
  const handleDeleteStudent = async (student) => {
    ask({
      title: 'Удаление',
      btnTitle: 'Удалить',
      subTitle: `Удалить ученика ${student.fullName}?`,
      callback: async () => {
        const apiData = await deleteStudent(student.id)
        
        if (apiData) {
          toast.success('Ученик успешно удален!')
          updateData()
        }
      }
    })
  }
  const handleChangeStatus = async (student, status) => {
    ask({
      title: 'Доступы',
      btnTitle: `${!status ? 'Отключить' : 'Включить'}`,
      subTitle: `${!status ? 'Отключить' : 'Включить'} доступ к системе ученику ${student.fullName}?`,
      callback: async () => {
        const apiData = await enableStudent(student.id, { enable: status })
        
        if (apiData) {
          toast.success(`Ученик успешно ${apiData.enable ? 'включен' : 'отключен'}!`)
          updateData()
        }
      }
    })
  }
  const handleClearFilters = () => {
    setGlobalFilters(null)
  }
  const onTeacherChange = async (data) => {
    if (data.teacher !== '') {
      const apiData = await getGroups({ teachers: data.teacher, limit: 50 })
      
      if (apiData) {
        setGroups(apiData)
      }
    } else {
      setGroups([])
    }
  }
  
  /* MODAL EVENTS */
  const openEditModal = () => {
    setOpenModal(true)
  }
  const closeEditModal = () => {
    setOpenModal(false)
  }
  
  /* EFFECTS */
  useEffect(() => {
    !globalFilters && updateData({ page: 1 })
    //eslint-disable-next-line
  }, [globalFilters])
  
  return (
    <>
      <SectionLayoutBase
        title="Ученики"
        noBottomOffset
        customS={customS}
        withGlobalFilters
        ModalProps={ModalProps}
        modalSubmit={updateData}
        btnTitle="Создать ученика"
        updateFilters={updateData}
        clearFilters={handleClearFilters}
        ModalContent={CreateStudentFields}
        filtersList={THeader({ teachers, groups, onTeacherChange })}
      >
        <Grid item xs={12} style={{ height: '100%', padding: '0 15px' }}>
          <TableC
            tableData={students}
            updateData={updateData}
            pagination={pagination}
            tableActions={TActions({
              openEditModal,
              studentsDispatch,
              setSelectedStudent,
              deleteStudent: handleDeleteStudent,
              setStudentStatus: handleChangeStatus
            })}
            tableHeader={THeader({ teachers, groups, onTeacherChange })}
          />
        </Grid>
      </SectionLayoutBase>
      {selectedStudent && (
        <ModalInnerContent open={openModal} close={closeEditModal} id="student_edit_modal" title="Редактирование ученика">
          <CreateStudentFields student={selectedStudent} close={closeEditModal} update={updateData}/>
        </ModalInnerContent>
      )}
    </>
  )
}
