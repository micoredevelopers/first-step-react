import { makeStyles } from '@material-ui/core/styles'

export const customS = {
  containerR: {
    padding: 0,
    maxWidth: 1110
  },
  contentGridR: {
    overflow: 'visible'
  }
}

export const studentPageStyles = makeStyles(theme => ({
  mb15: {
    marginBottom: 15
  },
  mt15: {
    marginTop: 15
  },
  itemGridRoot: {
    height: '100%',
    display: 'flex',
    padding: '0 15px',
    position: 'relative',
    flexDirection: 'column',
  
    [theme.breakpoints.down('lg')]: {
      marginBottom: 15
    }
  },
  halfCard: {
    minHeight: 'calc(50% - 15px)',
    maxHeight: 'calc(50% - 15px)'
  }
}))
