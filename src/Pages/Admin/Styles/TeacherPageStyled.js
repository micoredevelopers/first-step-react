import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import makeStyles from '@material-ui/core/styles/makeStyles'

export const customS = {
  contentGridR: {
    overflow: 'visible'
  }
}

export const ArrowBack = styled.div`
  top: 20px;
  z-index: 1;
  left: -56px;
  padding: 20px;
  cursor: pointer;
  position: absolute;
  transform: rotate(-180deg);
  
  img {
    width: 16px;
    height: 32px;
  }
  
  ${down('lg')} {
    top: 0;
    left: 0;
    padding: 0;
    width: 16px;
    position: relative;
    margin-bottom: 15px;
  }
`
export const teacherPageStyles = makeStyles(theme => ({
  itemGridRoot: {
    height: '100%',
    display: 'flex',
    padding: '0 15px',
    position: 'relative',
    flexDirection: 'column',
  
    [theme.breakpoints.down('lg')]: {
      marginBottom: 15
    }
  },
  halfCard: {
    minHeight: 'calc(50% - 15px)',
    maxHeight: 'calc(50% - 15px)',
    
    [theme.breakpoints.down('lg')]: {
      minHeight: 'calc(50vh - 15px)',
      maxHeight: 'calc(50vh - 15px)',
    }
  },
  mt15: {
    marginTop: 15
  },
  mb15: {
    marginBottom: 15
  }
}))
