import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import Grid from '@material-ui/core/Grid'
import { useLocation } from 'react-router'
import { getGroups, getLessons, getStudentLessonWork } from '../../Api'
import {
  InfoCard,
  GeneralCard,
  CardHomework,
  SectionLayoutBase,
  CreateTeacherFields
} from '../../Components'
import {
  useGroupsStore,
  useLessonsStore,
  useTeachersStore,
  useGroupsDispatch,
  useLessonWorkStore,
  useLessonsDispatch,
  useTeachersDispatch,
  useStaticListsStore,
  useLessonWorkDispatch
} from '../../Store'
/* STYLES */
import { teacherPageStyles, ArrowBack, customS } from './Styles/TeacherPageStyled'
import { checkDevice } from '../../Helpers'

export const TeacherPage = () => {
  const Location = useLocation()
  const classes = teacherPageStyles()
  const groupsDispatch = useGroupsDispatch()
  const lessonsDispatch = useLessonsDispatch()
  const teachersDispatch = useTeachersDispatch()
  const lessonWorksDispatch = useLessonWorkDispatch()
  const teacherId = Number(Location.pathname.split('/')[3])
  const { selectedGroup } = useGroupsStore()
  const { teachers } = useStaticListsStore()
  const { selectedLesson } = useLessonsStore()
  const { selectedTeacher } = useTeachersStore()
  const { allLessonWorks, lessonWork } = useLessonWorkStore()
  const [filters] = useState({ teachers: teacherId })
  const [teacherHomeworks, setTeacherHomeworks] = useState([])
  const [openModal, setOpenModal] = useState({ photo: false, file: false })
  
  /* ON CLICK EVENTS */
  const handleSelectWork = (work) => () => {
    lessonWorksDispatch({
      lessonWork: work,
      type: 'GET_LESSON_WORK_SUCCESS'
    })
  }
  const setSelectedLesson = (lesson) => () => {
    lessonsDispatch({
      selectedLesson: lesson,
      type: 'SET_SELECTED_LESSON_SUCCESS'
    })
  }
  const handleBackToMain = () => {
    lessonsDispatch({ type: 'CLEAR_SELECTED_LESSON_SUCCESS' })
  }
  
  /* MODALS */
  const handleOpenModal = (modal) => (e) => {
    e.stopPropagation()
    setOpenModal(prev => ({ ...prev, [modal]: true }))
  }
  const handleCLoseModal = () => {
    setOpenModal({ photo: false, file: false })
  }
  
  /* API */
  const getTeacherById = () => {
    teachers.forEach(teacher => {
      if (teacher.id === teacherId) {
        teachersDispatch({
          selectedTeacher: teacher,
          type: 'SET_SELECTED_TEACHER_SUCCESS'
        })
      }
    })
  }
  const getTeacherGroups = async () => {
    const apiData = await getGroups({ teachers: selectedTeacher.id, limit: 50 })
  
    if (apiData) {
      groupsDispatch({
        groups: apiData,
        type: 'SET_GROUPS_LIST_SUCCESS'
      })
    }
  }
  const getLessonWorks = async (filters) => {
    const apiData = await getStudentLessonWork(filters)
    
    if (apiData) {
      lessonWorksDispatch({
        allLessonWorks: apiData,
        type: 'GET_ALL_LESSON_WORKS_SUCCESS'
      })
    }
  }
  
  const getTeacherHomeworks = async (newFilters) => {
    const apiData = await getLessons({ group: selectedGroup?.id, ...newFilters, ...filters })
  
    if (apiData.rows) {
      const homeworks = apiData.rows.filter(lesson => lesson.homeworkDescription)
      
      if (!selectedLesson) {
        setTeacherHomeworks(homeworks)
      } else {
        setTeacherHomeworks(apiData.rows)
      }
    }
  }
  
  /* EFFECTS */
  useEffect(() => {
    getTeacherHomeworks({ limit: 50 })
    Location.state?.lesson && setSelectedLesson(Location.state.lesson)()
    Location.state?.teacher && teachersDispatch({
      type: 'SET_SELECTED_TEACHER_SUCCESS',
      selectedTeacher: Location.state.teacher
    })
    //eslint-disable-next-line
  }, [])
  useEffect(() => {
    getTeacherHomeworks({ limit: 50 })
  }, [selectedGroup])
  useEffect(() => {
    if (!Location.state?.teacher) {
      teachers.length > 0 && getTeacherById()
    }
    //eslint-disable-next-line
  }, [teachers])
  useEffect(() => {
    if (selectedLesson) {
      getLessonWorks({
        ...filters,
        groups: selectedGroup?.id,
        lesson: selectedLesson.lesson?.id ?? selectedLesson.id
      })
      getTeacherHomeworks({ byStudent: 1 })
    }
    //eslint-disable-next-line
  }, [selectedLesson])
  useEffect(() => {
    selectedTeacher && getTeacherGroups()
    //eslint-disable-next-line
  }, [selectedTeacher])
  
  return (
    <SectionLayoutBase
      withGroupFilters
      filters={filters}
      customS={customS}
      title={checkDevice('min-lg') && 'Выберите группу:'}
    >
      <Grid item xs={12} lg={6} className={classes.itemGridRoot}>
        {selectedLesson && (
          <ArrowBack onClick={handleBackToMain}>
            <img src="/assets/images/ArrowRightBlack.svg" alt="Back"/>
          </ArrowBack>
        )}
        {selectedTeacher && !selectedLesson && (
          <InfoCard
            info={selectedTeacher}
            ModalContent={CreateTeacherFields}
            contentProps={{ teacher: selectedTeacher }}
            modalProps={{ id: 'teacher_edit', title: 'Редактировать учителя' }}
          />
        )}
        <GeneralCard
          showData
          showGroup
          cardType="lessons"
          lessonType="lessons"
          isLink={selectedLesson}
          showType={selectedLesson}
          withArrow={selectedLesson}
          cardData={teacherHomeworks}
          showFiles={!selectedLesson}
          showComment={selectedLesson}
          isHomework={!selectedLesson}
          linkEvent={setSelectedLesson}
          isSelected={selectedLesson?.id}
          filters={{ ...filters, order: 'desc' }}
          cardTitle={selectedLesson ? 'Оценки и дз' : 'Домашние задания'}
        />
      </Grid>
      {!selectedLesson && (
        <Grid item xs={12} lg={6} className={classes.itemGridRoot}>
          <GeneralCard
            showData
            showGroup
            filters={filters}
            cardType="lessons"
            lessonType="lessons"
            cardTitle="Расписание"
            className={clsx(classes.halfCard, classes.mb15)}
          />
          <GeneralCard
            isLink
            showType
            showData
            showGroup
            showComment
            cardType="lessons"
            lessonType="lessons"
            cardTitle="Оценки и дз"
            linkEvent={setSelectedLesson}
            className={clsx(classes.halfCard, classes.mt15)}
            filters={{ ...filters, byStudent: 1, order: 'desc' }}
          />
        </Grid>
      )}
      
      {selectedLesson && (
        <Grid item xs={12} lg={6} className={classes.itemGridRoot}>
          <GeneralCard
            showMarks
            isStudents
            filters={filters}
            cardTitle="Ученики"
            cardType="students"
            cardData={allLessonWorks}
            isSelected={lessonWork?.id}
            linkEvent={handleSelectWork}
            showAbsent={selectedLesson.type}
            isHomework={!selectedLesson.type}
            className={!selectedLesson.type ? clsx(classes.halfCard, classes.mb15) : ''}
          />
          {!selectedLesson.type && (
            <CardHomework
              open={openModal}
              openModal={handleOpenModal}
              closeModal={handleCLoseModal}
              className={clsx(classes.halfCard, classes.mt15)}
            />
          )}
        </Grid>
      )}
    </SectionLayoutBase>
  )
}
