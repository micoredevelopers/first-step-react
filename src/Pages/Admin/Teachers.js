import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router'
import Grid from '@material-ui/core/Grid'
import { useTeachersStore, useTeachersDispatch } from '../../Store'
import { deleteTeacher, enableTeacher, getTeachers } from '../../Api'
import { THeader, TActions, ModalProps, customS } from './Config/TeachersConfigs'
import { ask, ModalInnerContent, SectionLayoutBase, TableC, CreateTeacherFields } from '../../Components'

export const Teachers = () => {
  const History = useHistory()
  const teachersDispatch = useTeachersDispatch()
  const [openModal, setOpenModal] = useState(false)
  const { teachers, pagination, selectedTeacher } = useTeachersStore()
  const [globalFilters, setGlobalFilters] = useState(null)

  /* UPDATE */
  const updateData = async (filters) => {
    const newFilters = { page: 1, ...filters }
    const apiData = await getTeachers({ page: pagination.page, ...globalFilters, ...newFilters })
  
    filters && setGlobalFilters(prev => ({ ...prev, ...newFilters }))
    
    if (apiData) {
      const allPages = Math.ceil(apiData.count / apiData.limit)
      
      teachersDispatch({
        teachers: apiData.rows,
        type: 'SET_TEACHERS_LIST_SUCCESS',
        pagination: { page: apiData.page, limit: apiData.limit, maxPages: allPages }
      })
    }
  }
  
  /* TABLE ACTION EVENTS */
  const setSelectedTeacher = (teacher) => {
    History.push(`/admin/teachers/${teacher.id}`, { teacher })
  }
  const handleDeleteTeacher = async (teacher) => {
    ask({
      title: 'Удаление',
      subTitle: `Удалить учителя ${teacher.fullName}?`,
      btnTitle: 'Удалить',
      callback: async () => {
        const apiData = await deleteTeacher(teacher.id)

        if (apiData) {
          toast.success('Учитель успешно удален!')
          updateData()
        }
      }
    })
  }
  const handleChangeStatus = async (teacher, status) => {
    ask({
      title: 'Доступы',
      subTitle: `${!status ? 'Отключить' : 'Включить'} доступ к системе учителю ${teacher.fullName}?`,
      btnTitle: `${!status ? 'Отключить' : 'Включить'}`,
      callback: async () => {
        const apiData = await enableTeacher(teacher.id, { enable: status })

        if (apiData) {
          toast.success(`Учитель успешно ${apiData.enable ? 'включен' : 'отключен'}!`)
          updateData()
        }
      }
    })
  }
  const handleClearFilters = () => {
    setGlobalFilters(null)
  }
  
  /* MODAL EVENTS */
  const openEditModal = () => {
    setOpenModal(true)
  }
  const closeEditModal = () => {
    setOpenModal(false)
  }
  
  /* EFFECTS */
  useEffect(() => {
    !globalFilters && updateData()
    //eslint-disable-next-line
  }, [globalFilters])
  
  return (
    <SectionLayoutBase
      noBottomOffset
      title="Учителя"
      customS={customS}
      withGlobalFilters
      filtersList={THeader}
      ModalProps={ModalProps}
      modalSubmit={updateData}
      btnTitle="Создать учителя"
      updateFilters={updateData}
      clearFilters={handleClearFilters}
      ModalContent={CreateTeacherFields}
    >
      <Grid item xs={12} style={{ height: '100%', padding: '0 15px' }}>
        <TableC
          tableData={teachers}
          tableHeader={THeader}
          updateData={updateData}
          pagination={pagination}
          tableActions={TActions({
            openEditModal,
            teachersDispatch,
            setSelectedTeacher,
            deleteTeacher: handleDeleteTeacher,
            setTeacherStatus: handleChangeStatus
          })}
        />
      </Grid>
      {selectedTeacher && (
        <ModalInnerContent open={openModal} close={closeEditModal} id="teachers_edit_modal" title="Редактирование учителя">
          <CreateTeacherFields teacher={selectedTeacher} close={closeEditModal} update={updateData}/>
        </ModalInnerContent>
      )}
    </SectionLayoutBase>
  )
}
