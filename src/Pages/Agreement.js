import React from 'react' //useState
// import { session } from 'store2'
// import { toast } from 'react-toastify'
import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { CustomBtn, SectionLayout, SectionTitle, TitleRow } from '../Components'
import { checkDevice } from '../Helpers'
// import { confirmContract } from '../Api'
// import { formatDate, getUser } from '../Helpers'

const useStyles = makeStyles(theme => ({
  gridContainer: {
    maxWidth: 920,
    margin: '0 auto'
  },
  rootContainer: {
    height: '100%',
    padding: '0 100px',
    [theme.breakpoints.down('md')]: {
      padding: '0 15px'
    }
  }
}))

const AgreementText = styled.p`
  color: #000c18;
  font-size: 16px;
  font-weight: 400;
  margin-bottom: 40px;
  font-family: "Avenir Next Cyr";
  
  ${down('md')} {
    font-size: 15px;
    margin-bottom: 30px;
  }
`

// const AcceptText = styled.p`
//   width: 100%;
//   font-size: 16px;
//   text-align: right;
//   font-family: "Avenir Next Cyr";
//   color: ${({ theme }) => theme.colors.success};
// `

const Styles = checkDevice('min-lg') ? ({
  titleRow: {
    padding: 0,
    marginBottom: 10
  },
}) : null

export const Agreement = () => {
  // const user = getUser()
  const classes = useStyles()
  // const [contractDate, setContractDate] = useState(user.confirmContractDate ? formatDate(user.confirmContractDate, 'dd MMM') : null)
  
  // const handleAccept = async () => {
  //   const apiData = await confirmContract()
  //
  //   if (apiData) {
  //     session.set('user', { ...user, confirmContractDate: apiData.confirmContractDate })
  //     setContractDate(apiData.confirmContractDate)
  //     toast.success('Договор успешно принят!')
  //   } else {
  //     toast.error('При принятии договора произошла ошибка!')
  //   }
  // }
  
  return (
      <SectionLayout style={{ height: 'auto' }}>
        <Container maxWidth={false} className={classes.rootContainer}>
          <Grid container spacing={0} className={classes.gridContainer}>
            <Grid item xs={12}>
              <TitleRow isTitle style={Styles?.titleRow}>
                <SectionTitle variant="h1" style={{ marginBottom: 0, ...Styles?.title }}>Договор</SectionTitle>
                {/*{contractDate && <AcceptText>Принято {contractDate}</AcceptText>}*/}
              </TitleRow>
            </Grid>
            <Grid item xs={12}>
              <AgreementText>
                Скоро будет...
              </AgreementText>
            </Grid>
            <Grid item xs={12}>
              <CustomBtn title="Принять условия" disabled/>
              {/*{!contractDate && <CustomBtn title="Принять условия" onClick={handleAccept}/>}*/}
            </Grid>
          </Grid>
        </Container>
      </SectionLayout>
  )
}
