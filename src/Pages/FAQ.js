import React, { useState } from 'react'
import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { FormInput, SectionLayout, SectionTitle, TitleRow } from '../Components'
import { checkDevice, getAccountType } from '../Helpers'

const useStyles = makeStyles(theme => ({
  gridContainer: {
    maxWidth: 920,
    margin: '0 auto'
  },
  rootContainer: {
    height: '100%',
    padding: '0 100px',
    [theme.breakpoints.down('md')]: {
      padding: '0 15px'
    }
  },
  searchInput: {
    marginBottom: 0,
    
    '& img': {
      left: 20,
      zIndex: 1,
      width: 12,
      height: 12,
      top: '50%',
      minWidth: 12,
      position: 'absolute',
      transform: 'translateY(-50%)'
    }
  }
}))

const QuestionCard = styled.div`
  width: 100%;
  padding: 30px;
  border-radius: 16px;
  background-color: #ffffff;
  
  ${down('md')} {
    padding: 25px 20px;
  }
`

const QuestionTopInfo = styled.div`
  display: flex;
  cursor: pointer;
  flex-direction: row;
  align-items: center;
  margin-bottom: 10px;
  justify-content: space-between;
`

const QuestionName = styled(Typography)`
  font-size: 24px;
  font-weight: 500;
  font-family: "Avenir Next Cyr Medium";
  color: ${({ theme }) => theme.colors.black};
  
  ${down('md')} {
    font-size: 18px;
  }
`

const QuestionArrow = styled.img`
  width: 14px;
  height: 7px;
  transition: all 0.4s ease-in-out;
  transform: rotateX(${({ collapsed }) => collapsed === 'true' ? 0 : 180}deg);
`

const QuestionDesc = styled(Typography)`
  display: block;
  font-size: 16px;
  font-weight: 400;
  overflow: hidden;
  line-height: 22px;
  display: -webkit-box;
  text-overflow: ellipsis;
  -webkit-box-orient: vertical;
  transition: all 0.6s ease-in-out;
  font-family: "Avenir Next Cyr", sans-serif;
  color: ${({ theme }) => theme.colors.black};
  -webkit-line-clamp: ${({ collapsed }) => collapsed === 'true' ? 3 : 'unset'};
  
  ${down('md')} {
    font-size: 15px;
  }
`

const Styles = checkDevice('min-lg') ? ({
  titleRow: {
    padding: 0,
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  title: {
    width: '100%',
    marginBottom: 15
  },
  searchInput: {
    maxWidth: '100%'
  }
}) : null

const TeachersQuestions = [
  {
    collapsed: true,
    name: 'Не получается войти?',
    desc: `Убедитесь что у ученика/учителя введен верный пароль и логин.`
  },
  {
    collapsed: true,
    name: 'Браузер устарел',
    desc: `Для корректной работы системы и отображения -  обновите браузер до последней версии.`
  },
  {
    collapsed: true,
    name: 'Как создать расписание?',
    desc: `Для того, чтобы создать расписание нажмите на кнопку «Создать расписание», выберите необходимую группу,
    время и тему урока, а также тип урока (обычный, контрольная работа или экзамен).
    Также, можно добавить заметку или комментарий к уроку.`
  },
  {
    collapsed: true,
    name: 'Можно ли поставить ученику оценку за урок?',
    desc: `После создания урока и его проведения вы можете поставить оценку за этот урок.
    Также, можно отметить кто был или не был на уроке. Если ученика не было, то оценку поставить ему нельзя.`
  },
  {
    collapsed: true,
    name: 'Что делать, если я не смогу провести урок?',
    desc: `Если вы не можете провести урок, то вы либо администратор может назначить замену.`
  },
  {
    collapsed: true,
    name: 'Урок уже прошел, но я забыл/а его создать.',
    desc: `Если вы провели урок, но забыли его создать заранее, вы можете его добавить в расписание после того, как урок уже проведен.`
  },
  {
    collapsed: true,
    name: 'Как мне добавить домашнее задание?',
    desc: `После создания урока вы можете добавить домашнее задание к нему. Перейдите во вкладку «Домашнее задание».
    Прикрепляйте файлы, фотографируйте учебник или тетрадь, а также оставляйте комментарии к заданию.`
  },
  {
    collapsed: true,
    name: 'Что делать ученику с выполненным заданием?',
    desc: `Ученик может прислать выполненное домашнее задание в своем личном кабинете.
    После этого вы сможете его проверить и поставить оценку за него.`
  },
  {
    collapsed: true,
    name: 'Какие возможности во вкладке «Расписание»?',
    desc: `Во вкладке «Расписание» можно отфильтровать расписание по группам либо посмотреть расписание за весь месяц и
    все группы. Вы можете также пролистать на предыдущие и будущие месяцы.
    Необходимую группу для фильтрации выберите слева от календаря, нажав на ее название.`
  },
  {
    collapsed: true,
    name: 'Как посмотреть все уроки за день?',
    desc: `Чтобы посмотреть все уроки за день, нажмите «more» и отобразятся все уроки на конкретную дату.
    Со всплывающего окна можно быстро перейти в конкретный урок.`
  },
  {
    collapsed: true,
    name: 'Как создать «Домашнее задание»?',
    desc: `Чтобы создать «Домашнее задание» выберите необходимую группу. Затем в выпадающем списке выберите необходимый урок.
    Если нужного урока нет, то вы уже создали домашнее задание для него ранее.`
  },
  {
    collapsed: true,
    name: 'Какие поля обязательны при создании урока?',
    desc: `При «Создании урока» поля, обязательные к заполнению, отмечены звездочками.`
  },
  {
    collapsed: true,
    name: 'Какие возможности у меня во вкладке «Домашнее задание»?',
    desc: `Во вкладке «Домашнее задание» вы можете добавлять домашние задания.  После сохранения домашнего задания,
    вы можете нажать на него и увидеть список учеников группы. После того, как ученик выполнит домашнее задание,
    вы можете нажать на этого ученика и посмотреть его ответ и поставить оценку за домашнее задание конкретного ученика.
    Если вы случайно поставили неправильную оценку, вы можете ее исправить, нажав на оценку.`
  },
]
const StudentsQuestions = []

const getQuestions = (account) => {
  if (account === 'teacher') {
    return TeachersQuestions
  } else {
    return StudentsQuestions
  }
}

export const FAQ = () => {
  const classes = useStyles()
  const account = getAccountType()
  const [search, setSearch] = useState('')
  const [questions, setQuestions] = useState(getQuestions(account))
  
  const handleChange = (e) => {
    const { value } = e.target
    
    setSearch(value)
    setQuestions(() => {
      if (value !== '') {
        return [...getQuestions(account).filter(question => question.name.includes(value) || question.desc.includes(value))]
      } else {
        return [...getQuestions(account)]
      }
    })
  }
  
  const handleCollapse = (index) => () => {
    setQuestions(prev => {
      prev[index].collapsed = !prev[index].collapsed
      
      return [...prev]
    })
  }
  
  return (
    <SectionLayout style={{ height: 'auto' }}>
      <Container maxWidth={false} className={classes.rootContainer}>
        <Grid container spacing={0} className={classes.gridContainer}>
          <Grid item xs={12}>
            <TitleRow isTitle style={Styles?.titleRow}>
              <SectionTitle variant="h1" style={{ marginBottom: 0, ...Styles?.title }}>FAQ</SectionTitle>
              <FormInput
                name="search"
                value={search}
                placeholder="Поиск"
                onChange={handleChange}
                className={classes.searchInput}
                icon="/assets/images/SearchIcon.svg"
              />
            </TitleRow>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={6}>
              {questions.map((question, index) => {
                const checkSymbols = question.desc.length < 150
                
                return (
                  <Grid key={`question_${index}`} item xs={12} md={6}>
                    <QuestionCard>
                      <QuestionTopInfo onClick={!checkSymbols ? handleCollapse(index) : null}>
                        <QuestionName variant="h3">{question.name}</QuestionName>
                        {!checkSymbols && (
                          <QuestionArrow
                            alt="Arrow"
                            src="/assets/images/ArrowDown.svg"
                            collapsed={`${question?.collapsed}`}
                          />
                        )}
                      </QuestionTopInfo>
                      <QuestionDesc collapsed={`${question?.collapsed}`}>{question.desc}</QuestionDesc>
                    </QuestionCard>
                  </Grid>
                )
              })}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </SectionLayout>
  )
}
