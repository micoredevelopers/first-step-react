import React from 'react'
import { toast } from 'react-toastify'
import styled from 'styled-components'
import { up } from 'styled-breakpoints'
import { LogIn } from '../Helpers'
import { CustomForm } from '../Components'
import { ApiFetch, loginUrl } from '../Api'

const LoginSection = styled.section`
  display: flex;
  min-height: 100vh;
  position: relative;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  
  img {
    width: 100%;
    height: 100%;
  }
  
  ${up('md')} {
    justify-content: center;
  }
`

const LogoWrap = styled.div`
  width: 80px;
  height: 80px;
  margin-bottom: 50px;
  
  ${up('md')} {
    width: 120px;
    height: 120px;
    margin-bottom: 20px;
  }
`

const SectionTitle = styled.h1`
  color: ${({ theme }) => theme.colors.black};
  font-family: Comfortaa, sans-serif;
  font-size: 48px;
  font-weight: 700;
  margin-bottom: 10px;
  
  ${up('md')} {
    font-size: 64px;
    margin-bottom: 15px;
  }
`

const SectionSubTitle = styled.h6`
  width: 100%;
  max-width: 100%;
  padding: 0 20px;
  color: ${({ theme }) => theme.colors.grey};
  font-family: "Avenir Next Cyr", sans-serif;
  font-size: 15px;
  font-weight: 400;
  text-align: center;
  margin-bottom: 30px;
  
  ${up('md')} {
    padding: 0;
    max-width: 410px;
    margin-bottom: 35px;
  }
`

const LoginS = {
  width: '100%',
  margin: '0 auto',
  maxWidth: '250px'
}

const LoginFields = [
  {
    type: 'input',
    name: 'username',
    defaultValue: '',
    size: { xs: 12 },
    placeholder: 'Почта',
    isValid: ['isEmpty']
  },
  {
    type: 'input',
    name: 'password',
    defaultValue: '',
    size: { xs: 12 },
    isValid: ['isEmpty'],
    placeholder: 'Пароль',
    inputType: 'password'
  },
]

export const Login = (props) => {
  const handleLoginSubmit = async (data) => {
    const apiCall = await ApiFetch(loginUrl, { method: 'POST', body: data })

    if (!apiCall.errors) {
      if (apiCall.data.enable) {
        switch (apiCall.data.type) {
          case 'teacher':
            props.history.push('/teacher')
            break
          case 'student':
            props.history.push('/student')
            break
          case 'admin':
            props.history.push('/admin')
            break
          default:
            props.history.push('/404')
            break
        }
  
        LogIn(apiCall.data)
      } else {
        toast.error('Ошибка входа, пользователь отключен от системы!')
      }
    } else {
      toast.error('Ошибка при входе')
    }
  }

  return (
    <LoginSection id="login-page">
      <LogoWrap>
        <img src="/assets/images/Logo.svg" alt="Logo"/>
      </LogoWrap>
      <SectionTitle>Вход</SectionTitle>
      <SectionSubTitle>
        Введите почту, которую вы давали менеджеру для регистрации и пароль, который менеджер дал вам
      </SectionSubTitle>
  
      <CustomForm
        btnTitle="Войти"
        formStyle={LoginS}
        fields={LoginFields}
        handleSubmit={handleLoginSubmit}
      />
    </LoginSection>
  )
}
