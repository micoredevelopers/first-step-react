import React, { useEffect, useState } from 'react'
import { Grid } from '@material-ui/core'
import { getStudents } from '../../Api'
import { useStudentsDispatch } from '../../Store/Context'
import { SectionLayoutBase, StudentClassContainer } from '../../Components'
import { checkDevice } from '../../Helpers'

const MobileS = checkDevice('min-lg') && ({
  item: {
    padding: '0 15px'
  },
  titleRow: {
    alignItems: 'stretch',
    flexDirection: 'column'
  },
  title: {
    marginBottom: 15
  }
})

const SectionS = {
  gridR: {
    maxWidth: 1220
  },
  sectionR: {
    overflow: 'auto',
    paddingBottom: 0
  },
  title: MobileS.title,
  itemGridR: MobileS.item,
  titleRow: MobileS.titleRow
}

export const Class = () => {
  const studentsDispatch = useStudentsDispatch()
  const [filters, setFilters] = useState(null)
  
  const update = async () => {
    const apiData = await getStudents(filters)
    
    if (apiData) {
      studentsDispatch({
        students: apiData.rows,
        type: 'SET_STUDENTS_LIST_SUCCESS'
      })
    }
  }
  const handleGlobalSearch = (e) => {
    const { value } = e.target
  
    if (e.key === 'Enter') {
      setFilters(prev => ({ ...prev, search: value }))
    }
  }
  
  useEffect(() => {
    update()
    //eslint-disable-next-line
  }, [filters])
  
  return (
    <SectionLayoutBase
      withSearch
      title="Мой класс"
      filters={filters}
      customS={SectionS}
      handleSearch={handleGlobalSearch}
    >
      <Grid item xs={12} style={{ padding: '0 15px 15px', height: '100%' }}>
        <StudentClassContainer/>
      </Grid>
    </SectionLayoutBase>
  )
}
