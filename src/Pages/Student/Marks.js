import React, { useEffect, useState } from 'react'
import Grid from '@material-ui/core/Grid'
import { getLessons } from '../../Api'
import { useLessonsDispatch } from '../../Store'
import { SectionLayoutBase, StudentMarksContainer } from '../../Components'
import { checkDevice } from '../../Helpers'

const SectionS = {
  gridR: {
    maxWidth: 1220
  },
  sectionR: {
    paddingBottom: 0
  },
  contentGridR: {
    overflow: 'hidden'
  }
}

const SectionMobileS = checkDevice('min-lg') && ({
  title: {
    marginBottom: 15
  },
  searchInput: {
    maxWidth: 'max-content'
  },
  titleRow: {
    flexDirection: 'column',
    alignItems: 'stretch'
  },
  titleRightWrap: {
    flexDirection: 'column',
    alignItems: 'flex-start',
  }
})

export const Marks = () => {
  const dispatch = useLessonsDispatch()
  const [filters, setFilters] = useState({ byStudent: 1 })

  const update = async (newFilters) => {
    const apiData = await getLessons({ ...newFilters, ...filters })

    if (apiData) {
      dispatch({
        lessons: apiData.rows,
        type: 'SET_LESSONS_LIST_SUCCESS',
        pagination: { page: apiData.page, limit: apiData.limit, maxPages: Math.ceil(apiData.count / apiData.limit)  }
      })
    }
  }
  
  const handleGlobalSearch = (e) => {
    const { value } = e.target
  
    if (e.key === 'Enter') {
      setFilters(prev => ({ ...prev, search: value }))
    }
  }
  
  const handleChangeWork = (data) => {
    setFilters(prev => ({ ...prev, [data.name]: data.checked ? 1 : 0 }))
  }

  useEffect(() => {
    update()
    //eslint-disable-next-line
  }, [filters])
  
  return (
    <SectionLayoutBase
      withSearch
      withSwitch
      noBottomOffset
      filters={filters}
      title="Оценки и дз"
      handleSwitch={handleChangeWork}
      handleSearch={handleGlobalSearch}
      customS={{ ...SectionS, ...SectionMobileS }}
    >
      <Grid item xs={12} style={{ padding: checkDevice('min-lg') ? '0 15px 15px' : '0 15px', height: '100%' }}>
        <StudentMarksContainer update={update}/>
      </Grid>
    </SectionLayoutBase>
  )
}
