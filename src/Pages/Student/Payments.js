import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid'
import { PaymentsProvider } from '../../Store'
import { SectionLayoutBase, StudentPaymentContainer } from '../../Components'
import { checkDevice } from '../../Helpers'

const MobileS = checkDevice('min-lg') && ({
  titleRow: {
    alignItems: 'stretch',
    flexDirection: 'column'
  },
  title: {
    marginBottom: 15
  },
  itemGridR: {
    padding: '0 15px'
  }
})

const SectionS = {
  sectionR: {
    paddingBottom: 0,
    overflow: 'auto'
  },
  gridR: {
    maxWidth: 1220
  },
  ...MobileS
}

export const Payments = () => {
  const [filters, setFilters] = useState(null)
  
  const handleGlobalSearch = (e) => {
    const { value } = e.target
  
    if (e.key === 'Enter') {
      setFilters(prev => ({ ...prev, search: value }))
    }
  }
  
  return (
    <PaymentsProvider>
      <SectionLayoutBase
        title="Оплата"
        customS={SectionS}
        handleSearch={handleGlobalSearch}
      >
        <Grid item xs={12} style={{ padding: '0 15px' }}>
          <StudentPaymentContainer filters={filters}/>
        </Grid>
      </SectionLayoutBase>
    </PaymentsProvider>
  )
}
