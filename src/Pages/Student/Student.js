import React, { useEffect, useState } from 'react'
import { session } from 'store2'
import { toast } from 'react-toastify'
import { useHistory } from 'react-router-dom'
import getDay from 'date-fns/getDay'
import addDays from 'date-fns/addDays'
import differenceInDays from 'date-fns/differenceInDays'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { RepeatStudy } from '../Teacher'
import { STATUSES } from '../../Constants'
import { studyStudent } from '../../Api/Study'
import { useLessonWorkDispatch } from '../../Store'
import { checkDevice, DesktopView, formatDate, getUser } from '../../Helpers'
import {
  Studying,
  GeneralCard,
  LightTooltip,
  SectionTitle,
  SectionLayout,
  SectionSubTitle
} from '../../Components'
import {
  Car,
  Wave,
  Girl,
  Tower,
  LoveFlag,
  RemindWrap,
  RemindText,
  ButtonsRow,
  RemindButton,
  LayoutImagesWrap
} from './Styles/StudentStyled'
import { getPayments } from '../../Api'

const useStyles = makeStyles((theme) => ({
  rootContainer: {
    zIndex: 1,
    height: '100%',
    maxWidth: '1220px',
    position: 'relative'
  },
  gridContainerRoot: { height: '100%' },
  gridItemRoot: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardGridContainer: {
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  cardGridItem: {
    marginBottom: 30,
    maxHeight: 'calc(33.3333% - 30px)',
    
    [theme.breakpoints.down('lg')]: {
      maxHeight: '100%'
    }
  }
}))

const StudentSteps = [
  {
    offset: 30,
    position: 'left',
    className: 'schedule-card',
    title: 'Информационная панель',
    desc: 'На этой панели вы можете видеть ближайшее расписание, последние оценки, ' +
      'а также домашние задания оставленные учителем'
  },
  {
    offset: 40,
    position: 'bottom',
    title: 'Оценки и дз',
    className: 'marks-link',
    desc: 'Вы сможете видеть все свои оценки за уроки, комментарии к ним, домашнее задание от учителя и оценку за него'
  },
  {
    offset: 40,
    position: 'bottom',
    title: 'Расписание',
    className: 'schedule-link',
    desc: 'Узнайте всё ваше расписание в календаре'
  },
  {
    offset: 40,
    position: 'bottom',
    title: 'Мой класс',
    className: 'class-link',
    desc: 'Узнайте кто учится в вашем классе и их посещаемость'
  },
  {
    offset: 40,
    title: 'Оплата',
    position: 'bottom',
    className: 'payment-link',
    desc: 'Отслеживайте ваши следующие и прошлые оплаты'
  },
  {
    offset: 40,
    position: 'bottom',
    id: 'account_menu',
    title: 'Всплывающее окно аккаунта',
    desc: 'Во всплывающем окно вы сможете найти вопросы-ответы'
  }
]

export const Student = () => {
  const user = getUser()
  const classes = useStyles()
  const History = useHistory()
  const [steps, setSteps] = useState(StudentSteps)
  const lessonWorkDispatch = useLessonWorkDispatch()
  const [repeatStudy, setRepeatStudy] = useState(false)
  const [remindStatus, setRemindStatus] = useState({ difference: 0, status: '' })
  const [remindDate, setRemindDate] = useState(localStorage.getItem('remind') ? localStorage.getItem('remind') : new Date())
  
  const goToHomework = (lesson) => () => {
    History.push('/student/marks')
    lessonWorkDispatch({
      lessonWork: lesson,
      type: 'GET_LESSON_WORK_SUCCESS'
    })
  }
  
  const handleRepeatStudy = () => {
    setSteps(StudentSteps)
    setRepeatStudy(true)
  }
  const handleEndStudying = async () => {
    const apiData = await studyStudent()
    
    if (apiData) {
      session.set('user', { ...user, wasTrained: apiData.wasTrained })
      toast.success('Обучение завершено!')
    }
  
    setSteps([])
    setRepeatStudy(true)
  }
  
  const checkPaymentStatus = async () => {
    const currentMonth = new Date()
    const apiData = await getPayments({
      students: user.id,
      month: formatDate(currentMonth, 'yyyy-MM')
    })
  
    if (apiData) {
      const payment = apiData.rows[0]
      const diff = differenceInDays(new Date(payment.payDate), currentMonth)

      if (payment.status !== 'paid') {
        if (diff <= 10 && diff > 5) {
          setRemindStatus({ status: STATUSES.LESS_10, difference: diff })
        } else if (diff <= 5 && diff > 2) {
          setRemindStatus({ status: STATUSES.LESS_5, difference: diff })
        } else if (diff <= 2 && diff >= 0) {
          setRemindStatus({ status: STATUSES.LESS_2, difference: diff })
        } else if (diff < 0) {
          setRemindStatus({ status: STATUSES.EXPIRED, difference: diff })
        }
      }
    }
  }
  const checkRemindDate = () => getDay(new Date()) >= getDay(new Date(remindDate))
  
  const handleCloseRemind = (e) => {
    e.preventDefault()
    const nextDay = addDays(new Date(), 1)
  
    setRemindDate(nextDay)
    localStorage.setItem('remind', `${nextDay}`)
  }
  
  useEffect(() => {
    checkPaymentStatus()
  }, [])

  return (
    <SectionLayout style={{ paddingBottom: checkDevice('min-lg') ? 0 : 30 }}>
      <DesktopView>
        <LayoutImagesWrap>
          <Car src="/assets/images/Car.svg" alt="Car"/>
          <Girl src="/assets/images/Girl.png" alt="Girl"/>
          <Wave src="/assets/images/Wave.png" alt="Wave"/>
          <Tower src="/assets/images/Tower.png" alt="Tower"/>
          <LoveFlag src="/assets/images/LoveFlag.png" alt="LoveFlag"/>
        </LayoutImagesWrap>
      </DesktopView>
      
      <Container maxWidth={false} className={classes.rootContainer}>
        <Grid container spacing={0} className={classes.gridContainerRoot}>
          <Grid item xs={12} md={6} className={classes.gridItemRoot}>
            <SectionTitle variant="h1">Hello, {user?.name}</SectionTitle>
            <SectionSubTitle variant="h6" style={{ marginBottom: checkDevice('min-lg') ? 20 : 0 }}>
              Рады приветствовать вас на онлайн-платформе курсов английского языка «The First Step»!
            </SectionSubTitle>
          </Grid>
          <Grid item xs={12} md={6} className={classes.gridItemRoot}>
            <Grid container spacing={0} className={classes.cardGridContainer}>
              <Grid item xs={12} className={classes.cardGridItem}>
                <GeneralCard
                  showData
                  withMobileSlider
                  cardType="lessons"
                  lessonType="lessons"
                  className="schedule-card"
                  cardTitle="Ближайшие уроки"
                  filters={{ newLessons: 1, order: 'desc' }}
                />
              </Grid>
              <Grid item xs={12} className={classes.cardGridItem}>
                <GeneralCard
                  viewOnly
                  showType
                  showMarks
                  isStudents
                  withMobileSlider
                  lessonType="marks"
                  cardType="students"
                  cardTitle="Последние оценки"
                  filters={{ student: user?.id }}
                />
              </Grid>
              <Grid item xs={12} className={classes.cardGridItem}>
                <GeneralCard
                  isLink
                  showData
                  withMobileSlider
                  cardType="lessons"
                  lessonType="homework"
                  linkEvent={goToHomework}
                  cardTitle="Домашние задания"
                  filters={{ byStudent: 1, order: 'desc' }}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
  
      {(repeatStudy || !user.wasTrained) && (
        <DesktopView>
          <Studying steps={steps} onEndStudying={handleEndStudying}/>
        </DesktopView>
      )}
      
      {!repeatStudy && (
        <DesktopView>
          <LightTooltip title="Повторить обучение?" placement="top" arrow>
            <RepeatStudy onClick={handleRepeatStudy}>
              <img src="/assets/images/StudyQuestion.svg" alt="Repeat study?"/>
            </RepeatStudy>
          </LightTooltip>
        </DesktopView>
      )}
      
      {(remindStatus.status !== '' && checkRemindDate()) && (
        <RemindWrap status={remindStatus.status}>
          <RemindText>
            {remindStatus.difference < 0 ?
              'Платеж просрочен, срочно оплатите его!' :
              `До конца оплаты осталось ${remindStatus.difference} дней`}
          </RemindText>
          <ButtonsRow>
            <RemindButton type="button" className="btn-payment" to="/student/payment">Оплатить</RemindButton>
            <RemindButton type="button" onClick={handleCloseRemind} to="/">Напомнить позже</RemindButton>
          </ButtonsRow>
        </RemindWrap>
      )}
    </SectionLayout>
  )
}
