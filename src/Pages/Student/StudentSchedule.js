import React, { useEffect, useState } from 'react'
import Grid from '@material-ui/core/Grid'
import { getLessons } from '../../Api'
import { formatDate, getUser } from '../../Helpers'
import { useLessonsDispatch } from '../../Store'
import { SectionLayoutBase, EventSchedule } from '../../Components'

const SectionS = {
  sectionR: {
    paddingBottom: 0
  }
}

export const StudentSchedule = () => {
  const user = getUser()
  const dispatch = useLessonsDispatch()
  const [filters, setFilters] = useState({ month: formatDate(new Date(), 'yyyy-MM'), groups: user.group?.id })

  const handleChangeDate = (date) => {
    const formattedDate = formatDate(date, 'yyyy-MM')

    setFilters(prev => ({ ...prev, month: formattedDate }))
  }
  
  const update = async () => {
    const apiData = await getLessons(filters)
    
    if (apiData) {
      dispatch({
        lessons: apiData.rows,
        type: 'SET_LESSONS_LIST_SUCCESS'
      })
    }
  }
  
  useEffect(() => {
    update()
    //eslint-disable-next-line
  }, [filters])

  return (
    <SectionLayoutBase
      noBottomOffset
      title="Расписание"
      filters={filters}
      customS={SectionS}
    >
      <Grid item xs={12} style={{ position: 'relative', height: '100%', display: 'flex', flexDirection: 'column' }}>
        <EventSchedule viewOnly update={update} handleChangeDate={handleChangeDate}/>
      </Grid>
    </SectionLayoutBase>
  )
}
