import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import { Link } from 'react-router-dom'
import { STATUSES } from '../../../Constants'

export const LayoutImagesWrap = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 0;
  position: absolute;
  
  img {
    position: absolute;
  }
`

export const Car = styled.img`
  z-index: 2;
  bottom: 30px;
  left: calc((100vw - (1110px + 120px)) / 2);
`

export const Wave = styled.img`
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  width: 100%;
`

export const Girl = styled.img`
  bottom: 0;
  z-index: 0;
  height: calc(100% - 223px);
  left: calc((100vw - (1110px + 185px)) / 2);
`

export const Tower = styled.img`
  bottom: 0;
  z-index: 2;
  right: 40px;
`

export const LoveFlag = styled.img`
  top: 45px;
  right: 40px;
`

export const RemindWrap = styled.div`
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 45px;
  z-index: 999;
  display: flex;
  position: fixed;
  align-items: center;
  justify-content: center;
  background-color: ${props => {
    switch (props.status) {
      case STATUSES.LESS_2:
      case STATUSES.EXPIRED:
        return '#d51006'
      case STATUSES.LESS_10:
        return '#06d58b'
      case STATUSES.LESS_5:
        return '#d59006'
      default: return '#d51006'
    }
  }};
  
  ${down('xs')} {
    height: auto;
    padding: 15px 0;
    align-items: center;
    flex-direction: column;
    justify-content: center;
  }
`

export const RemindText = styled.p`
  color: #fff;
  font-size: 20px;
  font-weight: 700;
  margin-right: 40px;
  font-family: Comfortaa;
  
  ${down('xs')} {
    margin-right: 0;
    text-align: center;
    margin-bottom: 15px;
  }
`

export const ButtonsRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  
  ${down('xs')} {
    align-items: center;
    flex-direction: column;
    justify-content: center;
  }
`

export const RemindButton = styled(Link)`
  height: 35px;
  display: flex;
  color: #000c18;
  padding: 0 30px;
  font-size: 16px;
  cursor: pointer;
  font-weight: 700;
  margin-right: 10px;
  align-items: center;
  background-color: #fff;
  font-family: Comfortaa;
  justify-content: center;
  
  ${down('xs')} {
    margin-right: 0;
  }
  
  &.btn-payment {
    color: #06d58b;
    
    ${down('xs')} {
      margin-bottom: 10px;
    }
  }
  
  &:last-child {
    margin-right: 10px;
  }
`
