import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import { toast } from 'react-toastify'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import { createHomework, getLessons, getStudentLessonWork } from '../../Api'
import {
  GeneralCard,
  CardHomework,
  SectionLayoutBase,
  CreateHomeworkFields, ask, ModalInnerContent
} from '../../Components'
import {
  useGroupsStore,
  useLessonsDispatch,
  useLessonWorkStore,
  useLessonWorkDispatch, useGroupsDispatch
} from '../../Store'
import { checkDevice } from '../../Helpers'

const ModalProps = {
  id: 'homework-modal',
  title: 'Создание ДЗ',
  subTitle: 'Все поля, кроме файла, обязательны к заполнению'
}

const EditModalProps = {
  id: 'homework-modal',
  title: 'Редактирование ДЗ',
  subTitle: 'Все поля, кроме файла, обязательны к заполнению'
}

const useStyles = makeStyles((theme) => ({
  fullHeight: {
    [theme.breakpoints.down('lg')]: {
      maxHeight: '100%'
    }
  },
  cardGridItem: {
    height: '100%',
    padding: '0 15px',
    
    [theme.breakpoints.down('lg')]: {
      padding: 0,
      height: 'auto',
      marginBottom: 15,
      maxHeight: '50vh'
    }
  }
}))

export const Homework = () => {
  const classes = useStyles()
  const groupsDispatch = useGroupsDispatch()
  const { selectedGroup } = useGroupsStore()
  const lessonsDispatch = useLessonsDispatch()
  const lessonWorksDispatch = useLessonWorkDispatch()
  const { allLessonWorks, lessonWork } = useLessonWorkStore()
  const [filters] = useState({ orderByDate: 'desc' })
  const [selectedLesson, setSelectedLesson] = useState(null)
  const [openModal, setOpenModal] = useState({ photo: false, file: false })
  const [openEditModal, setOpenEditModal] = useState(false)

  const update = async () => {
    const apiData = await getLessons({ group: selectedGroup?.id, ...filters })

    if (apiData) {
      lessonsDispatch({
        lessons: apiData.rows,
        type: 'SET_LESSONS_LIST_SUCCESS'
      })
    }
  }

  const getAllLessonWorks = async () => {
    const apiData = await getStudentLessonWork({ lesson: selectedLesson.id, groups: selectedGroup?.id })
    
    if (apiData) {
      lessonWorksDispatch({
        allLessonWorks: apiData,
        type: 'GET_ALL_LESSON_WORKS_SUCCESS'
      })
    }
  }
  
  /* CARDS CLICK EVENTS */
  const handleDeleteHomework = (row) => (e) => {
    e.stopPropagation()
    ask({
      title: 'Удаление ДЗ',
      subTitle: `Вы точно хотите удалить домашнее задание урока "${row.subject}"`,
      btnTitle: 'Удалить',
      callback: async () => {
        const apiData = await createHomework({ lessonId: row.id, data: { files: [] } })
  
        if (apiData) {
          toast.success('Домашнее задание удалено!')
          update()
        } else {
          toast.error('При удалении домашнего задания произошла ошибка!')
        }
      }
    })
  }
  const handleEditHomework = () => {
    update()
    handleCloseEditModal()
  }
  const handleSelectLesson = (row) => () => {
    setSelectedLesson(row)
  }
  const handleSelectHomework = (row) => () => {
    lessonWorksDispatch({
      lessonWork: row,
      type: 'GET_LESSON_WORK_SUCCESS'
    })
  }
  
  /* MODALS */
  const handleOpenModal = (modal) => (e) => {
    e.stopPropagation()
    setOpenModal(prev => ({ ...prev, [modal]: true }))
  }
  const handleCLoseModal = () => {
    setOpenModal({ photo: false, file: false })
    lessonsDispatch({ type: 'CLEAR_SELECTED_LESSON_SUCCESS' })
  }
  const handleOpenEditModal = (row) => (e) => {
    e.stopPropagation()
    groupsDispatch({
      selectedGroup: row?.group,
      type: 'SET_SELECTED_GROUP_SUCCESS'
    })
    lessonsDispatch({
      selectedLesson: row,
      type: 'SET_SELECTED_LESSON_SUCCESS'
    })
    setOpenEditModal(true)
  }
  const handleCloseEditModal = () => {
    setOpenEditModal(false)
  }
  /* EFFECTS */
  useEffect(() => {
    return () => {
      lessonWorksDispatch({ type: 'CLEAR_SELECTED_WORKS' })
    }
    //eslint-disable-next-line
  },[])
  useEffect(() => {
    selectedLesson && getAllLessonWorks()
    //eslint-disable-next-line
  }, [selectedLesson])
  useEffect(() => {
    setSelectedLesson(null)
    //eslint-disable-next-line
  }, [selectedGroup])
  
  return (
    <SectionLayoutBase
      withGroupFilters
      filters={filters}
      modalSubmit={update}
      btnTitle="Создать ДЗ"
      ModalProps={ModalProps}
      ModalContent={CreateHomeworkFields}
      title={`${checkDevice('min-lg') ? 'Дом.' : 'Домашнее'} задание`}
    >
      <Grid item xs={12} lg={4} className={classes.cardGridItem}>
        <GeneralCard
          showData
          showType
          showFiles
          withEdit
          withDelete
          isHomework
          filters={filters}
          cardType="lessons"
          lessonType="lessons"
          linkEvent={handleSelectLesson}
          onDelete={handleDeleteHomework}
          onEdit={handleOpenEditModal}
          isSelected={selectedLesson?.id}
        />
      </Grid>
      <Grid item xs={12} lg={4} className={classes.cardGridItem}>
        <GeneralCard
          showMarks
          isStudents
          isHomework
          cardType="students"
          cardData={allLessonWorks}
          isSelected={lessonWork?.id}
          linkEvent={handleSelectHomework}
        />
      </Grid>
      <Grid item xs={12} lg={4} className={clsx(classes.cardGridItem, classes.fullHeight)}>
        <CardHomework open={openModal} openModal={handleOpenModal} closeModal={handleCLoseModal}/>
      </Grid>
      <ModalInnerContent open={openEditModal} close={handleCloseEditModal} {...EditModalProps}>
        <CreateHomeworkFields isEdit close={handleEditHomework}/>
      </ModalInnerContent>
    </SectionLayoutBase>
  )
}
