import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useLocation } from 'react-router'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import { deleteLesson, getLessons, getStudentLessonWork } from '../../Api'
import {
  useGroupsStore,
  useLessonsDispatch,
  useLessonWorkStore,
  useLessonWorkDispatch
} from '../../Store'
import {
  GeneralCard,
  SectionLayoutBase,
  CreateLessonFields, ask, ModalInnerContent
} from '../../Components'

const ModalProps = {
  id: 'lesson-modal',
  title: 'Создание урока',
  subTitle: 'Все поля, кроме описания и замены, обязательны к заполнению'
}

const useStyles = makeStyles((theme) => ({
  cardGridItem: {
    height: '100%',
    padding: '0 15px',
    
    [theme.breakpoints.down('lg')]: {
      padding: 0,
      height: 'auto',
      marginBottom: 15,
      maxHeight: '50vh'
    }
  },
  fullHeight: {
    [theme.breakpoints.down('lg')]: {
      maxHeight: '100%'
    }
  }
}))

export const Lessons = () => {
  const classes = useStyles()
  const Location = useLocation()
  const { selectedGroup } = useGroupsStore()
  const lessonsDispatch = useLessonsDispatch()
  const { allLessonWorks } = useLessonWorkStore()
  const [openModal, setOpenModal] = useState(false)
  const lessonWorksDispatch = useLessonWorkDispatch()
  const [filters] = useState({ orderByDate: 'desc' })
  const [selectedLesson, setSelectedLesson] = useState(null)
  
  const update = async () => {
    const apiData = await getLessons({ group: selectedGroup?.id, ...filters })
    
    if (apiData) {
      lessonsDispatch({
        lessons: apiData.rows,
        type: 'SET_LESSONS_LIST_SUCCESS'
      })
    }
  }
  const handleDeleteHomework = (row) => (e) => {
    e.stopPropagation()
    ask({
      title: 'Удаление урока',
      subTitle: `Вы точно хотите удалить урок "${row.subject}"`,
      btnTitle: 'Удалить',
      callback: async () => {
        const apiData = await deleteLesson(row.id)
        
        if (apiData) {
          toast.success('Урок удален!')
          lessonWorksDispatch({ type: 'CLEAR_SELECTED_WORKS' })
          update()
        } else {
          toast.error('При удалении урока произошла ошибка!')
        }
      }
    })
  }
  const getAllLessonWorks = async (lessonId) => {
    const apiData = await getStudentLessonWork({ lesson: lessonId, groups: selectedGroup?.id })
    
    if (apiData) {
      lessonWorksDispatch({
        allLessonWorks: apiData,
        type: 'GET_ALL_LESSON_WORKS_SUCCESS'
      })
    }
  }
  
  /* CARD CLICK EVENT */
  const handleSelectLesson = (row) => () => {
    setSelectedLesson(row)
  }
  const handleEditLesson = (lesson) => (e) => {
    e.stopPropagation()
    lessonsDispatch({
      selectedLesson: lesson,
      type: 'SET_SELECTED_LESSON_SUCCESS'
    })
    handleOpenModal()
  }
  
  /* MODAL EVENTS */
  const handleOpenModal = () => {
    setOpenModal(true)
  }
  const handleCloseModal = () => {
    setOpenModal(false)
  }
  
  /* EFFECTS */
  useEffect(() => {
    Location.state?.lessonId && getAllLessonWorks(Location.state.lessonId)
    
    return () => {
      lessonWorksDispatch({ type: 'CLEAR_SELECTED_WORKS' })
    }
    //eslint-disable-next-line
  }, [])
  useEffect(() => {
    setSelectedLesson(null)
    //eslint-disable-next-line
  }, [selectedGroup])
  useEffect(() => {
    selectedLesson && getAllLessonWorks(selectedLesson.id)
    //eslint-disable-next-line
  }, [selectedLesson])
  
  return (
    <>
      <SectionLayoutBase
        title="Уроки"
        withGroupFilters
        filters={filters}
        modalSubmit={update}
        btnTitle="Создать урок"
        ModalProps={ModalProps}
        ModalContent={CreateLessonFields}
      >
        <Grid item xs={12} lg={6} className={classes.cardGridItem}>
          <GeneralCard
            showType
            showData
            withEdit
            withDelete
            showComment
            filters={filters}
            cardType="lessons"
            lessonType="lessons"
            onEdit={handleEditLesson}
            linkEvent={handleSelectLesson}
            onDelete={handleDeleteHomework}
            isSelected={selectedLesson?.id || Location.state?.lessonId}
          />
        </Grid>
        <Grid item xs={12} lg={6} className={classes.cardGridItem}>
          <GeneralCard
            showMarks
            showAbsent
            isStudents
            cardType="students"
            cardData={allLessonWorks}
            filters={{ lesson: selectedLesson?.id }}
          />
        </Grid>
      </SectionLayoutBase>
  
      {(openModal && selectedLesson) && (
        <ModalInnerContent
          open={openModal}
          id="lesson_edit_modal"
          close={handleCloseModal}
          title="Редактирование урока"
          subTitle="Все поля, кроме описания и замены, обязательны к заполнению"
        >
          <CreateLessonFields lesson={selectedLesson} close={handleCloseModal} update={update}/>
        </ModalInnerContent>
      )}
    </>
  )
}
