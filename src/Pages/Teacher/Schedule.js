import React, { useEffect, useState } from 'react'
import Grid from '@material-ui/core/Grid'
import { getGroups, getLessons } from '../../Api'
import { checkDevice, formatDate, getAccountType } from '../../Helpers'
import {
  useGroupsStore,
  useTeachersStore,
  useGroupsDispatch,
  useLessonsDispatch,
  useStaticListsStore,
  useTeachersDispatch
} from '../../Store'
import { EventSchedule, SectionLayoutBase, CreateLessonFields } from '../../Components'

const SectionS = {
  sectionR: {
    paddingBottom: 0
  },
  contentGridR: {
    padding: 0,
    height: '100%'
  },
  cardGridItem: {
    height: '100%',
    display: 'flex',
    position: 'relative',
    flexDirection: 'column',
    padding: checkDevice('xl') ? '0 15px' : '0',
  }
}

const ModalProps = {
  id: 'lesson-modal',
  title: 'Создание урока',
  subTitle: 'Все поля, кроме описания и замены, обязательны к заполнению'
}

const ScheduleFilters = ({ teachers, groups, changeTeacher, changeGroup }) => [
  {
    label: 'Учитель',
    name: 'fullName',
    onChange: changeTeacher,
    filter: { id: 'teachers', type: 'select' },
    options: teachers.map(teacher => ({ ...teacher, format: option => option.fullName }))
  },
  {
    label: 'Группа',
    options: groups,
    onChange: changeGroup,
    filter: { id: 'group', type: 'select' }
  }
]

export const Schedule = () => {
  const account = getAccountType()
  const groupsDispatch = useGroupsDispatch()
  const { teachers } = useStaticListsStore()
  const lessonsDispatch = useLessonsDispatch()
  const teachersDispatch = useTeachersDispatch()
  const { selectedTeacher } = useTeachersStore()
  const { groups, selectedGroup } = useGroupsStore()
  const [filters, setFilters] = useState({ month: formatDate(new Date(), 'yyyy-MM') })

  const handleChangeDate = (date) => {
    console.log(date)
    const formattedDate = formatDate(date, 'yyyy-MM')

    setFilters(prev => ({ ...prev, month: formattedDate }))
  }
  const handleChangeTeacher = async (e) => {
    const id = e.value || e.target.value
    const apiData = await getGroups({ teachers: id })
  
    if (id === '') {
      lessonsDispatch({ type: 'CLEAR_LESSONS_LIST_SUCCESS' })
      teachersDispatch({ type: 'CLEAR_SELECTED_TEACHER_SUCCESS' })
    } else {
      teachersDispatch({
        type: 'SET_SELECTED_TEACHER_SUCCESS',
        selectedTeacher: teachers.filter(teacher => teacher.id === id)[0]
      })
    }
    
    setFilters(prev => ({ ...prev, teachers: id }))
    
    if (apiData) {
      groupsDispatch({
        groups: apiData,
        type: 'SET_GROUPS_LIST_SUCCESS'
      })
    }
  }
  const handleChangeGroup = (e) => {
    const id = e.value
  
    groupsDispatch({
      type: 'SET_SELECTED_GROUP_SUCCESS',
      selectedGroup: groups.filter(group => group.id === id)[0]
    })
  }

  const update = async (newFilters) => {
    const apiData = await getLessons({ group: selectedGroup?.id, teachers: selectedTeacher?.id, ...filters, ...newFilters })

    if (apiData) {
      lessonsDispatch({
        lessons: apiData.rows,
        type: 'SET_LESSONS_LIST_SUCCESS'
      })
    }
  }
  
  const handleClearFilters = () => {
    lessonsDispatch({ type: 'CLEAR_LESSONS_LIST_SUCCESS' })
    groupsDispatch({ type: 'CLEAR_SELECTED_GROUP_SUCCESS' })
    teachersDispatch({ type: 'CLEAR_SELECTED_TEACHER_SUCCESS' })
  }
  
  useEffect(() => {
    if (account === 'admin') {
      if (selectedTeacher) {
        update()
      }
    } else {
      update()
    }
    //eslint-disable-next-line
  }, [selectedGroup, filters])
  
  return (
    <SectionLayoutBase
      noSpacing
      noBottomOffset
      filters={filters}
      title="Расписание"
      customS={SectionS}
      modalSubmit={update}
      updateFilters={update}
      ModalProps={ModalProps}
      btnTitle="Создать расписание"
      clearFilters={handleClearFilters}
      ModalContent={CreateLessonFields}
      btnProps={{ disabled: account === 'admin' && !selectedTeacher }}
      withGlobalFilters={account === 'admin' && checkDevice('min-lg')}
      withGroupFilters={account === 'admin' ? checkDevice('xl') : true}
      filtersList={ScheduleFilters({ teachers, groups, changeTeacher: handleChangeTeacher, changeGroup: handleChangeGroup })}
    >
      <Grid item xs={12} style={SectionS.cardGridItem}>
        <EventSchedule update={update} handleChangeDate={handleChangeDate} handleChangeSelect={handleChangeTeacher}/>
      </Grid>
    </SectionLayoutBase>
  )
}
