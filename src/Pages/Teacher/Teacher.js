import React, { useState } from 'react'
import clsx from 'clsx'
import { session } from 'store2'
import styled from 'styled-components'
import { toast } from 'react-toastify'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { studyTeacher } from '../../Api/Study'
import { getUser, DesktopView, checkDevice } from '../../Helpers'
import {
  Studying,
  CardNotes,
  GeneralCard,
  SectionTitle,
  LightTooltip,
  SectionLayout,
  SectionSubTitle
} from '../../Components'

const useStyles = makeStyles(theme => ({
  rootContainer: {
    height: '100%',
    padding: '0 15px',
    maxWidth: 'calc(80% + 30px)',
    [theme.breakpoints.down('md')]: {
      maxWidth: '100%'
    }
  },
  gridContainerRoot: {
    height: '100%',
    marginLeft: '-30px',
    marginRight: '-30px',
    width:'calc(100% + 60px)',
    [theme.breakpoints.down('md')]: {
      marginLeft: '-15px',
      marginRight: '-15px',
      width: 'calc(100% + 30px)'
    }
  },
  gridItemRoot: {
    height: '100%',
    display: 'flex',
    padding: '0 30px',
    flexDirection: 'column',
    [theme.breakpoints.down('md')]: {
      padding: '0 15px',
    }
  },
  mb30: {
    marginBottom: 30
  }
}))

const TeacherSteps = [
  {
    offset: 30,
    position: 'top',
    className: 'schedule-card',
    title: 'Информационная панель',
    desc: 'На этой панели вы можете видеть ближайшее расписание, особые уроки, ' +
      'а также оставлять заметки, чтобы не забыть что-то важное. ' +
      'А, также, если навести на один из тегов - вы сможете узнать, что он означает. ' +
      'В синей обводке указан группа, в желтой - тип урока, в красной - дата урока'
  },
  {
    offset: 40,
    position: 'bottom',
    title: 'Расписание',
    className: 'schedule-link',
    desc: 'В расписании вы можете создавать уроки, ставить себе замену и совершать другие действия с ' +
      'уроками, которые в дальнейшем увидят у себя ученики.'
  },
  {
    offset: 40,
    position: 'bottom',
    title: 'Домашнее задание',
    className: 'homework-link',
    desc: 'Вы можете оставлять, проверять и оценивать домашние задания'
  },
  {
    offset: 40,
    title: 'Уроки',
    position: 'bottom',
    className: 'lessons-link',
    desc: 'Вы можете создавать, оценивать и ставить н/б для каждого урока'
  },
  {
    offset: 40,
    position: 'bottom',
    id: 'account_menu',
    title: 'Всплывающее окно аккаунта',
    desc: 'Вы можете ознакомиться с вопросами-ответами, а так же договором с нашей компанией'
  }
]

export const RepeatStudy = styled.div`
  left: 60px;
  bottom: 60px;
  padding: 15px;
  z-index: 9999;
  display: flex;
  cursor: pointer;
  position: fixed;
  border-radius: 100%;
  background-color: ${({ theme }) => theme.colors.main};
  
  img {
    width: 40px;
    height: 40px;
  }
`

export const Teacher = () => {
  const user = getUser()
  const classes = useStyles()
  const [steps, setSteps] = useState(TeacherSteps)
  const [repeatStudy, setRepeatStudy] = useState(false)
  
  const handleRepeatStudy = () => {
    setSteps(TeacherSteps)
    setRepeatStudy(true)
  }
  
  const handleEndStudying = async () => {
    const apiData = await studyTeacher()
  
    if (apiData) {
      session.set('user', { ...user, wasTrained: apiData.wasTrained })
      toast.success('Обучение завершено!')
    }
  
    setSteps([])
    setRepeatStudy(true)
  }

  return (
    <SectionLayout>
      <Container maxWidth={false} className={classes.rootContainer}>
        <Grid container spacing={0} className={classes.gridContainerRoot}>
          <Grid item xs={12} md={6} className={classes.gridItemRoot}>
            <SectionTitle variant="h1">Hello, {user?.name}</SectionTitle>
            <SectionSubTitle variant="h6">
              Рады приветствовать вас на онлайн-платформе курсов английского языка «The First Step»!
            </SectionSubTitle>

            <GeneralCard
              showData
              showGroup
              withMobileSlider
              cardType="lessons"
              withoutGroupFilters
              lessonType="lessons"
              filters={{ today: 1 }}
              cardTitle="Расписание на сегодня"
              className={clsx('schedule-card', checkDevice('min-lg') && classes.mb30)}
            />
          </Grid>
          <Grid item xs={12} md={6} className={classes.gridItemRoot}>
            <GeneralCard
              showData
              showGroup
              withMobileSlider
              cardType="lessons"
              withoutGroupFilters
              lessonType="lessons"
              filters={{ tomorrow: 1 }}
              className={classes.mb30}
              cardTitle="Расписание на завтра"
            />
            <GeneralCard
              showData
              showType
              showGroup
              withMobileSlider
              cardType="lessons"
              withoutGroupFilters
              lessonType="lessons"
              cardTitle="Особые уроки"
              className={classes.mb30}
              filters={{ types: 'TYPE_EXAM,TYPE_TEST,TYPE_WORKING_OUT' }}
            />
            <CardNotes/>
          </Grid>
        </Grid>
      </Container>
      
      {(!user.wasTrained || repeatStudy) && (
        <DesktopView>
          <Studying steps={steps} onEndStudying={handleEndStudying}/>
        </DesktopView>
      )}
  
      {(user.wasTrained && !repeatStudy) && (
        <DesktopView>
          <LightTooltip title="Повторить обучение?" placement="top" arrow>
            <RepeatStudy onClick={handleRepeatStudy}>
              <img src="/assets/images/StudyQuestion.svg" alt="Repeat study?"/>
            </RepeatStudy>
          </LightTooltip>
        </DesktopView>
      )}
    </SectionLayout>
  )
}
