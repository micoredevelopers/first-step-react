import {
  FAQ,
  Class,
  Marks,
  Teacher,
  Student,
  Lessons,
  Payments,
  Schedule,
  Homework,
  Teachers,
  Students,
  Agreement,
  TeacherPage,
  StudentPage,
  PaymentsAdmin,
  StudentSchedule
} from '../../Pages'

export const getDefaultRoutes = (account) => [
  {
    Component: FAQ,
    path: `/${account}/faq`
  }
]

export const getAccountRoutes = (account) => {
  switch (account) {
    case 'teacher':
      return [
        {
          path: '/teacher',
          Component: Teacher
        },
        {
          Component: Schedule,
          path: '/teacher/schedule'
        },
        {
          Component: Lessons,
          path: '/teacher/lessons'
        },
        {
          Component: Homework,
          path: '/teacher/homework'
        },
        {
          Component: Agreement,
          path: `/teacher/agreement`
        }
      ]
    case 'student':
      return [
        {
          path: '/student',
          Component: Student
        },
        {
          Component: Marks,
          path: '/student/marks'
        },
        {
          path: '/student/schedule',
          Component: StudentSchedule
        },
        {
          Component: Class,
          path: '/student/class'
        },
        {
          Component: Payments,
          path: '/student/payment'
        }
      ]
    case 'admin':
      return [
        {
          Component: Teachers,
          path: '/admin/teachers'
        },
        {
          Component: TeacherPage,
          path: '/admin/teachers/:id'
        },
        {
          Component: Students,
          path: '/admin/students'
        },
        {
          Component: StudentPage,
          path: '/admin/students/:id'
        },
        {
          Component: Schedule,
          path: '/admin/schedule'
        },
        {
          path: '/admin/payment',
          Component: PaymentsAdmin
        }
      ]
    default:
      return []
  }
}
