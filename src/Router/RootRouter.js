import React, { useEffect } from 'react'
import styled from 'styled-components'
import { down } from 'styled-breakpoints'
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom'
import { Login, } from '../Pages'
import { AppHeader, AskModal } from '../Components'
import { getGroups, getStudents, getTeachers } from '../Api'
import { getAccountRoutes, getDefaultRoutes } from './Config'
import { useGroupsDispatch, useStaticListsDispatch, TeachersProvider, StudentsProvider } from '../Store'

const RootMainWrap = styled.main`
  padding-top: 100px;
  min-height: 100vh;
  
  ${down('md')} {
    padding-top: 70px;
  }
`

export const RootRouter = ({ isAuth, account }) => {
  const groupDispatch = useGroupsDispatch()
  const staticListDispatch = useStaticListsDispatch()
  const allRoutes = getDefaultRoutes(account).concat(getAccountRoutes(account))
  
  const redirectToPage = () => {
    switch (account) {
      case 'teacher':
        return '/teacher'
      case 'student':
        return '/student'
      case 'admin':
        return '/admin'
      default: return '/'
    }
  }
  
  const getGroupsList = async () => {
    const apiData = await getGroups({ limit: 50 })

    apiData && groupDispatch({
      groups: apiData,
      type: 'SET_GROUPS_LIST_SUCCESS'
    })
  }
  
  const getTeachersList = async () => {
    const apiData = await getTeachers()
    
    if (apiData) {
      staticListDispatch({
        teachers: apiData.rows,
        type: 'SET_TEACHERS_LIST_SUCCESS'
      })
    }
  }
  const getStudentsList = async () => {
    const apiData = await getStudents()
  
    if (apiData) {
      staticListDispatch({
        students: apiData.rows,
        type: 'SET_STUDENTS_LIST_SUCCESS'
      })
    }
  }
  
  useEffect(() => {
    if (isAuth) {
      switch (account) {
        case 'teacher':
          getTeachersList()
          getGroupsList()
          break
        case 'student':
          getStudentsList()
          break
        case 'admin':
          getTeachersList()
          getStudentsList()
          break
        default: return
      }
    }
    // eslint-disable-next-line
  }, [])
  
  return (
    <BrowserRouter>
      {!isAuth ? (
        <Switch>
          <Route exact path="/" component={Login}/>
          <Redirect from="*" to="/"/>
        </Switch>
      ) : (
        <>
          <AppHeader account={account}/>
          <RootMainWrap>
            <Switch>
              <Redirect exact from="/admin" to="/admin/teachers"/>
              {allRoutes.map(({ path, Component }, index) => (
                <Route key={index} exact path={path}>
                  <TeachersProvider>
                    <StudentsProvider>
                      <Component />
                    </StudentsProvider>
                  </TeachersProvider>
                </Route>
              ))}
              <Redirect from="*" to={redirectToPage()}/>
            </Switch>
          </RootMainWrap>
        </>
      )}
      <AskModal/>
    </BrowserRouter>
  )
}
