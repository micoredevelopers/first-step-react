import { makeStore } from '../makeStore'

const initialState = {
  groups: [],
  selectedGroup: undefined
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_GROUPS_LIST_SUCCESS':
      return {
        ...state,
        groups: action.groups,
        // selectedGroup: action.groups[0]
      }
    case 'SET_SELECTED_GROUP_SUCCESS':
      return {
        ...state,
        selectedGroup: action.selectedGroup
      }
    case 'CLEAR_SELECTED_GROUP_SUCCESS':
      return {
        ...state,
        selectedGroup: null
      }
    default: return state
  }
}

const [
  GroupsProvider,
  useGroupsStore,
  useGroupsDispatch
] = makeStore(reducer, initialState)

export { GroupsProvider, useGroupsStore, useGroupsDispatch }
