import { makeStore } from '../makeStore'

const initialState = {
  lessonWork: null,
  showLessons: true,
  showTitleRow: true,
  allLessonWorks: [],
  showStudents: false,
  showHomework: false,
  pagination: { page: 1, limit: 10, maxPages: 1 }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_ALL_LESSON_WORKS_SUCCESS':
      return {
        ...state,
        lessonWork: null,
        showLessons: false,
        showStudents: true,
        pagination: action.pagination,
        showTitleRow: !action.isHomework,
        allLessonWorks: action.allLessonWorks
      }
    case 'GET_LESSON_WORK_SUCCESS':
      return {
        ...state,
        showHomework: true,
        showLessons: false,
        showStudents: false,
        showTitleRow: false,
        lessonWork: action.lessonWork
      }
    case 'UPDATE_LESSON_WORK_SUCCESS':
      return {
        ...state,
        allLessonWorks: state.allLessonWorks.map(work => work.id === action.workId ?
          {
            ...work,
            absent: action.updatedWork.absent,
            lessonMark: action.updatedWork.lessonMark,
            homeworkMark: action.updatedWork.homeworkMark,
            homeworkFilesImages: action.updatedWork.homeworkFilesImages,
          } : work
        ),
        lessonWork: state.lessonWork?.id && state.lessonWork.id === action.updatedWork.id ?
          {
            ...state.lessonWork,
            // TODO: Change to get lesson files
            ...action.updatedWork
          } : state.lessonWork
      }
    case 'BACK_TO_LESSONS': {
      return {
        ...state,
        showLessons: true,
        showTitleRow: true,
        showStudents: false,
        showHomework: false
      }
    }
    case 'BACK_TO_STUDENTS': {
      return {
        ...state,
        showStudents: true,
        showHomework: false
      }
    }
    case 'CLEAR_SELECTED_WORKS':
      return {
        ...state,
        lessonWork: null,
        showLessons: true,
        allLessonWorks: [],
        showTitleRow: true,
        showStudents: false,
        showHomework: false
      }
    default: return state
  }
}

const [
  LessonWorkProvider,
  useLessonWorkStore,
  useLessonWorkDispatch
] = makeStore(reducer, initialState)

export { LessonWorkProvider, useLessonWorkStore, useLessonWorkDispatch }
