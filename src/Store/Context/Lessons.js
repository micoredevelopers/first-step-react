import { makeStore } from '../makeStore'

const initialState = {
  lessons: [],
  selectedLesson: null,
  pagination: { page: 1, limit: 10, maxPages: 1 }
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_LESSONS_LIST_SUCCESS':
      return {
        ...state,
        lessons: action.lessons,
        pagination: action.pagination
      }
    case 'SET_SELECTED_LESSON_SUCCESS':
      return {
        ...state,
        selectedLesson: action.selectedLesson
      }
    case 'CLEAR_LESSONS_LIST_SUCCESS':
      return initialState
    case 'CLEAR_SELECTED_LESSON_SUCCESS':
      return {
        ...state,
        selectedLesson: null
      }
    default: return state
  }
}

const [
  LessonsProvider,
  useLessonsStore,
  useLessonsDispatch
] = makeStore(reducer, initialState)

export { LessonsProvider, useLessonsStore, useLessonsDispatch }
