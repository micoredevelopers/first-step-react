import { makeStore } from '../makeStore'

const initialState = {
  payments: []
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_PAYMENTS_LIST_SUCCESS':
      return {
        ...state,
        payments: action.payments
      }
    default: return state
  }
}

const [
  PaymentsProvider,
  usePaymentsStore,
  usePaymentsDispatch
] = makeStore(reducer, initialState)

export { PaymentsProvider, usePaymentsStore, usePaymentsDispatch }

