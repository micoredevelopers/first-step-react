import { makeStore } from '../makeStore'

const initialState = {
  teachers: [],
  students: []
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_TEACHERS_LIST_SUCCESS':
      return {
        ...state,
        teachers: action.teachers
      }
    case 'SET_STUDENTS_LIST_SUCCESS':
      return {
        ...state,
        students: action.students
      }
    default: return state
  }
}

const [
  StaticListsProvider,
  useStaticListsStore,
  useStaticListsDispatch
] = makeStore(reducer, initialState)

export { StaticListsProvider, useStaticListsStore, useStaticListsDispatch }
