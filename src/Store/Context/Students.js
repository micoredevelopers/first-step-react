import { makeStore } from '../makeStore'

const initialState = {
  students: [],
  selectedStudent: null,
  pagination: { page: 1, limit: 10, maxPages: 1 }
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_STUDENTS_LIST_SUCCESS':
      return {
        ...state,
        students: action.students,
        pagination: action.pagination
      }
    case 'SET_SELECTED_STUDENT_SUCCESS':
      return {
        ...state,
        selectedStudent: action.selectedStudent
      }
    case 'CLEAR_SELECTED_STUDENTS_SUCCESS':
      return {
        ...state,
        selectedStudent: null
      }
    default: return state
  }
}

const [
  StudentsProvider,
  useStudentsStore,
  useStudentsDispatch
] = makeStore(reducer, initialState)

export { StudentsProvider, useStudentsStore, useStudentsDispatch }

