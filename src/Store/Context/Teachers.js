import { makeStore } from '../makeStore'

const initialState = {
  teachers: [],
  selectedTeacher: null,
  pagination: { page: 1, limit: 10, maxPages: 1 }
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_TEACHERS_LIST_SUCCESS':
      return {
        ...state,
        teachers: action.teachers,
        pagination: action.pagination
      }
    case 'SET_SELECTED_TEACHER_SUCCESS':
      return {
        ...state,
        selectedTeacher: action.selectedTeacher
      }
    case 'CLEAR_SELECTED_TEACHER_SUCCESS':
      return {
        ...state,
        selectedTeacher: null
      }
    default: return state
  }
}

const [
  TeachersProvider,
  useTeachersStore,
  useTeachersDispatch
] = makeStore(reducer, initialState)

export { TeachersProvider, useTeachersStore, useTeachersDispatch }
