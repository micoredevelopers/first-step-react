import { createGlobalStyle } from 'styled-components'

export const GlobalStyled = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  li {
    list-style: none;
  }
  
  input, select { 
    font-size: 100%;
  }
  
  button {
    border: none;
    background-color: transparent;
    outline: none !important;
  }
  
  a {
    text-decoration: none;
  }
    
  #root {
    width: 100%;
    position: relative;
    background: #efeff0;
    min-height: 100vh;
  }
  
  .e-quick-popup-wrapper,
  .e-more-popup-wrapper {
    z-index: -1 !important;
    display: none !important;
    visibility: hidden !important;
    pointer-events: none !important;
  }
`
