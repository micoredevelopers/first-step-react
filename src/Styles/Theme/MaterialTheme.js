import { createTheme } from '@material-ui/core'

export const MaterialTheme = createTheme({
  palette: {
    primary: {
      main: '#003d7c'
    },
    warning: {
      main: '#d59006'
    },
    text: {
      primary: '#000c18',
      disabled: '#bcbfc2',
      secondary: '#8f9398'
    },
    success: { main: '#06d58b' },
    secondary: { main: '#d51006' },
    error: { main: '#d51006' }
  },
  breakpoints: {
    keys: ['xs', 'sm', 'md', 'lg', 'xl'],
    values: { xs: 375, sm: 576, md: 768, lg: 1040, xl: 1199 }
  },
  spacing: 5
})
