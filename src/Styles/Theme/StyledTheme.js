const breakpoints = {
  xs: '375px',
  sm: '576px',
  md: '768px',
  lg: '1040px',
  xl: '1199px'
}

const colors = {
  main: '#003d7c',
  secondary: '#d51006',
  error: '#d51006',
  warning: '#d59006',
  success: '#06d58b',
  black: '#000c18',
  grey: '#8f9398',
  lightGrey: '#bcbfc2',
  disabled: '#e6e6e6'
}

export const StyledTheme = { colors, breakpoints }
